<?php
include('asset/main.php');
$main = new main();
$general = new general();

$type = $general->securestring('decrypt',$_GET['type']);
$start = $general->securestring('decrypt',$_GET['start']);
$end = $general->securestring('decrypt',$_GET['end']);
$startDate = date('Y-m-d', strtotime($start));
$endDate = date('Y-m-d', strtotime($end));

function getMrn($col, $mrn){
	$sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_mrn = '$mrn'");
	$row = mysql_fetch_assoc($sql);
	return $row[$col];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>KPJ Ipoh Specialist Hospital | SMBS</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="author" content="Nahahmad">
    <script src="js/webfont.js"></script>
    <link rel="stylesheet" href="css/googleapis.css" media="all">
    <link href="css/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="css/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="assets/demo/demo12/media/img/logo/favicon.ico" />
    <script src="js/jquery.mousewheel.min.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
</head>

<body class="m-page--fluid m--skin- m-page--loading-enabled m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-content">
            <div class="m-section__content" id="printThis">
                <table class="table table-sm m-table m-table--head-bg-secondary table-bordered  m-table--border-success">
                    <thead class="thead-inverse">
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                MRN
                            </th>
                            <th>
                                Episode
                            </th>
                            <th>
                                Department
                            </th>
                            <th>
                                Days
                            </th>
                            <th>
                                Amount
                            </th>
                            <th>
                                Bill No
                            </th>
                            <th>
                                Remarks
                            </th>
                            <th>
                                Doctor
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Responsible
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $bill = 1;
                        $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = '$type' AND d_status = 'active' AND d_vdate BETWEEN '$startDate' AND '$endDate'"); 
                        if(mysql_num_rows($listOut)){
                            while($rowOut=mysql_fetch_assoc($listOut)){
                        ?>
                        <tr>
                            <th scope="row">
                                <?php echo $bill; ?>
                            </th>
                            <td>
                                <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_mrn']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_episode']; ?>
                            </td>
                            <td>
                                <?php echo $general->allTable($general->allTable($rowOut['d_staffID'],'d_staffID','tbl_labor','d_department'),'d_id','tbl_setting','d_value'); ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_day']; ?>
                            </td>
                            <td>
                                RM <?php echo $rowOut['d_charge']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_bill']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_doc']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_created']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_who']; ?>
                            </td>
                            <td>
                                <?php echo $rowOut['d_treatCheck']; ?>
                            </td>
                        </tr>
                        <?php $bill++; } }else{ ?>
                        <tr>
                            <td colspan="12" style="text-align:center;background:#FEFEE;">
                                No Data
                            </td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <?php //$main->includePHP('component','left-sticky-nav'); ?>

    <script src="js/vendors.bundle.js" type="text/javascript"></script>
    <script src="js/scripts.bundle.js" type="text/javascript"></script>
    <script src="js/fullcalendar.bundle.js" type="text/javascript"></script>
    <script src="js/dashboard.js" type="text/javascript"></script>
    <script>
        $(window).on('load', function() {
            $('body').removeClass('m-page--loading');
        });
    </script>
    <!-- end::Page Loader -->
</body>
<!-- end::Body -->

</html>
<script src="js/jquery-3.3.1.min.js"></script>
<script>
    print();
</script>
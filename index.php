<?php
  include('asset/main.php');
  $main = new main();
  $controller = new general();
  $login = new pageSession();

  if(isset($_POST['btn-login'])){
    $username = mysql_real_escape_string(trim($_POST['username']));
    $password = mysql_real_escape_string(trim($_POST['password']));
    $password = sha1($password);

    $checkLogin = $login->userLogin($username,$password);
    header($checkLogin);
    // header('Location:system.php?p='.$controller->securestring('encrypt',105));
  }

  // var_dump($_SESSION);
  // echo "<br>";
  // echo "name = ".$controller->securestring('decrypt',$_SESSION['name']);
  // echo "<br>";
  // echo "role = ".$controller->securestring('decrypt',$_SESSION['roles']);
  // echo "<br>";
  //echo "login session = ".$controller->securestring('decrypt',$_SESSION['loginsession']);
?>

<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>KPJ Ipoh Specialist Hospital | SMBS</title>
		<meta name="description" content="KPJ Ipoh Specialist Hospital GL Portal">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Nahahmad">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="js/webfont.js"></script>
    <link rel="stylesheet" href="css/googleapis.css" media="all">
		<link href="css/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="images/favicon.ico" />
    <script src="js/jquery.mousewheel.min.js"></script>
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

    <div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(images/system/bg.jpg);">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								<img src="images/system/loguo.png" width="400">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Welcome to <br> Staff Medical Benefit System
								</h3>
							</div>
							<form class="m-login__form m-form" action="" method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input"   type="text" placeholder="IC Number" name="username" autocomplete="off">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary" type="submit" name="btn-login">
										Sign In
									</button>
								</div>
                <div class="" style="margin-top:30px;">
                  <?php
        						if(isset($_GET['action'])){
        							switch($controller->securestring('decrypt',trim($_GET['action']))){
        								case 'under-maintenance':
        						?>
        							<div class="alert alert-success alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								System is currently under maintenance. Please login back in few moments. Thank you.
        							</div>
        						<?php
        									break;
        								case 'logout':
        						?>
        							<div class="alert alert-success alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								You has successfully logout from system.
        							</div>
        						<?php
        									break;
        								case 'no-permission':
        						?>
        							<div class="alert alert-danger alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								You don't have permission to access the system.
        							</div>
        						<?php
                                            break;
        								case 'no-permission2':
        						?>
        							<div class="alert alert-danger alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								You don't have permission to access the page.
        							</div>
        						<?php
        									break;
        								case 'invalid-session':
        						?>
        							<div class="alert alert-info alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								Invalid login session to system. Please login back to system.
        							</div>
        						<?php
        									break;
        								case 'acc-deactive':
        						?>
        							<div class="alert alert-warning alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								Your account is deactive. Please contact administrator for activation.
        							</div>
        						<?php
        									break;
        								case 'multiple-login':
        						?>
        							<div class="alert alert-warning alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								Multiple login detected. Please login back to system.
        							</div>
        						<?php
        									break;
        								case 'No-Exist':
                    ?>
                      <div class="alert alert-warning alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                        Account username doesn't exist.
                      </div>
                    <?php
                          break;
                        case 'wrong-idpass':
                    ?>
        							<div class="alert alert-warning alert-dismissible fade show m-alert m-alert--square m-alert--air" role="alert">
        								<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        								Invalid password.
        							</div>
        						<?php
        									break;
        								default: break;
        							}
        						}
        					?>
                </div>
							</form>
						</div>
						<div class="m-login__account">
							<span class="m-login__account-msg">
							  Forgot your password ? Please contact administrator.
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="js/vendors.bundle.js" type="text/javascript"></script>
		<script src="js/scripts.bundle.js" type="text/javascript"></script>
	</body>
</html>

var DatatableRemoteAjaxDemo = {
  init: function() {
    var t;
    t = $(".m_datatable").mDatatable({
      data: {
        type: "remote",
        source: {
          read: {
            url: "js/data/user.php",
            map: function(t) {
              var e = t;
              return void 0 !== t.data && (e = t.data),
              e
            }
          }
        },
        pageSize: 10,
        serverPaging: !0,
        serverFiltering: !0,
        serverSorting: !0
      },
      layout: {
        scroll: !1,
        footer: !1
      },
      sortable: !0,
      pagination: !0,
      toolbar: {
        items: {
          pagination: {
            pageSizeSelect: [10, 20, 30, 50, 100]
          }
        }
      },
      search: {
        input: $("#generalSearch")
      },
      columns: [
        {
          field: "d_id",
          title: "#",
          sortable: !1,
          width: 40,
          selector: !1,
          textAlign: "center"
        }, {
          field: "d_name",
          title: "Name",
          width: 300
        }, {
          field: "d_department",
          title: "Dept",
          width: 70
        }, {
          field: "d_mrn",
          title: "MRN",
          width: 70
        }, {
          field: "d_ic",
          title: "Staff No",
          width: 70
        }, {
          field: "d_gender",
          title: "Gender",
          width: 70
        }, {
          field: "d_category",
          title: "Outpatient",
          width: 70
        }, {
          field: "d_category",
          title: "Inpatient",
          width: 70
        }, {
          field: "Actions",
          width: 40,
          title: "Actions",
          sortable: !1,
          overflow: "visible",
          template: function(t, e, a) {
            return '\t\t\t\t\t\t<div class="dropdown ' + (a.getPageSize() - e <= 4
              ? "dropup"
              : "") + '">\t\t\t\t\t\t\t<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">                                <i class="la la-ellipsis-h"></i>                            </a>\t\t\t\t\t\t  \t<div class="dropdown-menu dropdown-menu-right">\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\t\t\t\t\t\t    \t<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\t\t\t\t\t\t  \t</div>\t\t\t\t\t\t</div>\t\t\t\t\t\t'
          }
        }
      ]
    }),
    $("#m_form_status").on("change", function() {
      t.search($(this).val(), "Status")
    }),
    $("#m_form_type").on("change", function() {
      t.search($(this).val(), "Type")
    }),
    $("#m_form_status, #m_form_type").selectpicker()
  }
};
jQuery(document).ready(function() {
  DatatableRemoteAjaxDemo.init()
});

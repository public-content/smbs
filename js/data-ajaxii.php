
<script type="text/javascript">
var DatatableJsonRemoteDemo = {
init:
function() {
  var dir = "js/data/relative.php";
  var t,
    e;
    t = $(".m_datatable").mDatatable({
      data: {
        type: "remote",
        source: dir,
        pageSize: 30
      },
      layout: {
        theme: "default",
        class: "",
        scroll: !1,
        footer: !1
      },
      sort: {
        sort: "acs",
        field: "bil"
      },
      sortable: !0,
      pagination: !0,
      search: {
        input: $("#generalSearch")
      },
      columns: [
        {
          field: "bil",
          title: "#",
          width: 40
        },{
          field: "staff",
          title: "Staff-ID",
          width: 150
        },{
          field: "name",
          title: "Name",
          width: 150
        },{
          field: "mrn",
          title: "MRN",
          width: 50
        },{
          field: "ic",
          title: "IC No",
          width: 90
        },{
          field: "gender",
          title: "Gender",
          width: 50
        }
      ]
    }),
  e = t.getDataSourceQuery(),
  $("#m_form_status").on("change", function() {
    t.search($(this).val(), "Status")
  }).val(void 0 !== e.Status
    ? e.Status
    : ""),
  $("#m_form_type").on("change", function() {
    t.search($(this).val(), "Type")
  }).val(void 0 !== e.Type
    ? e.Type
    : ""),
  $("#m_form_status, #m_form_type").selectpicker()
},
};
jQuery(document).ready(function() {
DatatableJsonRemoteDemo.init()
});

</script>

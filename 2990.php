<?php
include('asset/main.php');
$main = new main();
$general = new general();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />

  <title>KPJ Ipoh Specialist Hospital | SMBS</title>
  <meta name="description" content="Latest updates and statistic charts">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
  <meta name="author" content="Nahahmad">
  <script src="js/webfont.js"></script>
  <link rel="stylesheet" href="css/googleapis.css" media="all">
  <link href="css/vendors.bundle.css" rel="stylesheet" type="text/css" />
  <link href="css/style.bundle.css" rel="stylesheet" type="text/css" />
  <link href="css/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="assets/demo/demo12/media/img/logo/favicon.ico" />
  <script src="js/jquery.mousewheel.min.js"></script>
</head>
<body class="m-page--fluid m--skin- m-page--loading-enabled m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
  <div class="m-page-loader m-page-loader--base">
    <div class="m-blockui">
      <span>Please wait...</span>
      <span>
        <div class="m-loader m-loader--brand"></div>
      </span>
    </div>
  </div>
  <div class="m-grid m-grid--hor m-grid--root m-page">
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
      <div class="m-container m-container--fluid m-container--full-height">
        <div class="m-stack m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-brand  m-brand--skin-dark ">
            <div class="m-stack m-stack--ver m-stack--general">
              <div class="m-stack__item m-stack__item--middle m-brand__logo">
                <a href="?page=index&demo=demo12" class="m-brand__logo-wrapper">
                  <img alt="" src="images/system/logo.png" width="30"/>
                </a>
              </div>
              <div class="m-stack__item m-stack__item--middle m-brand__tools">
                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                  <span></span>
                </a>
                <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                  <span></span>
                </a>
                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                  <i class="flaticon-more"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
            <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

            <?php include('asset/component/header-nav.php'); ?>
            <?php include('asset/component/header-profile.php'); ?>

          </div>
        </div>
      </div>
    </header>

    <?php $main->includePHP('view','page'); ?>

    <?php $main->includePHP('component','footer'); ?>

  </div>
  <div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
  </div>

  <?php //$main->includePHP('component','left-sticky-nav'); ?>

  <script src="js/vendors.bundle.js" type="text/javascript"></script>
  <script src="js/scripts.bundle.js" type="text/javascript"></script>
  <script src="js/fullcalendar.bundle.js" type="text/javascript"></script>
  <script src="js/dashboard.js" type="text/javascript"></script>
  <script>
    $(window).on('load', function() {
      $('body').removeClass('m-page--loading');
    });
  </script>
  <!-- end::Page Loader -->
</body>
<!-- end::Body -->

</html>

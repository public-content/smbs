<style>
    .loading {
        width: 165px;
        height: 30px;
        margin: 40px;
        padding: 10px;
        -webkit-box-shadow: 0px 0px 54px 12px rgba(166, 166, 166, 0.15);
        -moz-box-shadow: 0px 0px 54px 12px rgba(166, 166, 166, 0.15);
        box-shadow: 0px 0px 54px 12px rgba(166, 166, 166, 0.15);
    }

    .text {
        float: left;
        margin-top: 5px;
        margin-left: 16px;
    }

    .col-a {
        margin: 0;
        padding: 20px;
    }

    .col-b {
        margin: 0;
        padding: 20px;
    }

    .col-c {
        margin: 0;
        padding: 20px;
    }

</style>
<div class="col-a" id='link'>
    <ul>
<!--
        <li><a href="#" onclick="exportas()">Import</a></li>
        <li><a href="#" onclick="operationsql('phase1')">Phase 1 - table setting / table page</a></li>
        <li><a href="#" onclick="operationsql('phase2')">Phase 2 - creating other table</a></li>
        <li><a href="#" onclick="operationsql('phase3')">Phase 3 - profile</a></li>
        <li><a href="#" onclick="operationsql('phase4')">Phase 4 - dependent</a></li>
        <li><a href="#" onclick="treatment('1')">Phase 5 - treatment</a></li>
        <li><a href="#" onclick="firstorder()">All phase</a></li>
        <li><a href="#" onclick="supervisor()">Custom for supervisor</a></li>
-->
        <li><a href="#" onclick="custommade()">Custom for Me</a></li>

    </ul>
</div>
<div class="col-b" id="process"></div>
<div class="col-c" id='info'>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    info();

    function info() {
        document.getElementById('info').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            information: "showTable",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('info').innerHTML = html;
            }
        });
        return false;
    }

    function info2(val) {
        document.getElementById('info').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            information: "showTable",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('info').innerHTML = html;
                if (val <= 4) {
                    allee(val);
                } else {
                    treatment(1);
                }
            }
        });
        return false;
    }

    function operationsql(val) {
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            operationsql: val,
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
                info();
            }
        });
        return false;
    }

    function allee(val) {
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";

        var value = "phase" + val;

        var dataString = {
            operationsql: value,
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
                var vall = Number(val);
                var nee = vall + 1;
                info2(nee);
            }
        });
        return false;
    }

    function treatment(val) {
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var value = "phase" + val;
        var dataString = {
            treatment: value,
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
                var newVal = Number(val)+1;
                afterTreatment(newVal);
            }
        });
        return false;
    }

    function afterTreatment(val) {
        document.getElementById('info').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            information: "showTable",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('info').innerHTML = html;
                if (val <= 5) {
                    treatment(val);
                } else {
                    document.getElementById('process').innerHTML = "Done";
                }
            }
        });
        return false;
    }
    
    function exportas(){
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            exportas: "export",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
            }
        });
        return false;
    }
    
    function firstorder(){
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            exportas: "export",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
                allee(1)
            }
        });
        return false;
    }
    
    function supervisor(){
        document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            supervisor: "supervisor",
        };
        $.ajax({
            type: "post",
            url: "operation.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
            }
        });
        return false;
    }
    
    function custommade(){
          document.getElementById('process').innerHTML = "<div class='loading'> <img src='../images/system/gif/loading.gif' alt='' width='30' style='float:left'><div class='text'>Please Wait...</div></div>";
        var dataString = {
            custom: "custom",
        };
        $.ajax({
            type: "post",
            url: "custom.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('process').innerHTML = html;
            }
        });
        return false;
    }
    

</script>

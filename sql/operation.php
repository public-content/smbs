<?php
	session_start();
	
	$database['host']          	= 'localhost';
	$database['username']      	= 'wan';
	$database['password']      	= '12345678';
	$database['database']      	= 'smbsv2';
	
	$connect = mysql_connect($database['host'] , $database['username'] , $database['password']);
	if(!$connect){
		die(mysql_error());
	}
	mysql_select_db($database['database'], $connect) or die (mysql_error());
	
	date_default_timezone_set('Asia/Kuala_Lumpur');
?>

<?php 
    function getSetting($value){
		$sql = mysql_query("SELECT d_id FROM tbl_setting WHERE d_value = '$value'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_id'];
	}
	
	function getLaborDid($value){
		$sql = mysql_query("SELECT d_id FROM tbl_labor WHERE d_staffID = '$value'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_id'];
	}
	
	function getProfileDid($value){
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_ic = '$value'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_id'];
	}
	
	function getTypeGroup($value){
		if($value == "STAFF"){
			return 'staff';
		}elseif($value == "STAFFHOS"){
			return 'hos';
		}elseif($value == "DOCTOR"){
			return 'doctor';
		}elseif($value == "STAFFMGMT"){
			return 'management';
		}
	}
	
	function getRole($value){
		if($value == "STAFF"){
			return '394';
		}elseif($value == "STAFFHOS"){
			return '392';
		}elseif($value == "DOCTOR"){
			return '397';
		}elseif($value == "STAFFMGMT"){
			return '393';
		}
	}

    function getLevel($value){
        $sql = mysql_query("SELECT d_id FROM tbl_setting WHERE d_value = '$value'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_id'];
    } 

    function getActive($value){
        if($value == "Y"){
			return 'active';
		}elseif($value == "N"){
			return 'deactive';
		}elseif($value == "D"){
			return 'deleted';
		}
    }

    function getStaff($value){
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_ic = '$value'");
		$row = mysql_fetch_assoc($sql);
		$id = $row['d_id'];
		
		$sql1 = mysql_query("SELECT d_staffID FROM tbl_labor where d_id = '$id'");
		$row1 = mysql_fetch_assoc($sql1);
		return $row1['d_staffID'];
	}
	
	function getDoc($value){
		$sql = mysql_query("SELECT d_id FROM tbl_setting WHERE d_value3 = '$value'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_id'];
	}
	
	function getStatus($value){
		if($value == "Y"){
			return 'active';
		}else{
			return 'deactive';
		}
	}

    function treatmeat($value){
        $sql = mysql_query($value);
        if(mysql_num_rows($sql)){
            while($row = mysql_fetch_assoc($sql)){
                //all unit
                $staffid = mysql_real_escape_string(trim($row['staffid']));
                $mrn = mysql_real_escape_string(trim($row['treatmentmrn']));
                $episode = mysql_real_escape_string(trim($row['episodeno']));
                $charge = mysql_real_escape_string(trim($row['charge']));
                $noofday = mysql_real_escape_string(trim($row['noofday']));
                $bill = mysql_real_escape_string(trim($row['billno']));
                $doccode = mysql_real_escape_string(trim(getDoc($row['doccode'])));
                $treattype = mysql_real_escape_string(trim($row['treatmenttype']));	
                $treat = mysql_real_escape_string(trim($row['treatment']));
                $traetstat = mysql_real_escape_string(trim($row['treatmentstat']));
                $treatlsit = mysql_real_escape_string(trim($row['treatmentlist']));
                $treatconfirm = mysql_real_escape_string(trim($row['treatmentconfirm']));
                $exceedday = mysql_real_escape_string(trim($row['exceedday']));
                $exceedcharge = mysql_real_escape_string(trim($row['exceedcharge']));
                $exceedremarks = mysql_real_escape_string(trim($row['exceedremarks']));
                $bywho = "";
                $created = mysql_real_escape_string(trim($row['datecreated']));
                $modified = mysql_real_escape_string(trim($row['datemodified']));
                $active = mysql_real_escape_string(trim(getStatus($row['active'])));
                $d_visit = mysql_real_escape_string(trim($row['ldate']));


                $treatment = mysql_query("INSERT INTO tbl_treatment(d_staffID, d_mrn, d_episode, d_charge, d_day, d_bill, d_doc, d_type, d_treatment, d_treatmentstatus, d_treatmentlist, d_treatmentconfirm, d_exceedday, d_exceedcharge, d_exceedremarks, d_who, d_created, d_modified, d_status, d_vdate)VALUES('$staffid','$mrn','$episode','$charge','$noofday','$bill','$doccode','$treattype','$treat','$traetstat','$treatlsit','$treatconfirm','$exceedday','$exceedcharge','$exceedremarks','$bywho','$created','$modified','$active','$d_visit')");

                if(!$treatment){
                    echo mysql_error();
                }else{
                    echo 'success';
                }	
            }
        }
    }
?>


<?php 
    if(isset($_POST['operationsql'])){
        if($_POST['operationsql'] == 'phase1'){
            
            mysql_query("DROP TABLE tbl_setting");
            mysql_query("DROP TABLE tbl_page");
            mysql_query("DROP TABLE tbl_privileges");
            mysql_query("DROP TABLE tbl_installment");
            mysql_query("DROP TABLE tbl_labor");
            mysql_query("DROP TABLE tbl_logs");
            mysql_query("DROP TABLE tbl_profile");
            mysql_query("DROP TABLE tbl_treatment");
            mysql_query("DROP TABLE tbl_user");
            
//            create tbl_seeting and insert value
            $tbl_setting = mysql_query("CREATE TABLE `tbl_setting` (
                                          `d_id` int(11) NOT NULL,
                                          `d_type` varchar(50) NOT NULL DEFAULT '',
                                          `d_value` varchar(1000) NOT NULL DEFAULT '',
                                          `d_value2` varchar(1000) NOT NULL DEFAULT '',
                                          `d_value3` varchar(1000) NOT NULL DEFAULT '',
                                          `d_ref` varchar(10) NOT NULL DEFAULT '',
                                          `d_created` varchar(30) NOT NULL DEFAULT '',
                                          `d_modified` varchar(30) NOT NULL DEFAULT '',
                                          `d_status` varchar(20) NOT NULL DEFAULT ''
                                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            
            $insert_tbl_setting = mysql_query("INSERT INTO `tbl_setting`(`d_id`, `d_type`, `d_value`, `d_value2`, `d_value3`, `d_ref`, `d_created`, `d_modified`, `d_status`) VALUES
                                                (1, 'categorylevel', 'A0', '2400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (2, 'categorylevel', 'A1', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (3, 'categorylevel', 'A2', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (4, 'categorylevel', 'A3', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (5, 'categorylevel', 'A4', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (6, 'categorylevel', 'A5', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (7, 'categorylevel', 'A6', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (8, 'categorylevel', 'A7', '400.00', '10000.00', '', '2012-08-08 15:03:58', '2012-08-08 15:03:58', 'active'),
                                                (9, 'categorylevel', 'B0', '400.00', '10000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (10, 'categorylevel', 'B1', '400.00', '10000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (11, 'categorylevel', 'B2', '400.00', '10000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (12, 'categorylevel', 'C0', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (13, 'categorylevel', 'C1', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (14, 'categorylevel', 'C2', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (15, 'categorylevel', 'C3', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (16, 'categorylevel', 'D0', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (17, 'categorylevel', 'D1', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (18, 'categorylevel', 'D2', '400.00', '5000.00', '', '2012-08-11 10:01:28', '2012-08-11 10:01:28', 'active'),
                                                (19, 'categorylevel', 'Z1', 'infinite', 'infinite', '', '2012-09-06 09:55:30', '2012-09-06 09:55:30', 'active'),
                                                (20, 'categorylevel', 'Y1', '2400.00', '15000.00', '', '2012-09-06 10:02:30', '2012-09-06 10:02:30', 'active'),
                                                (21, 'categorylevel', 'X1', '0.00', '0.00', '', '2012-12-21 09:04:30', '2012-12-21 09:04:30', 'active'),
                                                (22, 'categorylevel', 'Y2', '2400.00', 'infinite', '', '2014-02-25 10:00:00', '2014-02-25 10:00:00', 'active'),
                                                (23, 'categorylevel', 'Y3', '1200.00', 'infinite', '', '2014-02-25 10:00:00', '2014-02-25 10:00:00', 'active'),
                                                (24, 'categorylevel', 'Y4', '1200.00', '15000.00', '', '2014-02-25 10:00:00', '2014-02-25 10:00:00', 'active'),
                                                (25, 'categorylevel', 'E1', '400.00', '10000.00', '', '2015-05-13 08:36:30', '2015-05-13 08:36:30', 'active'),
                                                (26, 'categorylevel', 'E2', '100.00', '0.00', '', '2015-05-13 08:36:30', '2015-05-13 08:36:30', 'active'),
                                                (27, 'categorylevel', 'E3', '300.00', '0.00', '', '2016-01-04 12:00:19', '2016-01-04 12:00:19', 'active'),
                                                (28, 'categorylevel', 'S1', '466.00', '0.00', '', '2017-04-04 00:00:00', '2017-04-04 00:00:00', 'active'),
                                                (29, 'department', 'A&E', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (30, 'department', 'ACCIDENT & EMERGENCY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (31, 'department', 'ADMINISTRATION', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (32, 'department', 'ADMISSION', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (33, 'department', 'BUSINESS OFFICE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (34, 'department', 'CAFE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (35, 'department', 'CENTRAL STORE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (36, 'department', 'CLAB', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (37, 'department', 'CLINIC', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (38, 'department', 'DAY CARE CENTRE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (39, 'department', 'DAYCARE CENTRE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (40, 'department', 'DIAGNOSTIC CENTRE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (41, 'department', 'DIAGNOSTIC IMAGING', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (42, 'department', 'DIET', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (43, 'department', 'DIETARY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (44, 'department', 'FINANCE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (45, 'department', 'FLEET MANAGEMENT', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (46, 'department', 'HAEMODIALYSIS', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (47, 'department', 'HUMAN RESOURCE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (48, 'department', 'ICU', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (49, 'department', 'ICU/CCU/CICU/HDU', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (50, 'department', 'INFORMATION TECHNOLOGY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (51, 'department', 'LINEN', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (52, 'department', 'MAINTENANCE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (53, 'department', 'MAINTENANCE & ENGINEERING', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (54, 'department', 'MARKETING', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (55, 'department', 'MATERNITY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (56, 'department', 'MEDICAL RECORD', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (57, 'department', 'MEDICAL WARD', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (58, 'department', 'NURSING ADMIN', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (59, 'department', 'OPERATION THEATER', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (60, 'department', 'OPERATION THEATRE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (61, 'department', 'PAEDIATRIC', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (62, 'department', 'PHARMACY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (63, 'department', 'PHYSIOTHERAPY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (64, 'department', 'PR/MARKETING', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (65, 'department', 'PREMIER WARD', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (66, 'department', 'PUBLIC RELATION & CUSTOMER SERVICE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (67, 'department', 'PURCHASING', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (68, 'department', 'PURCHASING STORE', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (69, 'department', 'QUALITY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (70, 'department', 'RADIOTHERAPY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (71, 'department', 'RADIOTHERAPY & ONCOLOGY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (72, 'department', 'SECURITY', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (73, 'department', 'SPECIAL DIAGNOSTIC', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (74, 'department', 'SURGICAL', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (75, 'department', 'TALENT MANAGEMENT', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (76, 'department', 'WARD 1', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (77, 'department', 'WARD 2', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (78, 'department', 'WARD 3', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (79, 'department', 'WARD 5', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (80, 'department', 'WARD 6', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (81, 'department', 'WARD 7A', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (82, 'department', 'WARD 7B', '', '', '', '2019-05-06 11:39:08', '2019-05-06 11:39:08', 'active'),
                                                (83, 'staffgroup', 'STAFF', '', '', '', '2019-05-06 12:29:48', '2019-05-06 12:29:48', 'active'),
                                                (84, 'staffgroup', 'STAFFMGMT', '', '', '', '2019-05-06 12:29:48', '2019-05-06 12:29:48', 'active'),
                                                (85, 'staffgroup', 'DOCTOR', '', '', '', '2019-05-06 12:29:48', '2019-05-06 12:29:48', 'active'),
                                                (86, 'staffgroup', 'STAFFHOS', '', '', '', '2019-05-06 12:29:48', '2019-05-06 12:29:48', 'active'),
                                                (230, 'doctor', 'DR TING CHEH SING', 'CS TING', '004', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (231, 'doctor', 'DR LOO VOON SAN', 'VS LOO', '007', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (232, 'doctor', 'DR CHEONG YOKE LEONG', 'YL CHEONG', '008', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (233, 'doctor', 'DATO DR SARJEET SINGH SIDHU', 'SARJEET', '010', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (234, 'doctor', 'DR F S MALHI', 'MALHI', '011', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (235, 'doctor', 'DR C S N NAGARA', 'NAGARA', '012', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (236, 'doctor', 'DR TEOH SOONG KEE', 'SK TEOH', '013', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (237, 'doctor', 'DATO DR RANJOTH SINGH', 'RANJOTH', '016', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (238, 'doctor', 'DR FOO KOK CHENG', 'KC FOO', '017', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (239, 'doctor', 'DR OOI TIAN SU', 'TS OOI', '018', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (240, 'doctor', 'DR CHOW SIANG YONG', 'SY CHOW', 'DR0306', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (241, 'doctor', 'DR AHMAD ADLAN', 'ADLAN', '020', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (242, 'doctor', 'DATO DR K S SIVANATHAN', 'SIVA', '022', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (243, 'doctor', 'DR RAVEENDRAN', 'RAVEEN', '023', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (244, 'doctor', 'DR TAI KIM TENG', 'KT TAI', '024', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (245, 'doctor', 'DR LEONG CHIN LENG', 'CL LEONG', 'DR0009', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (246, 'doctor', 'DR ENJER SINGH', 'ENJER', '026', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (247, 'doctor', 'DR LOH CHOONG SING', 'CS LOH', 'DR0033', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (248, 'doctor', 'DR T VISHVANATHAN', 'VISHVA', 'DR0030', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (249, 'doctor', 'DR LEE YOOI CHYUN', 'YC LEE', '031', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (250, 'doctor', 'DR (MRS) PATHMAKANTHAN', 'MRS KANTHAN', '033', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (251, 'doctor', 'DR LAM FOOK SHIN', 'FS LAM', 'DR0007', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (252, 'doctor', 'DR LEE MUN TOONG', 'MT LEE', '035', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (253, 'doctor', 'DR LEE MUN WAI', 'MW LEE', '036', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (254, 'doctor', 'DR AMINUDIN RAHMAN BIN MOHD MYDIN', 'AMIN R', '037', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (255, 'doctor', 'DR YOONG FOOK NGIAN', 'FN YOONG', '041', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (256, 'doctor', 'DR GEORGE THAVARAJAH', 'GEORGE THAV', 'DR0302', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (257, 'doctor', 'DR SELVAKUMAR A/L SELVARAJAH', 'SELVAKUMAR', '045', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (258, 'doctor', 'DATO DR GURDEEP SINGH MANN', 'GURDEEP', '046', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (259, 'doctor', 'DATO DR M MAJUMDER', 'MAJUMDER', '051', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (260, 'doctor', 'DR HENRY FOONG BOON BEE', 'HENRY FOONG', '053', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (261, 'doctor', 'DATO DR SUSHIL KUMAR RATTI', 'SUSHIL', '054', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (262, 'doctor', 'DR USHA DEVY', 'USHA', 'DR0027', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (263, 'doctor', 'DR DIVAKARAN T. GOVINDAN', 'DIVA', 'DR0029', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (264, 'doctor', 'DR SHARIFAH HALIMAH', 'SHARIFAH', '063', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (265, 'doctor', 'DR ARUKU NAIDU', 'ARUKU', '064', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (266, 'doctor', 'DR JAPARAJ A/L ROBERT PETER', 'JAPARAJ', '065', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (267, 'doctor', 'DR RAOOFAH BINTI MOHAMED AMIR', 'RAOOFAH', '066', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (268, 'doctor', 'DATO DR YEOH BENG SAN', 'BS YEOH', '072', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (269, 'doctor', 'DR JASMINE LAU YOKE CHIN', 'JASMINE', '073', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (270, 'doctor', 'DR YEE KHIM HOE', 'KH YEE', '074', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (271, 'doctor', 'DR KULDEEP SINGH', 'KULDEEP', '075', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (272, 'doctor', 'DR TANG YEW MUN', 'YM TANG', '076', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (273, 'doctor', 'DR LIM TI BIAN', 'TB LIM', '077', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (274, 'doctor', 'DR DAVID MANICKAM', 'DAVID M', '078', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (275, 'doctor', 'DR ADELINE TAN', 'ADELINE', '079', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (276, 'doctor', 'DR LIM WEE YOONG', 'WY LIM', 'DR0309', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (277, 'doctor', 'DR CHUNG SIN FAH', 'SF CHUNG', 'DR0304', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (278, 'doctor', 'DATO DR YEOH HUAT CHEE', 'DATO YEOH HC', 'DR0013', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (279, 'doctor', 'DR GIRITHARAN', 'GIRI', '084', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (280, 'doctor', 'DR GURCHARAN SINGH', 'GURCHARAN', '086', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (281, 'doctor', 'DR WILLIAM TAN', 'WILLAM T', '087', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (282, 'doctor', 'DR FOO JOO EE', 'JE FOO', '088', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (283, 'doctor', 'DR GURMAIL SINGH', 'GURMAIL', '089', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (284, 'doctor', 'DR LEE BOON CHYE', 'BC LEE', 'DR0022', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (285, 'doctor', 'DR LIEW SAN FOI', 'SF LIEW', '091', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (286, 'doctor', 'DR MEOR AHMAD B HJ MOHD KHAN', 'MEOR A', '093', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (287, 'doctor', 'DR LEONG OON KEONG', 'OK LEONG', '094', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (288, 'doctor', 'DR KAMARUDIN BIN S. MOHD.', 'KAMARUDIN', 'DR0028', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (289, 'doctor', 'DATO DR ANDREW CHUA', 'ADNREW C', '096', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (290, 'doctor', 'DR SHANMUGARAJAH RAJENDRA', 'SHANMUGARA', '099', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (291, 'doctor', 'DR SINTHAMONEY', 'SINTHAMONEY', '100', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (292, 'doctor', 'DR TAN ENG KEONG', 'EK TAN', '101', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (293, 'doctor', 'DR CHOONG CHOON HOOI', 'CH CHOONG', '103', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (294, 'doctor', 'DR LU LUAN', 'LU LUAN', 'DR0005', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (295, 'doctor', 'DR YUEN WAI MUN', 'WM YUEN', '113', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (296, 'doctor', 'DR WONG SEAK KHOON', 'SK WONG', '115', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (297, 'doctor', 'DR NG THENG CHUN', 'TC NG', 'DR0012', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (298, 'doctor', 'DR FOO CHANG LIM', 'CL FOO', 'DR0025', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (299, 'doctor', 'DR CHIN YOW WEN', 'YW CHIN', 'DR0016', '', '2019-05-11 09:28:01', '2019-05-11 09:28:01', 'active'),
                                                (300, 'doctor', 'DR KALIDASAN A/L GOVINDAN', 'KALIDASAN', '119', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (301, 'doctor', 'DR DING CHEK LANG', 'CL DING', '200', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (302, 'doctor', 'DATO DR R GUNASEGARAN', 'DATO GUNA', 'DR0307', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (303, 'doctor', 'DR KOH GUAN CHAI', 'GC KOH', 'DR0015', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (304, 'doctor', 'DR FAUZIAH BINTI KHAIRUDDIN', 'FAUZIAH', '203', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (305, 'doctor', 'DATO DR M SUBRAMANIAM', 'SUBRAMANIAM', '204', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (306, 'doctor', 'DR GO KUAN WENG', 'KW GO', 'DR0019', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (307, 'doctor', 'DR LIM YANG KWANG', 'YK LIM', '206', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (308, 'doctor', 'DR AMARJIT SINGH', 'AMARJIT', '210', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (309, 'doctor', 'DR FOONG YI KWAN', 'YK FOONG', 'DR0032', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (310, 'doctor', 'DR TAN HUAT CHAI', 'HC TAN', '220', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (311, 'doctor', 'DR HASRAL BIN NOOR HASNI', 'HASRAL', 'DR0017', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (312, 'doctor', 'DR ONG TEONG OON', 'TO ONG', 'DR0008', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (313, 'doctor', 'DR DEV TEO CIN NEE', 'DEV TEO', 'DR0014', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (314, 'doctor', 'DATO DR FADZLI CHEAH', 'FADZLI C', 'DR0021', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (315, 'doctor', 'DATO DR ZAKARIA B ABDUL KADIR', 'ZAKARIA', 'DR0018', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (316, 'doctor', 'DR CHEANG CHEE KEONG', 'CK CHEANG', '305', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (317, 'doctor', 'DR GURMIT KAUR', 'GURMIT', '401', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (318, 'doctor', 'DR ESTHER GUNASELI', 'ESTHER G', '402', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (319, 'doctor', 'DR DAVID YEO', 'DAVID YEO', '501', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (320, 'doctor', 'DATO DR JENAGARATNAM', 'JENA', '502', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (321, 'doctor', 'DR CHEW SWEE SEOK', 'SS CHEW', '506', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (322, 'doctor', 'DATO DR MAHADEVAN', 'MAHADEVAN', '507', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (323, 'doctor', 'GENERAL PRACTITIONER', 'GP', '508', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (324, 'doctor', 'OUTSIDE CONSULTANT', 'NONRESIDENT', '510', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (325, 'doctor', 'DR SUMATHI', 'SUMATHI', '512', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (326, 'doctor', 'LOCUM', 'LOCUM', '513', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (327, 'doctor', 'DR SAMUEL YU', 'SAMUEL YU', '514', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (328, 'doctor', 'DR SHAIDAN SULAIMAN', 'SHAIDAN', '515', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (329, 'doctor', 'DR AZHAR BIN ABDUL RAHMAN @RODZI', 'AZHAR', '516', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (330, 'doctor', 'MEDICAL OFFICER', 'MO', '518', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (331, 'doctor', 'DR AZLIN AZIZAN', 'AZLIN AZIZAN', '520', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (332, 'doctor', 'DR JOHN ANATHAM', 'JOHN ANA', '523', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (333, 'doctor', 'DR SIVABALAN', 'SIVABALAN', '524', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (334, 'doctor', 'DR (MRS) RITA OOI', 'RITA OOI', '525', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (335, 'doctor', 'DR YAP CHEE SENG', 'CS YAP', '526', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (336, 'doctor', 'DR KOH CHAN SING', 'CS KOH', '527', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (337, 'doctor', 'DR HALILI RAHMAT', 'HALILI', '529', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (338, 'doctor', 'DR NOOR AZMAN', 'NOOR AZMAN', '530', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (339, 'doctor', 'DR GANESA RASA', 'GANESA', '531', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (340, 'doctor', 'DR LUM WAN WEI', 'WW LUM', 'DR0011', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (341, 'doctor', 'DR LUIS CHEN SHIAN LIANG', 'LUIS CHEN', 'DR0020', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (342, 'doctor', 'DR GOH PAIK KEE', 'PK GOH', '534', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (343, 'doctor', 'DR TAY KUO SHIEN', 'KS TAY', '535', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (344, 'doctor', 'DR ONG KEE YIN', 'KY ONG', '536', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (345, 'doctor', 'DR YEOH THIAM LONG', 'TL YEOH', 'DR0024', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (346, 'doctor', 'DR CHAN CHEE HOE', 'CH CHAN', '538', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (347, 'doctor', 'DR SENTHI MALAR', 'SENTHI', '539', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (348, 'doctor', 'DR WONG WOAN YIING', 'WY WONG', 'DR0010', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (349, 'doctor', 'DR PERDAMEN SINGH', 'PERDAMEN', '541', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (350, 'doctor', 'DR WONG CHOON HENG', 'CH WONG', 'DR0002', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (351, 'doctor', 'DR WONG POH YEE', 'PY WONG', '543', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (352, 'doctor', 'DR VINOD KUMAR ISHWARLAL', 'VINOD', '544', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (353, 'doctor', 'DR AZIDAWATI BT ADNAN', 'AZIDAWATI', '545', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (354, 'doctor', 'DR CHANDRA SEGAR A/L C.BALAN', 'CHANDRA', '546', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (355, 'doctor', 'DR AHMAD SOBRI MUDA', 'AHMAD S', '547', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (356, 'doctor', 'DR AGNES HENG YOKE HUI', 'AGNES HENG', '548', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (357, 'doctor', 'DR AZURAINE ANOR BASAH', 'AZURAINE A', '549', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (358, 'doctor', 'DR KHAIRUL AZMI ABD KADIR', 'KHARIUL A', '550', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (359, 'doctor', 'DR CHANG SEIT KIM', 'SK CHANG', '551', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (360, 'doctor', 'DR MAHENDRA MANO', 'MANO', 'DR0006', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (361, 'doctor', 'DR ESKANDAR @ ZULKARNIAN', 'ESKANDAR', 'DR0031', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (362, 'doctor', 'DR DEVANANDHINI KRISNAN', 'DEVANAND', '504', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (363, 'doctor', 'DR LOW SHIAU CHUAN', 'SC LOW', '503', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (364, 'doctor', 'DR ZAMZIDA YUSOFF', 'ZAMZIDA', '068', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (365, 'doctor', 'DR NAWAZ HUSSAIN', 'NAWAZ', '207', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (366, 'doctor', 'DR ONG LIEH BIN', 'ONG LB', 'DR0003', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (367, 'doctor', 'DR NOORASHIKIN BINTI MAAN', 'NOORASHIKIN', 'DR0004', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (368, 'doctor', 'DR NOORUL IZZAH', 'NOORUL', '553', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (369, 'doctor', 'DR BISWA MOHAN BISWAL', 'BISWA', '570', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (370, 'doctor', 'DR CHEONG HON KIN', 'HK CHEONG', 'DR0023', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (371, 'doctor', 'DR PHANG HOE FATT', 'HF PHANG', '150', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (372, 'doctor', 'DR KO CHUN SENG', 'CS KO', 'DR0001', '', '2019-05-11 09:28:02', '2019-05-11 09:28:02', 'active'),
                                                (373, 'treatment', 'Medication', 'OUT', '', '', '2019-05-14 15:54:01', '2019-05-14 15:54:01', 'active'),
                                                (374, 'treatment', 'Normal', 'IN', '', '', '2019-05-14 15:54:01', '2019-05-14 15:54:01', 'active'),
                                                (375, 'treatment', 'Gynaecology', 'IN', '', '', '2019-05-14 15:54:30', '2019-05-14 15:54:30', 'active'),
                                                (376, 'treatmentlist', 'Covered', 'Gynaecology', '', '', '2019-05-14 15:55:22', '2019-05-14 15:55:22', 'active'),
                                                (377, 'treatmentlist', 'Kiv', 'Gynaecology', '', '', '2019-05-14 15:55:28', '2019-05-14 15:55:28', 'active'),
                                                (378, 'treatmentlist', 'Not Covered', 'Gynaecology', '', '', '2019-05-14 15:55:38', '2019-05-14 15:55:38', 'active'),
                                                (379, 'treatmentlist', 'General', 'Normal', '', '', '2019-05-14 15:56:04', '2019-05-14 15:56:04', 'active'),
                                                (380, 'treatmentlist', 'Ortho', 'Normal', '', '', '2019-05-14 15:56:13', '2019-05-14 15:56:13', 'active'),
                                                (381, 'treatmentlist', 'Delivery', 'Normal', '', '', '2019-05-14 15:56:24', '2019-05-14 15:56:24', 'active'),
                                                (382, 'treatmentlist', 'Neuro', 'Normal', '', '', '2019-05-14 15:56:31', '2019-05-14 15:56:31', 'active'),
                                                (383, 'treatmentlist', 'Eye', 'Normal', '', '', '2019-05-14 15:56:39', '2019-05-14 15:56:39', 'active'),
                                                (384, 'treatmentlist', 'ENT', 'Normal', '', '', '2019-05-14 15:56:49', '2019-05-14 15:56:49', 'active'),
                                                (385, 'treatmentlist', 'Gastro', 'Normal', '', '', '2019-05-14 15:56:59', '2019-05-14 15:56:59', 'active'),
                                                (386, 'treatmentlist', 'Nephro', 'Normal', '', '', '2019-05-14 15:57:11', '2019-05-14 15:57:11', 'active'),
                                                (387, 'treatmentlist', 'Cardiology', 'Normal', '', '', '2019-05-14 15:57:24', '2019-05-14 15:57:24', 'active'),
                                                (388, 'treatmentlist', 'Urology', 'Normal', '', '', '2019-05-14 15:57:36', '2019-05-14 15:57:36', 'active'),
                                                (389, 'treatmentlist', 'Endo', 'Normal', '', '', '2019-05-14 15:57:45', '2019-05-14 15:57:45', 'active'),
                                                (390, 'treatmentlist', 'Oncology', 'Normal', '', '', '2019-05-14 15:58:52', '2019-05-14 15:58:52', 'active'),
                                                (391, 'rolecategory', 'Administrator', 'master', '', '', '2019-05-14 15:58:52', '2019-05-14 15:58:52', 'active'),
                                                (392, 'rolecategory', 'hos', '', '', '', '2019-05-20 06:36:40', '2019-05-20 10:39:03', 'active'),
                                                (393, 'rolecategory', 'TMa', '', '', '', '2019-05-20 06:36:48', '2019-05-20 10:13:33', 'deleted'),
                                                (394, 'rolecategory', 'staff', '', '', '', '2019-05-20 06:36:56', '2019-05-20 06:36:56', 'active'),
                                                (395, 'rolecategory', 'Try', '', '', '', '2019-05-20 06:39:23', '2019-05-20 06:39:23', 'deleted'),
                                                (396, 'rolecategory', 'Testing', '', '', '', '2019-05-20 08:40:36', '2019-05-20 08:40:36', 'deleted'),
                                                (397, 'rolecategory', 'doctor', '', '', '', '2019-05-20 10:36:18', '2019-05-20 10:36:18', 'active'),
                                                (398, 'categorylevel', 'baru', '10000', '100000', '', '2019-06-25 09:58:09', '2019-06-25 10:25:09', 'deleted'),
                                                (399, 'doctor', 'Hazwan', 'NAH', '2046', '', '2019-06-25 10:05:05', '2019-06-25 10:25:41', 'deleted'),
                                                (400, 'staffgroup', 'STAFFBARU', '', '', '', '2019-06-25 10:10:44', '2019-06-25 10:25:15', 'deleted'),
                                                (401, 'department', 'Kuala Lumpur', '', '', '', '2019-06-25 10:14:35', '2019-06-25 10:25:21', 'deleted'),
                                                (402, 'treatment', 'CUCUK', '', '', '', '2019-06-25 10:14:48', '2019-06-25 10:25:46', 'deleted'),
                                                (403, 'treatmentlist', 'BERANAK', '', '', '', '2019-06-25 10:14:58', '2019-06-25 10:25:50', 'deleted'),
                                                (404, 'treatmentstat', 'HUTANG', '', '', '', '2019-06-25 10:15:07', '2019-06-25 10:25:53', 'deleted'),
                                                (405, 'treatment', 'Critical', 'IN', '', '', '2019-05-14 15:54:30', '2019-05-14 15:54:30', 'active'),
                                                (406, 'treatmentlist', 'Heart Attack', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (407, 'treatmentlist', 'Surgery of Coronary Artery Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (408, 'treatmentlist', 'Other Serious Coronary Artery Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (409, 'treatmentlist', 'Heart Valve Surgery', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (410, 'treatmentlist', 'Surgery of Aorta', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (411, 'treatmentlist', 'Angioplasty and Other Invasive Treatments for Coronary Artery Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (412, 'treatmentlist', 'Coronary Atherectomy', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (413, 'treatmentlist', 'Primary Pulmonary Arterial', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (414, 'treatmentlist', 'Hypertension', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (415, 'treatmentlist', 'Muscular Dystrophy', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (416, 'treatmentlist', 'Multiple Sclerosis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (417, 'treatmentlist', 'Motor Neurone Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (418, 'treatmentlist', 'Parkinsons Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (419, 'treatmentlist', 'Alzheimers Disease/Irreversible Organic Degenerative Brain Disorders', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (420, 'treatmentlist', 'Stroke', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (421, 'treatmentlist', 'Coma', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (422, 'treatmentlist', 'Poliomyelitis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (423, 'treatmentlist', 'Loss of Independent Existence', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (424, 'treatmentlist', 'Blindness', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (425, 'treatmentlist', 'Loss of Hearing/Deafness', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (426, 'treatmentlist', 'Loss of Speech', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (427, 'treatmentlist', 'Major Organ Transplant', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (428, 'treatmentlist', 'Major Burns', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (429, 'treatmentlist', 'Terminal Illness', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (430, 'treatmentlist', 'Kidney Failure', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (431, 'treatmentlist', 'Chronic Lung Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (432, 'treatmentlist', 'Systematic Lupus Erythematosus with Lupus Nephritis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (433, 'treatmentlist', 'Medullary Cystic Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (434, 'treatmentlist', 'Chronic Liver Disease', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (435, 'treatmentlist', 'Fulminant Viral Hepatitis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (436, 'treatmentlist', 'Encephalitis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (437, 'treatmentlist', 'Benign Brain Tumor', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (438, 'treatmentlist', 'Bacterial Meningitis', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (439, 'treatmentlist', 'Apallic Syndrome', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (440, 'treatmentlist', 'Cancer', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (441, 'treatmentlist', 'Aplastic Anemia', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (442, 'treatmentlist', 'AIDS due to Blood Transfusion', 'Critical', '', '', '2019-07-01 09:38:46', '2019-07-01 09:38:46', 'active'),
                                                (443, 'treatment', 'Try je', 'hah', '', '', '2019-07-01 09:47:37', '2019-07-01 09:47:40', 'deleted'),
                                                (444, 'treatmentlist', 'Sakiit Tua', 'ha', '', '', '2019-07-01 09:47:56', '2019-07-01 09:48:02', 'deleted');");
            
            $index_tbl_setting = mysql_query("ALTER TABLE `tbl_setting` ADD PRIMARY KEY (`d_id`);");
            $alter_tbl_setting = mysql_query("ALTER TABLE `tbl_setting` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;");
            
            
            
            if(!$tbl_setting){echo "tbl_setting = ".mysql_error(); }else{if(!$insert_tbl_setting){echo "insert tbl_setting = ".mysql_error(); }else{if(!$index_tbl_setting){echo "index tbl_setting = ".mysql_error(); }else{if(!$alter_tbl_setting){echo "alter tbl_setting = ".mysql_error(); }else{echo "tbl_setting = success!!<br>";$ok="ok";}}}}
            
            $page = mysql_query("CREATE TABLE `tbl_page` (`did` int(11) NOT NULL,`pageTitle` varchar(30) NOT NULL DEFAULT '',`pageTitleSmall` varchar(30) NOT NULL DEFAULT '',`pageName` varchar(50) NOT NULL DEFAULT '',`pageGroup` varchar(30) NOT NULL DEFAULT '',`pageDir` varchar(50) NOT NULL DEFAULT '',`status` varchar(20) NOT NULL DEFAULT '') ENGINE=InnoDB DEFAULT CHARSET=latin1");
            $insert_page = mysql_query("INSERT INTO `tbl_page` (`did`, `pageTitle`, `pageTitleSmall`, `pageName`, `pageGroup`, `pageDir`, `status`) VALUES
                                        (101, 'Dashboard', '', 'Dashboard', 'Dashboard A', 'dashboard', 'active'),
                                        (102, 'Charge Entry', 'Manual Charge Entry', 'Charge Entry', 'Charge', 'chargeEntry', 'active'),
                                        (103, 'List of Staff', '', 'List of Staff', 'Staff', 'listofStaff', 'active'),
                                        (104, 'Staff Registery', '', 'Staff Registery', 'Staff', 'staffregistery', 'active'),
                                        (105, 'Charge History', '', 'Charge History', 'Charge', 'chargehistory', 'active'),
                                        (106, 'Staff Medical History', '', 'Staff Medical History', 'Staff', 'staffmedicalhistorya', 'active'),
                                        (107, 'Daily Treatment', '', 'Daily Treatment', 'BO', 'treatmentcheck', 'active'),
                                        (110, 'Relative', '', 'Relative Area', 'Dependents', 'dependentsarea', 'active'),
                                        (111, 'Dashboard', 'Head of Services', 'Dashboard', 'Dashboard H', 'hosdashboard', 'active'),
                                        (112, 'Staff Installment', '', 'Staff Installment', 'BO', 'staffinstallment', 'active'),
                                        (113, 'Installment Log', '', 'Installment Log', 'BO', 'installmentlog', 'active'),
                                        (114, 'Dashboard', 'cashier', 'Dashboard', 'Dashboard C', 'cashierdashboard', 'active'),
                                        (200, 'Dashboard', 'Staff', 'Dashboard', 'Dashboard S', 'staffdashboard', 'active'),
                                        (201, 'Staff Medical History', 'Staff', 'Staff Medical History', 'StaffMedHis', 'staffmedicalhistory', 'active'),
                                        (700, 'Role Privileges', 'Admin Control', 'Role', 'Admin', 'roleprivileges', 'active'),
                                        (701, 'Master Setting', '', 'Admin Control', 'Admin', 'mastersetting', 'active');");
            $index_page = mysql_query("ALTER TABLE `tbl_page` ADD PRIMARY KEY (`did`);");
            $alter_page = mysql_query("ALTER TABLE `tbl_page` MODIFY `did` int(11) NOT NULL AUTO_INCREMENT;");
            
            
            if($ok == "ok"){
                if(!$page){echo "tbl_page = ".mysql_error(); }else{if(!$insert_page){echo "insert tbl_page = ".mysql_error(); }else{if(!$index_page){echo "index tbl_page = ".mysql_error(); }else{if(!$alter_page){echo "alter tbl_page = ".mysql_error(); }else{echo "tbl_page = success!!<br>";$okk = "ok";}}}}
            }
            
            
            
            $privileges = mysql_query("CREATE TABLE `tbl_privileges` (
                                      `d_id` int(11) NOT NULL,
                                      `d_type` varchar(20) NOT NULL DEFAULT '',
                                      `d_value` varchar(10) NOT NULL DEFAULT '',
                                      `d_role` varchar(10) NOT NULL DEFAULT '',
                                      `d_created` varchar(30) NOT NULL DEFAULT ''
                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $insertprivileges = mysql_query("INSERT INTO `tbl_privileges` (`d_id`, `d_type`, `d_value`, `d_role`, `d_created`) VALUES
                                    (99, 'role', '394', '394', '2019-06-21 10:28:41'),
                                    (100, 'page', '200', '394', '2019-06-21 10:28:41'),
                                    (101, 'page', '201', '394', '2019-06-21 10:28:41'),
                                    (176, 'role', '397', '397', '2019-07-03 09:25:27'),
                                    (177, 'page', '111', '397', '2019-07-03 09:25:27'),
                                    (178, 'page', '201', '397', '2019-07-03 09:25:27'),
                                    (204, 'role', '392', '392', '2019-08-09 11:07:47'),
                                    (205, 'page', '111', '392', '2019-08-09 11:07:47'),
                                    (206, 'page', '201', '392', '2019-08-09 11:07:47'),
                                    (207, 'role', '391', '391', '2019-08-19 14:56:24'),
                                    (208, 'page', '101', '391', '2019-08-19 14:56:24'),
                                    (209, 'page', '102', '391', '2019-08-19 14:56:24'),
                                    (210, 'page', '105', '391', '2019-08-19 14:56:24'),
                                    (211, 'page', '103', '391', '2019-08-19 14:56:24'),
                                    (212, 'page', '104', '391', '2019-08-19 14:56:24'),
                                    (213, 'page', '106', '391', '2019-08-19 14:56:24'),
                                    (214, 'page', '107', '391', '2019-08-19 14:56:24'),
                                    (215, 'page', '112', '391', '2019-08-19 14:56:24'),
                                    (216, 'page', '113', '391', '2019-08-19 14:56:24'),
                                    (217, 'page', '110', '391', '2019-08-19 14:56:24'),
                                    (218, 'page', '114', '391', '2019-08-19 14:56:24'),
                                    (219, 'page', '700', '391', '2019-08-19 14:56:24'),
                                    (220, 'page', '701', '391', '2019-08-19 14:56:24');");
            $indexprivileges = mysql_query("ALTER TABLE `tbl_privileges` ADD PRIMARY KEY (`d_id`);");
            $alterprivileges = mysql_query("ALTER TABLE `tbl_privileges`MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;");
            
            if($okk == "ok"){
                if(!$privileges){echo "tbl_privileges = ".mysql_error(); }else{if(!$insertprivileges){echo "insert tbl_privileges = ".mysql_error(); }else{if(!$indexprivileges){echo "index tbl_privileges = ".mysql_error(); }else{if(!$alterprivileges){echo "alter tbl_privileges = ".mysql_error(); }else{echo "tbl_privileges = success!!";$okk = "ok";}}}}
            }
            
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------           
        }elseif($_POST['operationsql'] == 'phase2'){
//            create table page with data
            $installment = mysql_query("CREATE TABLE `tbl_installment` (
                                          `d_id` int(11) NOT NULL,
                                          `d_type` varchar(30) NOT NULL DEFAULT '',
                                          `d_datacode` varchar(30) NOT NULL DEFAULT '',
                                          `d_value1` varchar(200) NOT NULL DEFAULT '',
                                          `d_value2` varchar(200) NOT NULL DEFAULT '',
                                          `d_value3` varchar(200) NOT NULL DEFAULT '',
                                          `d_value4` varchar(1000) NOT NULL DEFAULT '',
                                          `d_value5` varchar(100) NOT NULL DEFAULT '',
                                          `d_created` varchar(30) NOT NULL DEFAULT '',
                                          `d_modified` varchar(30) NOT NULL DEFAULT '',
                                          `d_who` varchar(30) NOT NULL DEFAULT '',
                                          `d_status` varchar(30) NOT NULL DEFAULT ''
                                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indexinstallment = mysql_query("ALTER TABLE `tbl_installment` ADD PRIMARY KEY (`d_id`),ADD KEY `d_id` (`d_id`);");
            $alterinstallment = mysql_query("ALTER TABLE `tbl_installment` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;");
            
                
            $labor = mysql_query("CREATE TABLE `tbl_labor` (
                                  `d_id` int(11) NOT NULL,
                                  `d_staffID` varchar(10) NOT NULL DEFAULT '',
                                  `d_department` varchar(100) NOT NULL DEFAULT '',
                                  `d_employment` varchar(30) NOT NULL DEFAULT '',
                                  `d_resignation` varchar(30) NOT NULL DEFAULT '',
                                  `d_group` varchar(10) NOT NULL DEFAULT '',
                                  `d_designation` varchar(150) NOT NULL DEFAULT '',
                                  `d_category` varchar(10) NOT NULL,
                                  `d_created` varchar(30) NOT NULL DEFAULT '',
                                  `d_modified` varchar(30) NOT NULL DEFAULT '',
                                  `d_wstatus` varchar(30) NOT NULL DEFAULT '',
                                  `d_status` varchar(10) NOT NULL
                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indexlabor = mysql_query("ALTER TABLE `tbl_labor` ADD PRIMARY KEY (`d_id`),ADD KEY `d_staffID` (`d_staffID`);");
            $alterlabor = mysql_query("ALTER TABLE `tbl_labor` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;");
            
                
            $logs = mysql_query("CREATE TABLE `tbl_logs` (
                                  `d_id` int(11) NOT NULL,
                                  `d_type` varchar(200) NOT NULL DEFAULT '',
                                  `d_value` varchar(1000) NOT NULL DEFAULT '',
                                  `d_value2` varchar(1000) NOT NULL DEFAULT '',
                                  `d_value3` varchar(1000) NOT NULL DEFAULT '',
                                  `d_ip` varchar(50) NOT NULL DEFAULT '',
                                  `d_created` varchar(30) NOT NULL DEFAULT '',
                                  `d_status` varchar(30) NOT NULL DEFAULT ''
                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indexlogs = mysql_query("ALTER TABLE `tbl_logs` ADD PRIMARY KEY (`d_id`);");
            $alterlogs = mysql_query("ALTER TABLE `tbl_logs` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT;");
            
                    
            $profile = mysql_query("CREATE TABLE `tbl_profile` (
                                      `d_id` int(11) NOT NULL,
                                      `d_type` varchar(30) NOT NULL DEFAULT '',
                                      `d_labor` varchar(10) NOT NULL DEFAULT '',
                                      `d_available` varchar(10) NOT NULL DEFAULT '',
                                      `d_dependent` varchar(10) NOT NULL DEFAULT '',
                                      `d_name` varchar(500) NOT NULL DEFAULT '',
                                      `d_gender` varchar(10) NOT NULL DEFAULT '',
                                      `d_ic` varchar(12) NOT NULL DEFAULT '',
                                      `d_pnum` varchar(15) NOT NULL DEFAULT '',
                                      `d_email` varchar(100) NOT NULL DEFAULT '',
                                      `d_martial` varchar(10) NOT NULL DEFAULT '',
                                      `d_relstatus` varchar(30) NOT NULL DEFAULT '',
                                      `d_relic` varchar(12) NOT NULL DEFAULT '',
                                      `d_mrn` varchar(10) NOT NULL DEFAULT '',
                                      `d_created` varchar(30) NOT NULL DEFAULT '',
                                      `d_modified` varchar(30) NOT NULL DEFAULT '',
                                      `d_status` varchar(30) NOT NULL DEFAULT ''
                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indexprofile = mysql_query("ALTER TABLE `tbl_profile` ADD PRIMARY KEY (`d_id`);");
            $alterprofile = mysql_query("ALTER TABLE `tbl_profile` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6309;");
            
                
            $treatment = mysql_query("CREATE TABLE `tbl_treatment` (
                                      `d_id` int(11) NOT NULL,
                                      `d_staffID` varchar(10) NOT NULL DEFAULT '',
                                      `d_mrn` varchar(10) NOT NULL DEFAULT '',
                                      `d_episode` varchar(5) NOT NULL DEFAULT '',
                                      `d_charge` varchar(8) NOT NULL DEFAULT '',
                                      `d_day` varchar(3) NOT NULL DEFAULT '',
                                      `d_bill` varchar(20) NOT NULL DEFAULT '',
                                      `d_doc` varchar(10) NOT NULL DEFAULT '',
                                      `d_type` varchar(25) NOT NULL DEFAULT '',
                                      `d_treatment` varchar(25) NOT NULL DEFAULT '',
                                      `d_treatmentstatus` varchar(20) NOT NULL DEFAULT '',
                                      `d_treatmentlist` varchar(255) NOT NULL DEFAULT '',
                                      `d_treatmentconfirm` varchar(30) NOT NULL DEFAULT '',
                                      `d_exceedday` varchar(10) NOT NULL DEFAULT '',
                                      `d_exceedcharge` varchar(20) NOT NULL DEFAULT '',
                                      `d_exceedRemarks` varchar(500) NOT NULL DEFAULT '',
                                      `d_who` varchar(10) NOT NULL DEFAULT '',
                                      `d_created` varchar(30) NOT NULL DEFAULT '',
                                      `d_modified` varchar(30) NOT NULL DEFAULT '',
                                      `d_status` varchar(30) NOT NULL DEFAULT '',
                                      `d_vdate` varchar(30) NOT NULL DEFAULT '',
                                      `d_treatCheck` varchar(3) NOT NULL DEFAULT 'NO'
                                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indextreatment = mysql_query("ALTER TABLE `tbl_treatment` ADD PRIMARY KEY (`d_id`),ADD KEY `d_vdate` (`d_vdate`),ADD KEY `d_staffID` (`d_staffID`),ADD KEY `d_id` (`d_id`);");
            $altertreatment = mysql_query("ALTER TABLE `tbl_treatment` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44324;");
            
                
            $user = mysql_query("CREATE TABLE `tbl_user` (
                                  `d_id` int(11) NOT NULL,
                                  `d_role` varchar(50) NOT NULL DEFAULT '',
                                  `d_userid` varchar(30) NOT NULL DEFAULT '',
                                  `d_username` varchar(100) NOT NULL DEFAULT '',
                                  `d_password` varchar(500) NOT NULL DEFAULT '',
                                  `d_created` varchar(30) NOT NULL DEFAULT '',
                                  `d_modified` varchar(30) NOT NULL DEFAULT '',
                                  `d_loginsess` varchar(200) NOT NULL DEFAULT '',
                                  `d_status` varchar(30) NOT NULL DEFAULT ''
                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            $indexuser = mysql_query("ALTER TABLE `tbl_user` ADD PRIMARY KEY (`d_id`);");
            $alteruser = mysql_query("ALTER TABLE `tbl_user` MODIFY `d_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1437;");
            
            
            if(!$installment){echo "tbl_installment = ".mysql_error(); }else{if(!$indexinstallment){echo "insert tbl_installment = ".mysql_error(); }else{if(!$alterinstallment){echo "index tbl_installment = ".mysql_error(); }else{echo "tbl_installment = success!!<br>";$satu="ok";}}}
            
            if($satu == "ok"){
                if(!$labor){echo "tbl_labor = ".mysql_error(); }else{if(!$indexlabor){echo "insert tbl_labor = ".mysql_error(); }else{if(!$alterlabor){echo "index tbl_labor = ".mysql_error(); }else{echo "tbl_labor = success!!<br>";$dua="ok";}}}
            }
            
            if($dua == "ok"){
                if(!$logs){echo "tbl_log = ".mysql_error(); }else{if(!$indexlogs){echo "insert tbl_log = ".mysql_error(); }else{if(!$alterlogs){echo "index tbl_log = ".mysql_error(); }else{echo "tbl_log = success!!<br>";$tiga="ok";}}}
            }
            
            if($tiga == "ok"){
                if(!$profile){echo "tbl_profile = ".mysql_error(); }else{if(!$indexprofile){echo "insert tbl_profile = ".mysql_error(); }else{if(!$alterprofile){echo "index tbl_profile = ".mysql_error(); }else{echo "tbl_profile = success!!<br>";$empat="ok";}}}
            }
            
            if($empat == "ok"){
                if(!$treatment){echo "tbl_treatment = ".mysql_error(); }else{if(!$indextreatment){echo "insert tbl_treatment = ".mysql_error(); }else{if(!$altertreatment){echo "index tbl_treatment = ".mysql_error(); }else{echo "tbl_treatment = success!!<br>";$lima="ok";}}}
            }
            
            if($lima == "ok"){
                if(!$user){echo "tbl_user = ".mysql_error(); }else{if(!$indexuser){echo "insert tbl_user = ".mysql_error(); }else{if(!$alteruser){echo "index tbl_user = ".mysql_error(); }else{echo "tbl_user = success!!<br>";}}}
            }
            
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------   
            
        }elseif($_POST['operationsql'] == 'phase3'){
        
        $sql = mysql_query("SELECT * FROM staff");
	if(mysql_num_rows($sql)){
		while($row = mysql_fetch_assoc($sql)){
		
		//this is for table labor...
		$staffID = $row['staffid'];
		$department = getSetting($row['department']);
		$employment = $row['employment'];
		$resignation = $row['resignation'];
		$group = getSetting($row['staffgroup']);
		$designation = $row['designation'];
		$category = getSetting($row['category']);
		$status = $row['workstatus'];
        $active = getActive($row['active']);
		$labor = mysql_query("INSERT INTO tbl_labor(d_staffID, d_department, d_employment, d_resignation, d_group, d_designation, d_category, d_created, d_modified, d_wstatus, d_status)
											 VALUES('$staffID','$department','$employment','$resignation','$group','$designation','$category',NOW(),NOW(),'$status','$active')");
											 
		if(!$labor){
			echo "labor ".mysql_error();
		}else{
			//get did from tbl_labor
			$labordid = getLaborDid($staffID);
			//this table for table profile
			$type = "STAFF";
			$name = mysql_real_escape_string(trim($row['staffname']));
			$gender = $row['gender'];
			$ic = $row['staffic'];
			$phone = $row['phone'];
			$email = $row['email'];
			$maritial = $row['maritial'];
			$relstat = $row['relstat'];
			$relstatic = $row['relstatic'];
			$mrn = $row['mrn'];
			$profile = mysql_query("INSERT INTO tbl_profile(d_type, d_labor, d_dependent, d_name, d_gender, d_ic, d_pnum, d_email, d_martial, d_relstatus, d_relic, d_mrn, d_created, d_modified, d_status)VALUES('$type','$labordid','','$name','$gender','$ic','$phone','$email','$maritial','$relstat','$relstatic','$mrn',NOW(),NOW(),'$active')");
			
			if(!$profile){
				echo "profile ".mysql_error();
			}else{
				//get did from table profile
				$profiledid = getProfileDid($ic);
				$type = getTypeGroup($row['staffgroup']);
				$role = getRole($row['staffgroup']);
				$username = $row['staffic'];
				$password = $row['password'];
				//this is for table user
				$user = mysql_query("INSERT INTO tbl_user(d_userid, d_role, d_username, d_password, d_created, d_modified, d_status)VALUES('$profiledid','$role','$username','$password',NOW(),NOW(),'$active')");
				
				if(!$user){
					echo "user ".mysql_error();
				}else{
                    
                    
				    echo 'succeed <br>';
				}
			}
		}
		
		
		
		}
	}
    mysql_query("INSERT INTO tbl_user (d_id, d_role, d_userid, d_username, d_password, d_created, d_modified, d_loginsess, d_status) VALUES (NULL, '391', '', 'nahahmad', '25da36c3e140b05b26aa587ef2ac1c2fb303e150', '2019-08-22 10:57:29', '2019-08-22 10:57:29', '', 'active');");
        }elseif($_POST['operationsql'] == 'phase4'){
            
            $sql = mysql_query("SELECT * FROM relative"); 
            if(mysql_num_rows($sql)){ 
                while($row = mysql_fetch_assoc($sql)){

                    $staffic = getProfileDid($row['staffic']);
                    $relativename = mysql_real_escape_string(trim($row['relativename']));
                    $relativeic = $row['relativeic'];
                    $relativemrn = $row['relativemrn'];
                    $relativegender = $row['relativegender'];
                    $relationship = $row['relationship'];
                    $active = getActive($row['active']);

                    $dependent = mysql_query("INSERT INTO tbl_profile(d_type, d_dependent, d_name, d_ic, d_mrn, d_gender, d_relstatus, d_status, d_created, d_modified)VALUES('DEPENDENT','$staffic','$relativename','$relativeic','$relativemrn','$relativegender','$relationship','$active',NOW(),NOW())");

                    if(!$dependent){
                        echo mysql_error();
                    }else{
                        echo 'succeed <br>';
                    }
                }
            }
            
        }
    }

    if(isset($_POST['information'])){
        if($_POST['information'] == 'showTable'){
            echo "<button onclick='info()'>refresh</button><br><br><br>";
            
            $showTable = mysql_query("SHOW TABLES FROM smbsv2");
            
            if(!$showTable){
                echo mysql_error();
            }else{
                while ($row = mysql_fetch_row($showTable)) {
                      $tablename = $row[0];
                      $showTotal = mysql_num_rows(mysql_query("SELECT * FROM $tablename"));
                      echo "Table: $tablename\n = $showTotal<br>";

                    
                }
            }
        }
    }



    if(isset($_POST['exportas'])){
        if($_POST['exportas'] == 'export'){
            
            // Database configuration
            $host = "localhost";
            $username = "root";
            $password = "mysql";
            $database_name = "smbsv2";

            // Get connection object and set the charset
            $conn = mysqli_connect($host, $username, $password, $database_name);
            $conn->set_charset("utf8");


            // Get All Table Names From the Database
            $tables = array();
            $sql = "SHOW TABLES";
            $result = mysqli_query($conn, $sql);

            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
            
            $sqlScript = "";
            foreach ($tables as $table) {

                // Prepare SQLscript for creating table structure
                $query = "SHOW CREATE TABLE $table";
                $result = mysqli_query($conn, $query);
                $row = mysqli_fetch_row($result);

                $sqlScript .= "\n\n" . $row[1] . ";\n\n";


                $query = "SELECT * FROM $table";
                $result = mysqli_query($conn, $query);

                $columnCount = mysqli_num_fields($result);

                // Prepare SQLscript for dumping data for each table
                for ($i = 0; $i < $columnCount; $i ++) {
                    while ($row = mysqli_fetch_row($result)) {
                        $sqlScript .= "INSERT INTO $table VALUES(";
                        for ($j = 0; $j < $columnCount; $j ++) {
                            $row[$j] = $row[$j];

                            if (isset($row[$j])) {
                                $sqlScript .= '"' . $row[$j] . '"';
                            } else {
                                $sqlScript .= '""';
                            }
                            if ($j < ($columnCount - 1)) {
                                $sqlScript .= ',';
                            }
                        }
                        $sqlScript .= ");\n";
                    }
                }

                $sqlScript .= "\n"; 
            }
            
            if(!empty($sqlScript))
            {
                // Save the SQL script to a backup file
                $backup_file_name = $database_name . '_backup_' . time() . '.sql';
                $fileHandler = fopen($backup_file_name, 'w+');
                $number_of_lines = fwrite($fileHandler, $sqlScript);
                fclose($fileHandler); 

                // Download the SQL backup file to the browser
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($backup_file_name));
                ob_clean();
                flush();
                readfile($backup_file_name);
                exec('rm ' . $backup_file_name); 
            }
        }
    }


    if(isset($_POST['treatment'])){
        if($_POST['treatment'] == 'phase1'){
            $sql = "SELECT * FROM treatment WHERE uid BETWEEN 0 AND 9999";
            echo treatmeat($sql);
        }elseif($_POST['treatment'] == 'phase2'){
                $sql = "SELECT * FROM treatment WHERE uid BETWEEN 10000 AND 19999";
                echo treatmeat($sql);
            }elseif($_POST['treatment'] == 'phase3'){
                $sql = "SELECT * FROM treatment WHERE uid BETWEEN 20000 AND 29999";
                echo treatmeat($sql);
            }elseif($_POST['treatment'] == 'phase4'){
                $sql = "SELECT * FROM treatment WHERE uid BETWEEN 30000 AND 39999";
                echo treatmeat($sql);
            }elseif($_POST['treatment'] == 'phase5'){
                $sql = "SELECT * FROM treatment WHERE uid BETWEEN 40000 AND 50000";
            echo treatmeat($sql);
        }
    }

    if(isset($_POST['supervisor'])){
        if($_POST['supervisor'] == 'supervisor'){
            $sql = mysql_query("SELECT * FROM supervisor");
            if(mysql_num_rows($sql)){
                while($row = mysql_fetch_assoc($sql)){
                    $department = getSetting($row['department']);
                    $name = mysql_real_escape_string(trim($row['name']));
                    $level = getLevel($row['level']);
                    $username = mysql_real_escape_string(trim($row['username']));
                    $password = mysql_real_escape_string(trim($row['password']));
                    $active = getActive($row['active']);
                    
                    echo $level." = ".$row['level']."<br>";
                }
            }
        }
    }
?>
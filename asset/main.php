<?php
  class main
  {
    public function __construct()
    {
      //all main connector
      $this->includePHP('controller','connection');//connection DB
      $this->includePHP('controller','general');//general function
      $this->includePHP('controller','message');//message function
      $this->includePHP('controller','pagesession');//general function
    }

    //simple method function to call php file
    public function includePHP($folder,$filename){
      include('asset/'.$folder.'/'.$filename.'.php');
    }
  }


?>

<?php 
//OUTPATIENT
$yearOUT = masterID(masterID($getlabid, 'd_id', 'tbl_labor', 'd_category'), 'd_id', 'tbl_setting', 'd_value2');
$usageOUT = usage1('out',$getlabid);
$yearOUT1 = number_format($yearOUT,2);
$usageOUT1 = number_format($usageOUT,2);
$balanceOUT = number_format(balance1($yearOUT,$usageOUT),2);
$percentageOUT = percentage1($yearOUT,$usageOUT);
?>
  
   <div class="col-md-12 col-lg-12 col-xl-12">
    <div class="m-widget24" style="background:#c4e1f3b3;">
        <div class="m-widget24__item">
            <h4 class="m-widget24__title">
                Out-Patient
            </h4>
            <br>
            <span class="m-widget24__desc">
                Balance
            </span>
            <span class="m-widget24__stats m--font-primary">
                RM <?php echo $balanceOUT; ?>
            </span>
            <div class="m--space-10"></div>
            <div class="progress m-progress--sm">
                <div class="progress-bar m--bg-primary" role="progressbar" style="width: <?php echo $percentageOUT; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="m-widget24__change">
                Current Usage <b>RM <?php echo $usageOUT1; ?></b><br>
                Yearly Benefits <b>RM <?php echo $yearOUT1; ?></b>
            </span>
            <span class="m-widget24__number">
                <?php echo $percentageOUT; ?>
            </span>
        </div>
    </div>
</div>


<div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
    <table class="table m-table m-table--head-separator-metal">
        <thead>
            <tr>
                <th>
                    No
                </th>
                <th>
                    Date
                </th>
                <th>
                    Name
                </th>
                <th>
                    MRN
                </th>
                <th>
                    Amount(RM)
                </th>
                <th>
                    Remarks
                </th>
                <th>

                </th>
            </tr>
        </thead>
        <tbody style="background:#d3d3d34d;">
            <?php
                $bil = 1;
                $year = date('Y');
                $staffID = masterID($getlabid, 'd_id', 'tbl_labor', 'd_staffID');
                $outList = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_staffID = '$staffID' AND year(d_vdate)='$year' AND d_status = 'active'"); 
                if(mysql_num_rows($outList)){
                    while($outrow = mysql_fetch_assoc($outList)){ 
            ?>
            <tr>
                <th scope="row">
                    <?php echo $bil; ?>
                </th>
                <td>
                    <?php echo date('d-m-Y',strtotime($outrow['d_vdate'])); ?>
                </td>
                <td>
                    <?php echo masterID($outrow['d_mrn'], 'd_mrn', 'tbl_profile', 'd_name'); ?>
                </td>
                <td>
                    <?php echo $outrow['d_mrn']; ?>
                </td>
                <td>
                    <?php echo number_format($outrow['d_charge'],2); ?>
                </td>
                <td>
                    <?php echo $outrow['d_treatment']; if($outrow['d_treatlist'] != ''){ print ' - '.$outrow['d_treatlist']; } ?><br>
                            <?php if($outrow['d_exceedRemarks'] != ''){ echo "[ ".$outrow['d_exceedRemarks']." ]"; } ?>
                </td>
                <td>
                    <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis('<?php echo $outrow['d_id']; ?>','<?php echo $_POST['searchtype']; ?>','<?php echo $_POST['thevalue']; ?>')"></i></a>
                </td>
            </tr>
            <?php $bil++; }}else{ ?>
            <tr>
                <th scope="row" colspan="7" style="text-align:center;">
                    No treatment records found
                </th>
            </tr>
            <?php }?>

        </tbody>
    </table>
</div>

<?php 
//INPATIENT
$yearIN = masterID(masterID($getlabid, 'd_id', 'tbl_labor', 'd_category'), 'd_id', 'tbl_setting', 'd_value3');
$usageIN = usage1('in',$getlabid);
$balanceIN = number_format(balance1($yearIN,$usageIN),2);
$percentageIN = percentage1($yearIN,$usageIN);
?>

<div class="col-md-12 col-lg-12 col-xl-12">
    <div class="m-widget24" style="background:#f3efc4b3;">
        <div class="m-widget24__item">
            <h4 class="m-widget24__title">
               In-Patient (Benefit) 
            </h4>
            <br>
            <span class="m-widget24__desc">
                Balance
            </span>
            <span class="m-widget24__stats m--font-info">
                RM <?php echo $balanceIN; ?>
            </span>
            <div class="m--space-10"></div>
            <div class="progress m-progress--sm">
                <div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $percentageIN; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="m-widget24__change">
                Current Usage <b>RM <?php echo $usageIN; ?> </b><br>
                Yearly Benefits <b><?php echo $yearIN; ?></b>
            </span>
            <span class="m-widget24__number">
                <?php echo $percentageIN; ?>
            </span>
        </div>
    </div>
</div>


<div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
    <table class="table m-table m-table--head-separator-metal">
        <thead>
            <tr>
                <th>
                    No
                </th>
                <th>
                    Date
                </th>
                <th>
                    Name
                </th>
                <th>
                    MRN
                </th>
                <th>
                    Day
                </th>
                <th>
                    Amount(RM)
                </th>
                <th>
                    Remarks
                </th>
                <th>

                </th>
            </tr>
        </thead>
        <tbody style="background:#d3d3d34d;">
            <?php
                $bil = 1;
                $year = date('Y');
                $staffID = masterID($getlabid, 'd_id', 'tbl_labor', 'd_staffID');
                $inList = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_staffID = '$staffID' AND year(d_vdate)='$year' AND d_status = 'active'"); 
                if(mysql_num_rows($inList)){
                    while($inrow = mysql_fetch_assoc($inList)){ 
            ?>
            <tr>
                <th scope="row">
                    <?php echo $bil; ?>
                </th>
                <td>
                    <?php echo date('d-m-Y',strtotime($inrow['d_vdate'])); ?>
                </td>
                <td>
                    <?php echo masterID($inrow['d_mrn'], 'd_mrn', 'tbl_profile', 'd_name'); ?>
                </td>
                <td>
                    <?php echo $inrow['d_mrn']; ?>
                </td>
                <td>
                    <?php echo $inrow['d_day']; ?>
                </td>
                <td>
                    <?php echo number_format($inrow['d_charge'],2); ?>
                </td>
                <td>
                    <?php echo $inrow['d_treatment']; if($inrow['d_treatmentlist'] != ''){ print ' - '.$inrow['d_treatmentlist']; } ?><br>
                            <?php if($inrow['d_exceedRemarks'] != ''){ echo "[ ".$inrow['d_exceedRemarks']." ]"; } ?>
                </td>
                <td>
                    <a href="#" title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis('<?php echo $inrow['d_id']; ?>','<?php echo $_POST['searchtype']; ?>','<?php echo $_POST['thevalue']; ?>')"></i></a>
                </td>
            </tr>
            <?php $bil++; }}else{ ?>
            <tr>
                <th scope="row" colspan="8" style="text-align:center;">
                    No treatment records found
                </th>
            </tr>
            <?php }?>
        </tbody>
    </table>
</div>


<?php 
$outYear = $cont->getYearlyAmount('out', $labid);
$outUsage = $cont->usage1('out', $labid);
$outBal = $cont->balance1($outYear, $outUsage);
$inYear = $cont->getYearlyAmount('in', $labid);
$inUsage = $cont->usage1('in', $labid);
$inBal = $cont->balance1($inYear, $inUsage);

?>

<h6 style="margin-top:20px;text-decoration:underline;">Outpatient Benefits</h6>
<span style="font-size:13px;">
    Benefit: <b>RM <?php echo number_format($outYear,2); ?></b><br />
    Current Usage: <b>RM <?php echo number_format($outUsage,2); ?></b><br />
    Balance: <b>RM <?php echo number_format($outBal,2); ?></b><br />
</span>
<h6 style="margin-top:20px;text-decoration:underline;">Inpatient Benefits for Staff</h6>
<span style="font-size:13px;">
    Benefit: <b>RM <?php echo number_format($inYear,2); ?></b><br />
    Current Usage: <b>RM <?php echo number_format($inUsage,2); ?></b><br />
    Balance: <b>RM <?php echo number_format($inBal,2); ?></b><br />
</span>
<div style="border:1px solid black;padding:10px;margin-top:20px;">
    <h6 style="text-decoration:underline;">Dependents List</h6>
    <span style="font-size:13px;">
        <?php
        $bil++;
        $sql = mysql_query("SELECT * FROM tbl_profile WHERE d_dependent = '$proid' AND d_status = 'active'");
        if(mysql_num_rows($sql)){
            while($row = mysql_fetch_assoc($sql)){
    //            echo $bil.". ".$row['d_name']." <br>[<b>".$row['d_mrn']."</b>]<br>";
                echo $bil.". ".$row['d_name']." - ".$row['d_relstatus']."<br>";
                echo "<b>IC : </b>".$row['d_ic']."<br>";
                echo "<b>MRN : </b>".$row['d_mrn']."<br><br>";

                $bil++;
            }
        }
        ?>
    </span>  
</div>

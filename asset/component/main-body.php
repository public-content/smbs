<?php
  // $pageModel = $general->pageInfo($page,'pageDir');
  // $pageTitle = $general->pageInfo($page,'pageTitle');
  // $pageName = $general->pageInfo($page,'pageName');
  // $main->includePHP('model',$pageModel);
  // $cont = new $pageModel();
  // $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
  // $pageDir = 'system.php?p='.$_GET['p'];
?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
  <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-subheader ">
      <div class="d-flex align-items-center">
        <div class="mr-auto">
          <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
          <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
            <li class="m-nav__item m-nav__item--home">
              <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i>
              </a>
            </li>
            <li class="m-nav__separator">
              -
            </li>
            <li class="m-nav__item">
              <a href="<?php echo $pageDir ?>" class="m-nav__link">
                <span class="m-nav__link-text">
                  <?php echo $pageName ?>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="m-content">
    
    </div>
  </div>
</div>

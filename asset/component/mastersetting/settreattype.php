<div class="m-portlet m-portlet--unair">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Treatment Type
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button type="button" class="btn m-btn--pill btn-primary" data-toggle="modal" data-target="#addtrattype">Add New Type</button>
                </li>
                <li class="m-portlet__nav-item">
                    <a class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="removeAll('settreattype')">
                        <i class="la la-close"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-12" id="settreattype-noti" style="margin-top:20px;"></div>
    <div class="m-portlet__body" id="settreattype-body">

    </div>
</div>
<div class="modal fade" id="addtrattype" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Add Treatment
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="d_value-settreattype" class="form-control-label">
                            Treatment Name:
                        </label>
                        <input type="text" class="form-control m-input--solid" id="d_value-settreattype" value="">
                    </div>
                    <div class="form-group">
                        <label for="d_value-settreattype" class="form-control-label">
                            Category:
                        </label>
                        <select class="form-control m-input m-input--solid" id="d_value2-settreattype" name="d_value2-settreattype" required>
                            <option value="BLANK">
                                Choose...
                            </option>
                            <option value="IN">
                                IN
                            </option>
                            <option value="OUT">
                                OUT
                            </option>
                        </select>
                        <input type="hidden" class="form-control" id="d_value3-settreattype" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addProcess('settreattype','treatment')">
                    Add
                </button>
            </div>
        </div>
    </div>
</div>
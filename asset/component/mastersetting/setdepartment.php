<div class="m-portlet m-portlet--unair">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Department
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <button type="button" class="btn m-btn--pill btn-primary" data-toggle="modal" data-target="#adddepartment">Add New Department</button>
                </li>
                <li class="m-portlet__nav-item">
                    <a class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="removeAll('setdepartment')">
                        <i class="la la-close"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-12" id="setdepartment-noti" style="margin-top:20px;"></div>
    <div class="m-portlet__body"  id="setdepartment-body">
        
    </div>
</div>
<div class="modal fade" id="adddepartment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Add Department
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="d_value-setcategory" class="form-control-label">
                            Department Name:
                        </label>
                        <input type="text" class="form-control" id="d_value-setdepartment" value="">
                        <input type="hidden" class="form-control" id="d_value2-setdepartment" value="">
                        <input type="hidden" class="form-control" id="d_value3-setdepartment" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addProcess('setdepartment','department')">
                    Add
                </button>
            </div>
        </div>
    </div>
</div>
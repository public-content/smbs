<?php 
$outYear = $cont->getYearlyAmount('out', $labid);
$outUsage = $cont->usage1('out', $labid);
$outBal = $cont->balance1($outYear, $outUsage);
$inYear = 14;
$inUsage = $cont->usage2('in',$labid);
$inBal = $cont->balance1($inYear, $inUsage);

?>

<h6 style="margin-top:20px;text-decoration:underline;">Outpatient Benefits</h6>
<span style="font-size:13px;">
    Benefit: <b>RM <?php echo number_format($outYear,2); ?></b><br />
    Current Usage: <b>RM <?php echo number_format($outUsage,2); ?></b><br />
    Balance: <b>RM <?php echo number_format($outBal,2); ?></b><br />
</span>
<h6 style="margin-top:20px;text-decoration:underline;">Inpatient Benefits for Staff</h6>
<span style="font-size:13px;">
    Benefit: <b><?php echo $inYear; ?> Days</b><br />
    Current Usage: <?php echo $inUsage; ?> Days<b></b><br />
    Balance: <b><?php echo $inBal; ?> Days</b><br />
</span>
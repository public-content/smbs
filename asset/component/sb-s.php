<div class="col-lg-6">
    <div class="m-widget24" style="background:#c4e1f3b3;">
        <div class="m-widget24__item">
            <h4 class="m-widget24__title">
                Out-Patient
            </h4>
            <br>
            <span class="m-widget24__desc">
                Yearly Benefits
            </span>
            <span class="m-widget24__stats m--font-primary">
                RM <?php echo $cont->getYearlyAmount('out',$_SESSION['labid']); ?>
            </span>
            <div class="m--space-10"></div>
            <div class="progress m-progress--sm">
                <div class="progress-bar m--bg-primary" role="progressbar" style="width: <?php echo $cont->getPercentage('out',$_SESSION['labid']); ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="m-widget24__change">
                Current Usage <b>RM <?php echo $cont->getCurrentUsage('out',$_SESSION['labid']); ?></b><br>
                Balance <b>RM <?php echo $cont->getBalance('out',$_SESSION['labid']); ?></b>
            </span>
            <span class="m-widget24__number">
                <?php echo $cont->getPercentage('out',$_SESSION['labid']); ?>
            </span>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="m-widget24" style="background:#f3efc4b3;">
        <div class="m-widget24__item">
            <h4 class="m-widget24__title">
                In-Patient
            </h4>
            <br>
            <span class="m-widget24__desc">
                Yearly Benefits
            </span>
            <span class="m-widget24__stats m--font-info">
                <?php echo '14'; ?> Days
            </span>
            <div class="m--space-10"></div>
            <div class="progress m-progress--sm">
                <div class="progress-bar m--bg-info" role="progressbar" style="width: <?php echo $cont->getPercentageDays('in',$_SESSION['labid']); ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <span class="m-widget24__change">
                Current Usage <b><?php echo $cont->getCurrentUsageDays('in',$_SESSION['labid']); ?></b><br>
                Balance <b><?php echo $cont->getBalanceDays('in',$_SESSION['labid']); ?> Days</b>
            </span>
            <span class="m-widget24__number">
                <?php echo $cont->getPercentageDays('in',$_SESSION['labid']); ?>
            </span>
        </div>
    </div>
</div>
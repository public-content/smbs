<!DOCTYPE html>
<html lang="en">

<head>
    <title>Benefit Engines</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../test/bootstrap-4.3.1.min.css">
    <script src="../test/jquery-3.4.1.min.js"></script>
    <script src="../test/popper-1.14.7.min.js"></script>
    <script src="../test/bootstrap-4.3.1.min.js"></script>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3" style="background-color:lavender;"></div>
            <div class="col-lg-6" style="background-color:lavenderblush;">
                <h1 style="text-align:center;">This Is Benefit Engine</h1>
                <div style="margin-top:80px;">
                    
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" id="value" placeholder="value">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <select class="form-control" id="type">
                                <option>Choose ...</option>
                                <option value="mrn">MRN</option>
                                <option value="id">STAFF ID</option>
                                <option value="ic">IC NUMBER</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" onclick="submitValue()">Submit</button>
                        </div>
                    </div>
                    <div class="col-lg-12" id="display"></div>
                </div>
            </div>
            <div class="col-lg-3" style="background-color:lavender;"></div>
        </div>
    </div>
</body>

</html>
<script>
    function submitValue(){
        document.getElementById('display').innerHTML = "<div style='text-align:center;'>Loading...</div>";
        var value = document.getElementById('value').value;
        var type = document.getElementById('type').value;
        
        var dataString = { process : 'submit', value : value, type : type }
        $.ajax({
            type: "post",
            url: "display.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('display').innerHTML = html;
            }
        });
        return false;
    }
</script>

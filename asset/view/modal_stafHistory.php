<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
    
    function getMrn($col, $mrn){
        $sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_mrn = '$mrn'");
        $row = mysql_fetch_assoc($sql);
        return $row[$col];
    }

    function getStaffID($did){
		$sql = mysql_query("SELECT d_staffID FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_staffID'];
	}
?>

<?php if(isset($_POST['page']) == 'postDate'){ ?>
<?php $startDater = mysql_real_escape_string($_POST['value1']); ?>
<?php $startDate = date('Y-m-d', strtotime($startDater)); ?>
<?php $endDater = $_POST['value2']; ?>
<?php $endDate = date('Y-m-d', strtotime($endDater)); ?>

<div class="m-portlet m-portlet--bordered m-portlet--unair">
    <div class="m-portlet__body">
        <div class="col-lg-12">
            <table>
                <tr>
                    <td>Name</td>
                    <td class="col-1">:</td>
<!--                    <?php echo $startDater.'  '.$endDater;?>-->
                    <td> <?php echo $general->masterTake('tbl_profile', 'd_name', $_SESSION['proid']); ?></td>
                </tr>
                <tr>
                    <td>IC</td>
                    <td class="col-1">:</td>
                    <td> <?php echo $general->masterTake('tbl_profile', 'd_ic', $_SESSION['proid']); ?></td>
                </tr>
                <tr>
                    <td>Staff ID</td>
                    <td class="col-1">:</td>
                    <td> <?php echo $general->masterTake('tbl_labor', 'd_staffID', $_SESSION['labid']); ?></td>
                </tr>
                
            </table>
        </div>
        <div class="col-lg-12" style="border-bottom:1px lightgrey solid; margin:20px 0px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                Out-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Dept
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bil=1;
											$sesID = getStaffID($general->securestring('decrypt', $_SESSION['labid'])); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_status = 'active' AND d_staffID ='$sesID' AND d_vdate BETWEEN '$startDate' AND '$endDate' "); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
												?>
                    <tr>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                    </tr>
                    <?php $bil++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12" style="height:40px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                In-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Department
                        </th>
                        <th>
                            Days
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bill=1;
											$year='2019'; $sesID = getStaffID($general->securestring('decrypt', $_SESSION['labid'])); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_status = 'active' AND d_staffID ='$sesID' AND d_vdate BETWEEN '$startDate' AND '$endDate'"); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
								        ?>
                    <tr>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_day']; ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                    </tr>
                    <?php $bill++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<?php } ?>

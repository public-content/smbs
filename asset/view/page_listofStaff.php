<?php
  $pageModel = $general->pageInfo($page,'pageDir');
  $pageTitle = $general->pageInfo($page,'pageTitle');
  $pageName = $general->pageInfo($page,'pageName');
  $pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
  $main->includePHP('model',$pageModel);
  $main->includePHP('controller','jsonconverter');
  $cont = new $pageModel();
  $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
  $pageDir = 'system.php?p='.$_GET['p'];
?>
<style>
    div.tip {
        /*        border-bottom: 1px dashed;*/
        text-decoration: none
    }

    div.tip:hover {
        cursor: help;
        position: relative
    }

    div.tip span {
        display: none
    }

    div.tip:hover span {
        border: #c0c0c0 1px dotted;
        padding: 5px 20px 5px 5px;
        display: block;
        z-index: 100;
        background: url(../images/status-info.png) #000000 no-repeat 100% 5%;
        left: 0px;
        margin: 10px;
        width: 250px;
        position: absolute;
        top: 10px;
        text-decoration: none
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Staff List
                                    <small>

                                    </small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-form m-form--label-align-right ">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center" style="padding-top:15px;">
                                        <div class="col-md-4">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span><i class="la la-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="col-lg-12" id="noti">
                            
                        </div>
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="m_datatable" id="json_data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php include('js/data-ajax.php'); ?>
<script>
    function editUser(did) {
        //        var page = "<?php echo $cont->securestring('encrypt','104') ?>";
        //        var url = "system.php?p="+page;
        //        location.replace(url + '&r=' + did);
        var dataString = {
            function: "edit",
            did: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_listofstaff.php",
            data: dataString,
            cache: false,
            success: function(html) {
                var dir = html;
                location.replace(dir);
            }
        });
        return false;
    }

    function functiondid1(did) {
        if (confirm("DEACTIVATE this user ?")) {
            var dataString = {
                function: 'deactivate',
                did: did
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_listofstaff.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    alert(html);
                    location.reload(); 
                }
            });
            return false;
        } else {

        }
    }
    
    function functiondid2(did) {
        if (confirm("ACTIVATE this user ?")) {
            var dataString = {
                function: 'activate',
                did: did
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_listofstaff.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    alert(html);
                    location.reload(); 
                    
                }
            });
            return false;
        } else {

        }
    }
    
    function functiondid3(did) {
        if (confirm("DELETE this user ?")) {
            var dataString = {
                function: 'delete',
                did: did
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_listofstaff.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    alert(html);
                    location.reload(); 
                }
            });
            return false;
        } else {

        }
    }
</script>
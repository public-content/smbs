<?php
  $pageModel = $general->pageInfo($page,'pageDir');
  $pageTitle = $general->pageInfo($page,'pageTitle');
  $pageName = $general->pageInfo($page,'pageName');
  $pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
  $main->includePHP('model',$pageModel);
  $cont = new $pageModel();
  $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
//  $pageDir = 'system.php?p='.$_GET['p'];
  $pageDir = $_SERVER['REQUEST_URI'];
?>
<style>
    .red {
        color: red
    }

    ;
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName;?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-portlet m-portlet--bordered m-portlet--unair">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Staff Medical History
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <form>
                                    <?php $dnow=date('m/d/Y'); $dnow2=date('Y-m-d'); ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-form-label col-lg-2 col-sm-12">
                                            Date range
                                        </label>
                                        <div class="col-lg-6 col-md-9 col-sm-12">
                                            <div class="input-daterange input-group" id="m_datepicker_5">
                                                <input type="text" class="form-control m-input" name="startdate" id="startdate" value="<?php echo $dnow; ?>" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control" name="endate" id="endate" value="<?php echo $dnow; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-sm-12">
                                            <button type="button" class="btn btn-secondary" onclick="postDate()">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" id="getHistory">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    postDate();
    function postDate() {
        var startdate = document.getElementById('startdate').value;
        var enddate = document.getElementById('endate').value;
        var page = "postDate";

        var dataString = {
            val1: startdate,
            val2: enddate,
            page: "pageHistory"
        };
        document.getElementById('getHistory').innerHTML = "<div class='m-blockui' style='background:none;'><span><img src='images/system/gif/loading.gif' alt='' width='30' class='m-loader--brand' style='background:none;'></span><span>Please wait...</span></div>";
        $.ajax({
            type: "post",
            url: "asset/view/modal_treatmentcheck.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('getHistory').innerHTML = html;
                //                alert(stardate);
            }
        });
        return false;
    }
    
    function checkThis1(did){
       var dataString = {
            val1: did,
            action: "check"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_treatmentcheck.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('getHistory').innerHTML = html;
                postDate();
            }
        });
        return false;
    }
    
    function checkThis2(did){
        var dataString = {
            val1: did,
            action: "uncheck"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_treatmentcheck.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('getHistory').innerHTML = html;
                postDate();
            }
        });
        return false;
    }
    
    function postPrint(type){
        var startdate = document.getElementById('startdate').value;
        var enddate = document.getElementById('endate').value;
        var dataString = {
            val1: type,
            val2: startdate,
            val3: enddate,
            action: "printPage"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_treatmentcheck.php",
            data: dataString,
            cache: false,
            success: function(html) {
                console.log(dataString);
                window.open(html, "", "width=800,height=600,left=100,top=100,resizable=yes,scrollbars=yes");
                
            }
        });
        return false;
    }
</script>
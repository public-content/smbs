<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
    
    function getMrn($col, $mrn){
        $sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_mrn = '$mrn'");
        $row = mysql_fetch_assoc($sql);
        return $row[$col];
    }

    function getStaffID($did){
		$sql = mysql_query("SELECT d_staffID FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_staffID'];
	}

    function totalOutpatient($labid,$proid){
        $checkDep = totalDependent($proid);
        if($checkDep != 0){
            $result = getBenefit('out', $labid)*2;
        }else{
            $result = getBenefit('out', $labid);
        }
        return $result;
    }

    function totalInpatient($labid){
        $getEmployment = getEmployment($labid);
        if($getEmployment == 'old'){
            $result = "14 Days";
        }else{
            $result = "RM ".number_format(getBenefit('in', $labid), 2);
        }
        return $result;
    }

    function getEmployment($labid){
        $befordate = "2005-03-07";
        $sql = mysql_query("SELECT d_employment FROM tbl_labor WHERE d_id = '$labid'");
        $row = mysql_fetch_assoc($sql);
        $d_employment = $row['d_employment'];
        
        if(strtotime($d_employment) <= strtotime($befordate)){
            $result = 'old';
        }else{
            $result = 'new';
        }
        return $result;
    }

    function getBenefit($type, $labor){
        $getsql = mysql_query("SELECT d_category FROM tbl_labor WHERE d_id = '$labor'");
        $get = mysql_fetch_assoc($getsql);
        $setting = $get['d_category'];
        
        if($type == 'out'){
            $val = "d_value2";
        }else{
            $val = "d_value3";
        }
        
        $sql = mysql_query("SELECT $val FROM tbl_setting WHERE d_id = '$setting'");
        $row = mysql_fetch_assoc($sql);
        return $row[$val];
    }

    function totalDependent($proid){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$proid' AND d_status = 'active'");
        return mysql_num_rows($sql);
    }

    function getDependent($did){
    $sql = mysql_query("SELECT * FROM tbl_profile WHERE d_dependent = '$did' AND d_status = 'active'");
    if(mysql_num_rows($sql)){
    while($row = mysql_fetch_assoc($sql)){
    ?>
    <div class="m-widget4__item">
        <div class="m-widget4__info">
            <span class="m-widget4__title">
                <b><?php echo $row['d_name']; ?></b> -  <?php echo $row['d_relstatus']; ?> - [ MRN : <?php echo $row['d_mrn']; ?>]
            </span>
        </div>
    </div>
    <?php
            }
        }else{
           return "There is no records of your family members.<br>
                    If your family member name not appear:-<br>
                    1. It was not entered in KAIZEN ESS<br>
                    2. It has not been update to SMBS by HR yet<br>
                    3. They are not covered by the hospital"; 
        }
    }

    function getTotal($type, $labid, $startDate, $endDate){
        $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_type = '$type' AND d_staffID = '$labid' AND d_vdate BETWEEN '$startDate' AND '$endDate' AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        return $row['total'];
    }

?>

<?php 
if(isset($_POST['page'])){ 
   $page = $_POST['page'];
    if($page == 'postDate'){
        $search = $_POST['val1'];
        $user = mysql_real_escape_string(trim($_POST['val2']));
        $startDate = date('Y-m-d', strtotime($_POST['val3']));
        $endDate = date('Y-m-d', strtotime($_POST['val4']));
        
        if($search == 'std'){
            $labid = $general->allTable2($user,'d_staffID','tbl_labor','d_id');
            $proid = $general->allTable2($labid,'d_labor','tbl_profile','d_id');
            $mrn = $general->allTable2($proid,'d_id','tbl_profile','d_mrn');
        }elseif($search == 'stc'){
            $labid = $general->allTable2($user,'d_ic','tbl_profile','d_labor');
            $proid = $general->allTable2($user,'d_ic','tbl_profile','d_id');
            $mrn = $general->allTable2($proid,'d_id','tbl_profile','d_mrn');
        }elseif($search == 'stm'){
            $proid = $general->allTable2($user,'d_mrn','tbl_profile','d_id');
            $labid = $general->allTable2($user,'d_mrn','tbl_profile','d_labor');
            $mrn = $user;
        }
        
        if($labid == 0 || $proid == 0){
            echo 'No User Found, try again';
        }else{
        ?>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
    <div class="m-portlet__body">
       <div class="col-lg-12" style="" id="notification"></div>
        <div class="col-lg-12">
            <table>
                <tr>
                    <td>Name</td>
                    <td class="col-1">:</td>
<!--                    <?php echo $startDater.'  '.$endDater;?>-->
                    <td> <?php echo $general->allTable2($proid, 'd_id','tbl_profile', 'd_name'); ?></td>
                </tr>
                <tr>
                    <td>IC</td>
                    <td class="col-1">:</td>
                    <td> <?php echo $general->allTable2($proid, 'd_id','tbl_profile', 'd_ic'); ?></td>
                </tr>
                <tr>
                    <td>Staff ID</td>
                    <td class="col-1">:</td>
                    <td> <?php echo $general->allTable2($labid, 'd_id','tbl_labor', 'd_staffID'); ?></td>
                </tr>
                <tr>
                    <td>Staff MRN</td>
                    <td class="col-1">:</td>
                    <td> <?php echo $mrn; ?></td>
                </tr>
                <tr>
                    <td>Out-Patient</td>
                    <td class="col-1">:</td>
                    <td><span id=""><?php echo "RM ".number_format(totalOutpatient($labid,$proid),2); ?></span><span id="balOut"> ( <b>Balance :</b> RM0.00-test )</span></td>
                </tr>
                <tr>
                    <td>In-Patient</td>
                    <td class="col-1">:</td>
<!--                    <td><span id=""><?php echo totalInpatient($labid); ?></span><span id="balIn"> ( <b>Balance :</b> RM0.00-test )</span></td>-->
                    <td><span id=""><?php echo totalInpatient($labid); ?></span></td>
                </tr>
                <tr>
                    <td>Relative</td>
                    <td class="col-1">:</td>
                    <td> <?php echo getDependent($proid); ?></td>
                </tr>
                
            </table>
        </div>
        <div class="col-lg-12" style="border-bottom:1px lightgrey solid; margin:20px 0px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                Out-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Dept
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bil=1; $testtotal = 0;
											$year='2019'; $sesID = $general->allTable2($labid, 'd_id','tbl_labor', 'd_staffID'); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_status = 'active' AND d_staffID ='$sesID' AND d_vdate BETWEEN '$startDate' AND '$endDate' "); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
												?>
                    <tr>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; $testtotal += $rowOut['d_charge']; ?> 
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                                            <td>
                                               <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                                            </td>
                    </tr>
                    <?php $bil++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="5"></td>
<!--                        <td colspan="5"><b><?php //$staffID = $general->allTable2($labid,'d_id','tbl_labor','d_staffID'); echo "RM".number_format(getTotal('OUT', $staffID, $startDate, $endDate),2); ?></b></td>-->
                        <td colspan="5"><b><?php  echo "RM".number_format($testtotal,2); ?></b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12" style="height:40px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                In-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Department
                        </th>
                        <th>
                            Days
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bill=1; $testtotal2=0;
								            $sesID = $general->allTable2($labid, 'd_id','tbl_labor', 'd_staffID'); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_status = 'active' AND d_staffID ='$sesID' AND d_vdate BETWEEN '$startDate' AND '$endDate'"); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
								        ?>
                    <tr>
                        <th scope="row">
                            <?php echo $bill; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_day']; ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; $testtotal2 += $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?><br>
                            <?php if($rowOut['d_exceedRemarks'] != ''){ echo "[ ".$rowOut['d_exceedRemarks']." ]"; } ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                                            <td>
                                               <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                                            </td>
                    </tr>
                    <?php $bill++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="6"></td>
                        <td colspan="5"><b><?php  echo "RM".number_format($testtotal2,2); ?></b></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php 
//calc
$balOut = " (<b> Balance :</b> RM ".number_format((totalOutpatient($labid,$proid)-$testtotal),2)." )"; 
$balIn = " (<b> Balance :</b> RM ".number_format((getBenefit('in', $labid)-$testtotal2),2)." )";

?>
<input type="hidden" id="totalBalOut" value="<?php echo $balOut; ?>">
<input type="hidden" id="totalBalIn" value="<?php echo $balIn; ?>">
        <?php
        }
}
}

if(isset($_POST['entitlement'])){ 
    $type = mysql_real_escape_string(trim($_POST['val1']));
    $value = mysql_real_escape_string(trim($_POST['val2']));
?>

<?php
}

?>



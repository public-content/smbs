<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }

    .activator {
        color: cyan;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="m-section m-section--last">
                        <div class="m-section__content">
                            <div class="m-demo">
                                <div class="m-demo__preview">
                                    <div class="m-list-search">
                                        <div class="m-list-search__results">
                                            <span class="m-list-search__result-message m--hide">
                                                No record found
                                            </span>
                                            <span class="m-list-search__result-category m-list-search__result-category--first">
                                                Setting
                                            </span>
                                            <a href="#" class="m-list-search__result-item" id="setcategoryactive" onclick="openBody('setcategory')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-folder m--font-warning"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Category Level
                                                </span>
                                            </a>
                                            <a href="#" class="m-list-search__result-item" id="setgroupactive" onclick="openBody('setgroup')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-group m--font-success"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Staff Group
                                                </span>
                                            </a>
                                            <a href="#" class="m-list-search__result-item" id="setdepartmentactive" onclick="openBody('setdepartment')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-building m--font-info"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Department
                                                </span>
                                            </a>
                                            <a href="#" class="m-list-search__result-item" id="setdoctoractive" onclick="openBody('setdoctor')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-medkit m--font-danger"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Doctors
                                                </span>
                                            </a>
                                            <a href="#" class="m-list-search__result-item" id="settreattypeactive" onclick="openBody('settreattype')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-list-ul m--font-info"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Treatment Type
                                                </span>
                                            </a>
                                            <a href="#" class="m-list-search__result-item" id="setlistactive" onclick="openBody('setlist')">
                                                <span class="m-list-search__result-item-icon">
                                                    <i class="la la-list-ul m--font-info"></i>
                                                </span>
                                                <span class="m-list-search__result-item-text">
                                                    Treatment List
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8" id='body-main'>
                    <div id="setlist"> <?php include("asset/component/mastersetting/setlist.php"); ?> </div>
                    <div id="setstatus"> <?php include("asset/component/mastersetting/setstatus.php"); ?> </div>
                    <div id="settreattype"> <?php include("asset/component/mastersetting/settreattype.php"); ?> </div>
                    <div id="setcategory"> <?php include("asset/component/mastersetting/setcategory.php"); ?> </div>
                    <div id="setgroup"> <?php include("asset/component/mastersetting/setgroup.php"); ?> </div>
                    <div id="setdepartment"> <?php include("asset/component/mastersetting/setdepartment.php"); ?> </div>
                    <div id="setdoctor"> <?php include("asset/component/mastersetting/setdoctor.php"); ?> </div>
                    <div class="m-page-loadera m-page-loader--base" style="background:none;" id="loader">
                        <div class="m-blockui" style="background:white;">
                            <span>
                                <img src='images/system/gif/loading.gif' alt='' width='30' class="m-loader--brand" style="background:none;">
                            </span>
                            <span>Please wait...</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $("#setlist").hide();
    $("#setstatus").hide();
    $("#settreattype").hide();
    $("#setcategory").hide();
    $("#setgroup").hide();
    $("#setdepartment").hide();
    $("#setdoctor").hide();
    $('#loader').hide();


    function openBody(page) {
        $('#loader').show();
        var element = document.getElementById(page + 'active');
        var sub = ['setlist', 'setstatus', 'settreattype', 'setcategory', 'setgroup', 'setdepartment', 'setdoctor', 'loader'];
        for (var x = 0; x < sub.length; x++) {
            if (sub[x] == page) {
                $("#" + sub[x]).show();
                getset(page);
            } else {
                $("#" + sub[x]).hide();
            }
        }
        //        alert(sub[0]);
    }

    function removeAll(page) {
        $("#" + page).hide();
    }

    function getset(val) {
        var dataString = {
            setType: val
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_mastersetting.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById(val + "-body").innerHTML = html;
            }
        });
        return false;
    }

    function delThis(val, page) {
        var noti = page + "-noti";
        if (confirm("Are you sure!")) {
            var dataString = {
                delete: "delete",
                value: val
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_mastersetting.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    document.getElementById(noti).innerHTML = html;
                    getset(page);
                }
            });
            return false;
        } else {

        }

    }

    function actProcess(did, page) {
        var act = "act" + page;
        var noti = page + "-noti";
        var catName = document.getElementById('d_value-' + did).value;
        var catOut = document.getElementById('d_value2-' + did).value;
        var catIn = document.getElementById('d_value3-' + did).value;

        var dataString = {
            act: act,
            d_value: catName,
            d_value2: catOut,
            d_value3: catIn,
            did: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_mastersetting.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById(noti).innerHTML = html;
                getset(page);
            }
        });
        return false;
    }

    function addProcess(page, table) {
        var add = table;
        var noti = page + "-noti";
        var d_value = document.getElementById('d_value-' + page).value;
        var d_value2 = document.getElementById('d_value2-' + page).value;
        var d_value3 = document.getElementById('d_value3-' + page).value;

        var dataString = {
            add: add,
            d_value: d_value,
            d_value2: d_value2,
            d_value3: d_value3
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_mastersetting.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById(noti).innerHTML = html;
                getset(page);
            }
        });
        return false;
    }
</script>

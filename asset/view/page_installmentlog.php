<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = $_SERVER['REQUEST_URI'];
?>
<style>
    .red {
        color: red
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet" id="table-list">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-list-3"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Completed Installment
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-scrollbar-shown="true">
                        <div class='kt-section'>
                            <div class='kt-section__content'>
                                <table class='table'>
                                    <thead class='thead-dark'>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Staff ID</th>
                                            <th>Department</th>
                                            <th>Amount(RM)</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="completeList">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-portlet m--hide" id="table-log">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon">
                                        <i class="flaticon-list-3"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Completed Installment
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    
    getList();
    
    function getList(){
        document.getElementById('completeList').innerHTML = "<tr><td colspan='8'><div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'> Please wait..</div></td></tr>";
        var dataString = {
            checking: 'getList'
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById('completeList').innerHTML = data;
            }
        });
        return false;
    }
</script>
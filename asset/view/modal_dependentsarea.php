<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();

    function allTable($val, $colP, $Ftbl, $colN){
          $sql = mysql_query("SELECT $colN FROM $Ftbl WHERE $colP = '$val'");
          $row = mysql_fetch_assoc($sql);
          if($row[$colN] != ''){
              return $row[$colN];
          }else{
            return '';
          }
      }

    function allTable1($val, $colP, $Ftbl, $colN){
          $sql = mysql_query("SELECT $colN FROM $Ftbl WHERE $colP = '$val'");
          $row = mysql_fetch_assoc($sql);
          if($row[$colN] != ''){
              return $row[$colN];
          }else{
            return '0';
          }
      }

?>

<?php 
    function getDependentImage($who, $gen){
		if($who == 'WIFE'){
			$result = 'images/profile/wife.png';
		}else{    
			if($gen == 'MALE'){
				$result = 'images/profile/childmale.png';
			}else{
				$result = 'images/profile/childfemale.png';
			}
		}
		
		return $result;
	}

    function checkIC($ic){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_ic = '$ic'");
        $row = mysql_num_rows($sql);
        if($row != 0){
            return 'YES';
        }else{
            return 'NO';
        }
    }

    function dependentCount($did){
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$did'");
		$row = mysql_num_rows($sql);
		return $row;
	}

    function getDependent($did){
        $sql = mysql_query("SELECT * FROM tbl_profile WHERE d_dependent = '$did' AND d_status = 'active'");
        if(mysql_num_rows($sql)){
            while($row = mysql_fetch_assoc($sql)){
                ?>
<div class="m-widget4__item">
    <div class="m-widget4__img m-widget4__img--pic">
        <img src="<?php echo getDependentImage($row['d_relstatus'],$row['d_gender']); ?>" alt="">
    </div>
    <div class="m-widget4__info">
        <span class="m-widget4__title">
            <?php echo $row['d_name']; ?>
        </span>
        <br>
        <span class="m-widget4__sub">
            <?php echo $row['d_relstatus']; ?> - [ MRN : <?php echo $row['d_mrn']; ?> ]
        </span>
    </div>
    <div class="m-widget4__progress">

    </div>
    <div class="m-widget4__ext">
        <a href="#" class="m-btn m-btn--hover-brand m-btn--pill btn btn-sm btn-secondary" data-toggle="modal" data-target="#editRelative<?php echo $row['d_id']; ?>">
            View
        </a>
    </div>
</div>
<div class="modal fade" id="editRelative<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Edit Relative
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group">
                        <label for="dname">
                            Name
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dname<?php echo $row['d_id']; ?>" name="dname" value="<?php echo $row['d_name']; ?>" required>
                        <input type="hidden" class="form-control m-input m-input--solid" id="did<?php echo $row['d_id']; ?>" name="did" value="<?php echo $row['d_id']; ?>" required>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div class="form-group m-form__group">
                        <label for="dIC">
                            I/C Number
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dIC<?php echo $row['d_id']; ?>" name="dIC" value="<?php echo $row['d_ic']; ?>" required>
                        <span class="m-form__help">Without '-' symbol</span>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="dMRN">
                            MRN
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dMRN<?php echo $row['d_id']; ?>" name="dMRN" value="<?php echo $row['d_mrn']; ?>" required>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">
                            Gender:
                        </label>
                        <div class="m-checkbox-list">
                            <select class="form-control m-input m-input--solid" name="dGender" id="dGender<?php echo $row['d_id']; ?>" required>
                                <option>
                                    Choose
                                </option>
                                <option value="MALE" <?php if($row['d_gender'] == 'MALE'){ print 'selected'; }  ?>>
                                    Male
                                </option>
                                <option value="FEMALE" <?php if($row['d_gender'] == 'FEMALE'){ print 'selected'; }  ?>>
                                    Female
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">
                            Relation:
                        </label>
                        <div class="m-checkbox-list">
                            <select class="form-control m-input m-input--solid" name="dRelay" id="dRelay<?php echo $row['d_id']; ?>" required>
                                <option>
                                    Choose
                                </option>
                                <option value="WIFE" <?php if($row['d_relstatus'] == 'WIFE'){ print 'selected'; }  ?>>
                                    Wife
                                </option>
                                <option value="HUSBAND" <?php if($row['d_relstatus'] == 'HUSBAND'){ print 'selected'; }  ?>>
                                    Husband
                                </option>
                                <option value="CHILDREN" <?php if($row['d_relstatus'] == 'CHILDREN'){ print 'selected'; }  ?>>
                                    Children
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submitEdit(<?php echo $row['d_id']; ?>)">
                        Edit
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="submitDel(<?php echo $row['d_id']; ?>)">
                        Delete
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
            }
        }else{
           return "There is no records of your family members.<br>
                    If your family member name not appear:-<br>
                    1. It was not entered in KAIZEN ESS<br>
                    2. It has not been update to SMBS by HR yet<br>
                    3. They are not covered by the hospital"; 
        }
    }
?>

<?php
    if(isset($_POST['action'])){
        $page = mysql_real_escape_string(trim($_POST['action']));
        
        if($page == 'pageSubmit'){
            $val1 = mysql_real_escape_string(trim($_POST['val1']));
            $val2 = mysql_real_escape_string(trim($_POST['val2']));
            $val3 = mysql_real_escape_string(trim($_POST['val3']));
            $val4 = mysql_real_escape_string(trim($_POST['val4']));
            $val5 = mysql_real_escape_string(trim($_POST['val5']));
            $val6 = mysql_real_escape_string(trim($_POST['val6']));
            $val7 = mysql_real_escape_string(trim($_POST['val7']));
            $dependent = allTable($val1,'d_ic','tbl_profile','d_id');
            
            //check if ic is valid
            $checkIC = checkIC($val4);
            
            if($checkIC == 'YES'){
                $patner = allTable($val4,'d_ic','tbl_profile','d_id');
               $sql = mysql_query("UPDATE tbl_profile set d_relstatus = 'YES', d_martial = 'MARRIED', d_relic = '$val4' WHERE d_id = '$dependent'");
                if(!$sql){
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
                }else{
                    $sql1 = mysql_query("UPDATE tbl_profile set d_relstatus = 'YES', d_martial = 'MARRIED', d_relic = '$val1' WHERE d_id = '$patner'");
                    if(!$sql1){
                        echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
                    }else{
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Well done
                                </strong>
                                Succfully Added
                            </div>";
                    }
                }  
            }else{
               $sql = mysql_query("INSERT INTO tbl_profile(d_type, d_dependent, d_name, d_gender, d_relstatus, d_ic, d_mrn, d_created, d_modified, d_status)
                      VALUES('DEPENDENTS','$dependent','$val3','$val6','$val7','$val4','$val5',NOW(),NOW(),'active')");
            
                if(!$sql){
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
                }else{
                    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Well done
                                </strong>
                                Succfully Added
                            </div>";
                } 
            }
            
        }elseif($page == 'pageEdit'){
            $val1 = mysql_real_escape_string(trim($_POST['val1']));
            $val2 = mysql_real_escape_string(trim($_POST['val2']));
            $val3 = mysql_real_escape_string(trim($_POST['val3']));
            $val4 = mysql_real_escape_string(trim($_POST['val4']));
            $val5 = mysql_real_escape_string(trim($_POST['val5']));
            $val6 = mysql_real_escape_string(trim($_POST['val6']));
            
            $sql = mysql_query("UPDATE tbl_profile SET d_name = '$val2', d_ic = '$val3', d_mrn = '$val4', d_gender = '$val5', d_relstatus = '$val6', d_modified = NOW() WHERE d_id = '$val1'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Ohh No!
                            </strong>
                            ".mysql_error()."
                        </div>";
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Well done
                            </strong>
                            Succfully Updated
                        </div>";
            }
        }elseif($page == 'pageDel'){
           $val1 = mysql_real_escape_string(trim($_POST['val1'])); 
            $sql = mysql_query("UPDATE tbl_profile SET d_status = 'deleted', d_modified = NOW() WHERE d_id = '$val1'");
                if(!$sql){
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
                }else{
                    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Well done
                                </strong>
                                Succfully Updated
                            </div>";
                }
        }elseif($page == 'pageDelpat'){
            $val1 = mysql_real_escape_string(trim($_POST['val1']));
            $patner = allTable($val1,'d_id','tbl_profile','d_relic');
            $patnerdid = allTable($patner,'d_ic','tbl_profile','d_id');
            
            $sql = mysql_query("UPDATE tbl_profile SET d_relic = '',d_relstatus = 'NO', d_modified = NOW() WHERE d_id = '$val1'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
            }else{
                $sql1 = mysql_query("UPDATE tbl_profile SET d_relic = '',d_relstatus = 'NO', d_modified = NOW() WHERE d_id = '$patnerdid'");
                if(!$sql1){
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Ohh No!
                                </strong>
                                ".mysql_error()."
                            </div>";
                }else{
                    echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                                <strong>
                                    Well done
                                </strong>
                                Succfully Updated
                            </div>";
                }
            }
        }elseif($page == 'pageCheckIC'){
            $ic = mysql_real_escape_string(trim($_POST['val1']));
            $sql = mysql_query("SELECT * FROM tbl_profile WHERE d_ic = '$ic'");
            $r = mysql_num_rows($sql);
            if($r != '0'){
                $row = mysql_fetch_assoc($sql);
                $name = $row['d_name'];
                $mrn = $row['d_mrn'];
                $gender = $row['d_gender'];
                $result = array("rname"=>$name, "rmrn"=>$mrn, "rgender"=>$gender);
            }else{
                $name = "";
                $mrn = "";
                $gender = "";
                $result = array("rname"=>$name, "rmrn"=>$mrn, "rgender"=>$gender);
            }
            echo json_encode($result);
        }
    }
?>


<?php 
     if(isset($_POST['page'])){
        if($_POST['page'] == 'page_profile'){
            //        check existant
            if($_POST['thevalue'] != ''){
//                   get 2 main things
                if($_POST['searchtype'] == 'std'){
                    $getlabid = allTable1($_POST['thevalue'], 'd_staffID', 'tbl_labor', 'd_id');
                    $getproid = allTable1($getlabid, 'd_labor', 'tbl_profile', 'd_id');
                }elseif($_POST['searchtype'] == 'stc'){
                    $getlabid  = allTable1($_POST['thevalue'], 'd_ic', 'tbl_profile', 'd_labor');
                    $getproid = allTable1($_POST['thevalue'], 'd_ic', 'tbl_profile', 'd_id');
                }elseif($_POST['searchtype'] == 'stm'){
                    $getlabid = allTable1($_POST['thevalue'], 'd_mrn', 'tbl_profile', 'd_labor');
                    $getproid = allTable1($_POST['thevalue'], 'd_mrn', 'tbl_profile', 'd_id');
                }

                if($getlabid != '0' || $getproid != '0'){
?>
<div class="row">
    <div class="col-lg-6" style="margin-top:40px" id="col-dynamic">
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th scope="row">
                        Name
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_name'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Staff ID
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_staffID'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Staff IC
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_ic'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Gender
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_gender'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        MRN
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_mrn'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Date Joined
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo date('d M Y',strtotime($general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_employment'))); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Department
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_department'), 'd_id', 'tbl_setting', 'd_value'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Designation
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_designation'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Category
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_category'), 'd_id', 'tbl_setting', 'd_value'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Family Members
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <!-- check dependent on this proid -->
                        <div class="m-widget4 m-widget4--progress">
                            <?php 
                        echo getDependent($getproid); 
                    ?>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th scope="row">

                    </th>
                    <td class="td-a">

                    </td>
                    <?php //'system.php?p='.$general->securestring('encrypt','102').'&i='.$general->securestring('encrypt',$getproid); ?>
                    <td style="text-align:right;">
                        <?php $available = $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_available'); ?>
                        <?php if($available == 'YES'){ ?>
                        <a href="" class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#addRelative">
                            <span>
                                <i class="la la-plus-square-o"></i>
                                <span>
                                    Add Relative
                                </span>
                            </span>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <?php  $relic = $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_relic'); ?>
    <?php 
        if($relic != ''){ 
            $getlabid2  = allTable1($relic, 'd_ic', 'tbl_profile', 'd_labor');
            $getproid2 = allTable1($relic, 'd_ic', 'tbl_profile', 'd_id');
    ?>
        
        <div class="col-lg-6" style="margin-top:40px" id="col-dynamic">
        <div class="form-group m-form__group m--margin-top-10">
			<div class="alert m-alert m-alert--default" role="alert">
				This staff's partner were in same hospital. <span style="float: rigth;"> <button type="button" class="btn btn-secondary btn-sm" onclick="submitdelpat('<?php echo $getproid2; ?>')">Delete from partner</button> </span>
			</div>
        </div>
        <table class="table table-hover">
            <tbody>
                <tr>
                    <th scope="row">
                        Name
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid2, 'd_id', 'tbl_profile', 'd_name'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Staff ID
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getlabid2, 'd_id', 'tbl_labor', 'd_staffID'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Staff IC
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid2, 'd_id', 'tbl_profile', 'd_ic'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Gender
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid2, 'd_id', 'tbl_profile', 'd_gender'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        MRN
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getproid2, 'd_id', 'tbl_profile', 'd_mrn'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Date Joined
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo date('d M Y',strtotime($general->allTable($getlabid2, 'd_id', 'tbl_labor', 'd_employment'))); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Department
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($general->allTable($getlabid2, 'd_id', 'tbl_labor', 'd_department'), 'd_id', 'tbl_setting', 'd_value'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Designation
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($getlabid2, 'd_id', 'tbl_labor', 'd_designation'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Category
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <?php echo $general->allTable($general->allTable($getlabid2, 'd_id', 'tbl_labor', 'd_category'), 'd_id', 'tbl_setting', 'd_value'); ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Family Members
                    </th>
                    <td class="td-a">
                        :
                    </td>
                    <td>
                        <!-- check dependent on this proid -->
                        <div class="m-widget4 m-widget4--progress">
                            <?php 
                        echo getDependent($getproid2); 
                    ?>
                        </div>

                    </td>
                </tr>
                <tr>
                    <th scope="row">

                    </th>
                    <td class="td-a">

                    </td>
                    <?php //'system.php?p='.$general->securestring('encrypt','102').'&i='.$general->securestring('encrypt',$getproid); ?>
                    <td style="text-align:right;">
                        <?php $available = $general->allTable($getproid2, 'd_id', 'tbl_profile', 'd_available'); ?>
                        <?php if($available == 'YES'){ ?>
                        <a href="" class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#addRelative">
                            <span>
                                <i class="la la-plus-square-o"></i>
                                <span>
                                    Add Relative
                                </span>
                            </span>
                        </a>
                        <?php } ?>
                    </td>
                </tr>
            </tbody>
        </table>
        
    </div>
    <?php } ?>
    
</div>

<div class="modal fade" id="addRelative" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add Relative
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group">
                        <label for="staffIC">
                            Identification I/C
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="staffIC" name="staffIC" value="<?php echo $general->allTable($getproid, 'd_id', 'tbl_profile', 'd_ic'); ?>" readonly>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div class="form-group m-form__group">
                        <label for="staffID">
                            Identification ID
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="staffID" name="staffID" value="<?php echo $general->allTable($getlabid, 'd_id', 'tbl_labor', 'd_staffID'); ?>" readonly>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div style="border-bottom:1px solid black;margin-bottom:10px;"></div>
                    <div class="form-group m-form__group">
                        <label for="dIC">
                            I/C Number
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dIC" name="dIC" value="" required onkeyup="searchIC()">
                        <span class="m-form__help">Without '-' symbol</span>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="dname">
                            Name
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dname" name="dname" value="" required>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div class="form-group m-form__group">
                        <label for="dMRN">
                            MRN
                        </label>
                        <input type="text" class="form-control m-input m-input--solid" id="dMRN" name="dMRN" value="" required>
                        <!--                        <span class="m-form__help">We'll never share your email with anyone else.</span>-->
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">
                            Gender:
                        </label>
                        <div class="m-checkbox-list">
                            <select class="form-control m-input m-input--solid" name="dGender" id="dGender" required>
                                <option>
                                    Choose
                                </option>
                                <option value="MALE">
                                    Male
                                </option>
                                <option value="FEMALE">
                                    Female
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <label for="">
                            Relation:
                        </label>
                        <div class="m-checkbox-list">
                            <select class="form-control m-input m-input--solid" name="dRelay" id="dRelay" required>
                                <option>
                                    Choose
                                </option>
                                <option value="WIFE">
                                    Wife
                                </option>
                                <option value="HUSBAND">
                                    Husband
                                </option>
                                <option value="CHILDREN">
                                    Children
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submitRelative()">
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
                }else{
//                    echo '<span style="background:red;color:white;padding:2px 10px;margin-left:20px;width:20%;margin-top:20px;">No data found.</span>';
                    echo "<div class='col-12' style='margin-top:20px'><div class='alert alert-danger' role='alert'><strong>Ohh no!</strong> No data found.</div></div>";
                }
            }else{
//                echo '<span style="background:red;color:white;padding:2px 10px;margin-left:20px;width:20%;margin-top:20px;">The input is blank.</span>';
                echo "<div class='col-12' style='margin-top:20px'><div class='alert alert-danger' role='alert'><strong>Ohh no!</strong> The input is blank.</div></div>";

            }
        }elseif($_POST['page'] == 'page_blanks'){
    
        }
    }
?>

<?php
  $pageModel = $general->pageInfo($page,'pageDir');
  $pageTitle = $general->pageInfo($page,'pageTitle');
  $pageName = $general->pageInfo($page,'pageName');
  $pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
  $main->includePHP('model',$pageModel);
  $cont = new $pageModel();
  $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
//  $pageDir = 'system.php?p='.$_GET['p'];
  $pageDir = $_SERVER['REQUEST_URI'];
?>
<style>
    .red {
        color: red
    }

    ;

</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?php echo "Staff Dependents"; ?>
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">
                    <div class="col-lg-12">
                        <form method="post">
                            <div class="form-group m-form__group row">
                                <label for="example-search-input" class="col-2 col-form-label">
                                    Search by:
                                </label>
                                <div class="col-3">
                                    <select class="form-control m-input m-input--solid" id="searchtype">
                                        <option value="std">
                                            Staff ID
                                        </option>
                                        <option value="stc">
                                            Staff IC
                                        </option>
                                        <option value="stm">
                                            MRN
                                        </option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <input class="form-control m-input m-input--solid" type="search" value="" id="thevalue">
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-secondary m-btn m-btn--icon" onclick="postdid()">
                                        <span>
                                            <i class="la la-search"></i>
                                            <span>
                                                Search
                                            </span>
                                        </span>
                                    </button>
                                </div>
                                <div class="col-1" style="text-align:center;">
                                    <button class="btn btn-metal m-btn m-btn--icon" type="reset" onclick="closepage()">
                                        <span>
                                            <i class="la la-retweet"></i>
                                            <span>
                                                Reset
                                            </span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-12" style="margin-top:10px;" id="noti">
                        
                    </div>
                    <div class="col-lg-12" id="displayProfile">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function postdid() {
        document.getElementById('displayProfile').innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='100'>";
        var searchtype = document.getElementById('searchtype').value;
        var thevalue = document.getElementById('thevalue').value;
        var page = "page_profile";

        var dataString = {
            page: page,
            searchtype: searchtype,
            thevalue: thevalue
        };
        //        console.log(dataString);
        $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('displayProfile').innerHTML = html;
                //                                       alert(searchtype+thevalue); 
            }
        });
        return false;
    }

    function submitRelative() {
        var staffIC = document.getElementById('staffIC').value;
        var staffID = document.getElementById('staffID').value;
        var dname = document.getElementById('dname').value;
        var dIC = document.getElementById('dIC').value;
        var dMRN = document.getElementById('dMRN').value;
        var dGender = document.getElementById('dGender').value;
        var dRelay = document.getElementById('dRelay').value;

        var dataString = {
            val1: staffIC,
            val2: staffID,
            val3: dname,
            val4: dIC,
            val5: dMRN,
            val6: dGender,
            val7: dRelay,
            action: "pageSubmit"
        };

        $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('noti').innerHTML = html;
                postdid();
            }
        });
        return false;
    }

    function submitEdit(xix) {
        var did = document.getElementById('did'+xix).value;
        var dname = document.getElementById('dname'+xix).value;
        var dIC = document.getElementById('dIC'+xix).value;
        var dMRN = document.getElementById('dMRN'+xix).value;
        var dGender = document.getElementById('dGender'+xix).value;
        var dRelay = document.getElementById('dRelay'+xix).value;

        var dataString = {
            val1: xix,
            val2: dname,
            val3: dIC,
            val4: dMRN,
            val5: dGender,
            val6: dRelay,
            action: "pageEdit"
        };

        $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('noti').innerHTML = html;
                postdid();
            }
        });
        return false;
    }
    
    function submitDel(did){
        
        if (confirm("Are you sure!")) {
             var dataString = {
            val1: did,
            action: "pageDel"
        };

        $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('noti').innerHTML = html;
                postdid();
            }
        });
        return false; 
      } else {
          
      }
    }
    
    function submitdelpat(did){
        if (confirm("Are you sure!")) {
             var dataString = {
            val1: did,
            action: "pageDelpat"
        };

        $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('noti').innerHTML = html;
                postdid();
            }
        });
        return false; 
      } else {
          
      }
    }
    
    function searchIC(){
        var count = $('#dIC').val().length;
        var ic = document.getElementById('dIC').value;
        var dataString = {
            action: "pageCheckIC",
            val1: ic
        };
        if (count > 10) {
           $.ajax({
            type: "post",
            url: "asset/view/modal_dependentsarea.php",
            data: dataString,
            cache: false,
            success: function(html) {
                var obj = $.parseJSON(html);
                var name = obj.rname;
                var mrn = obj.rmrn;
                var gender = obj.rgender;
                
                document.getElementById("dname").value = name;
                document.getElementById("dMRN").value = mrn;
                document.getElementById("dGender").value = gender;
            }
        });
        return false; 
        }
        
    }
    

</script>

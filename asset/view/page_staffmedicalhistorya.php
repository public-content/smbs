<?php 
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$main->includePHP('model','staffdashboard');
	$cont = new $pageModel();
	$dashboard = new staffdashboard();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
	$pageDir = 'system.php?p='.$_GET['p'];
	
?>


<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="col-lg-12" id="passwordNotice">
                <?php if($passNoty != ''){ echo $passNoty; } ?>
                <?php if($dashboard->checkPassword($_SESSION['logid']) == 1){ ?>
                <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>
                            Warning !
                        </strong>
                        Please change your current password: 1234 to new password. This to prevent others from using your account.
                    </div>
                    <div class="m-alert__actions" style="width: 160px;">
                        <button type="button" class="btn btn-warning btn-sm m-btn m-btn--pill m-btn--wide" data-toggle="modal" data-target="#changePassword">
                            Change
                        </button>
                    </div>
                </div>
                <?php }  ?>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Staff Medical History
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <form>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-2 col-sm-12">
                                        Search By:
                                    </label>
                                    <div class="col-lg-2 col-md-9 col-sm-12">
                                        <select class="form-control m-input m-input--solid" id="searchtype">
                                            <option value="std">
                                                Staff ID
                                            </option>
                                            <option value="stc">
                                                Staff IC
                                            </option>
                                            <option value="stm">
                                                Staff MRN
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-9 col-sm-12">
                                        <input class="form-control m-input m-input--solid" type="search" value="" id="thevalue">
                                    </div>
                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                        <div class="input-daterange input-group" id="m_datepicker_5">
                                            <?php $dnm="01/01/"; $year=date('Y'); $fuldate=$dnm."".$year; ?>
                                            <input type="text" class="form-control m-input" name="startdate" id="startdate" value="<?php echo $fuldate; ?>" />
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-ellipsis-h"></i>
                                                </span>
                                            </div>
                                            <?php $dnow=date('m/d/Y'); ?>
                                            <input type="text" class="form-control" name="endate" id="endate" value="<?php echo $dnow; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-12">
                                        <button type="button" class="btn btn-secondary" onclick="postDate()">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" id="getHistory">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    function postDate() {
        document.getElementById('getHistory').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var searchtype = document.getElementById('searchtype').value;
        var thevalue = document.getElementById('thevalue').value;
        var startdate = document.getElementById('startdate').value;
        var enddate = document.getElementById('endate').value;
        var page = "postDate";

        var dataString = {
            val1:searchtype,
            val2:thevalue,
            val3:startdate,
            val4:enddate,
            page:page
        };
        if(thevalue == ''){
            alert('Input cannot be blank');
        }else{
             $.ajax({
            type: "post",
            url: "asset/view/modal_stafHistorya.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('getHistory').innerHTML = html;
                var useOut = document.getElementById('totalBalOut').value;
                var useIn = document.getElementById('totalBalIn').value;
                 postOutput('balOut', useOut);
//                postOutput('balIn', useIn);
            }
        });
        return false; 
        }
    }
    
    function delThis(did){
        if (confirm("Are you sure!")) {
            var dataString = { val1 : did, action : "delThis" };
            $.ajax({
                type:"post",
                url:"asset/view/modal_chargehistory.php",
                data:dataString,
                cache:false,
                success: function(html){
                    document.getElementById('notification').innerHTML = html;
                    postDate();
                }
            });
            return false;  
      } else {
          
      }
    }
    
    function postOutput(id, value){
        document.getElementById(id).innerHTML = value;
    }
</script>
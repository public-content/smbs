<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = $_SERVER['REQUEST_URI'];
?>
<style>
    .red {
        color: red
    }

    .loading {
        margin: auto;
        box-shadow: 1px 5px 10px #d5d4d4;
        display: table-cell;
        vertical-align: middle;
        padding: 8px 15px;
        font-size: 1rem;
        font-weight: 400;
        z-index: auto;
    }
    
    .na-col-1{
        font-weight: 600;
        float:left;
        display:block;
        text-decoration: underline;
    }
    
    .na-col-2{
        margin-left: 8px;
        display:inline;
/*        float:left;*/
    }

</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-list"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                List of Installment
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body" style="padding:0;">
                    <div class="col-lg-12" style="margin-top:14px;" id="alert1">

                    </div>
                    <div class="m-scrollable" data-scrollable="true" data-max-height="300" data-scrollbar-shown="true">
                        <div style="margin:0px 20px;">
                            <table class="table m-table">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Profile
                                        </th>
                                        <th>
                                            Benefit
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="installmentlist">
                                    <?php
                                       $bil="1"; $sql = mysql_query("SELECT * FROM tbl_installment WHERE d_type = 'MAIN' AND (d_status = 'new' OR d_status = 'progress' OR d_status = 'finish')"); if(mysql_num_rows($sql)){ while($row = mysql_fetch_assoc($sql)){
                                       if($row['d_status'] == 'new'){
                                           $color = "#8b9dc3";
                                           $col = "#e9ebee";
                                           $status = "<span style='color:#000000;background:#ffbf00;padding:0px 8px;'> New </span>";
                                       }elseif($row['d_status'] == 'progress'){
                                           $color = "#3b5998";
                                           $col = "#e9ebee";
                                           $status = "<span style='color:#000000;background:#f9f9f9;padding:0px 8px;'> Progress </span>";
                                       }elseif($row['d_status'] == 'finish'){
                                           $color = "#e9ebee";
                                           $col = "#797781";
                                           $status = "<span style='color:#f9f9f9;background:#000000;padding:0px 8px;'> Finish </span>";
                                       }
                                       $labid = $cont->allTable($row['d_value1'], 'd_staffID', 'tbl_labor', 'd_id');
                                       $proid = $cont->allTable($labid, 'd_labor', 'tbl_profile', 'd_id');
                                       $logid = $cont->allTable($proid, 'd_userid', 'tbl_user', 'd_id');
                                    ?>
                                    <tr class="" style="background:<?php echo $color; ?>;color:<?php echo $col; ?>;margin-bottom:8px;">
                                        <th scope="row">
                                            <?php echo $bil; ?>
                                        </th>
                                        <td>
                                            <b><?php echo $cont->allTable($proid, 'd_id', 'tbl_profile', 'd_name'); ?></b><br>
                                            <?php echo $cont->allTable($cont->allTable($labid, 'd_id', 'tbl_labor', 'd_department'), 'd_id', 'tbl_setting', 'd_value'); ?><br>
                                            <br>
                                            <b>ID : </b><?php echo $row['d_value1']; ?><br>
                                            <b>MRN : </b><?php echo $cont->allTable($proid, 'd_id', 'tbl_profile', 'd_mrn'); ?>
                                        </td>
                                        <td>
                                            <b>Referrence : </b><?php echo $row['d_value3']; ?> <br>
                                            <b>Installment Balance: </b><?php echo "RM ".$row['d_value2']; ?><br>
                                            <b>Status : </b><?php echo $status; ?><br>
                                            <br>
                                            <b>Remark : </b> <?php echo $row['d_value4']; ?><br>
                                        </td>
                                        <td style="background:#e9ebee;">
                                            <?php if($row['d_status'] == 'new'){ ?>
                                            <a href="#" class="btn btn-primary m-btn" title="Create" style="margin-bottom:3px;" data-toggle="modal" data-target="#create<?php echo $row['d_id']; ?>">Create</a>

                                            <?php }elseif($row['d_status'] == 'progress'){ ?>
                                            <a href="#" class="btn btn-info m-btn m-btn--icon m-btn--icon-only" title="View" style="margin-bottom:3px;" onclick="showInfo('<?php echo $row['d_id']; ?>')"><i class="la la-eye"></i></a>
                                            <?php }else{ ?>
                                            <a href="#" class="btn btn-info m-btn m-btn--icon m-btn--icon-only" title="View" style="margin-bottom:3px;" onclick="showInfo('<?php echo $row['d_id']; ?>')"><i class="la la-eye"></i></a>
                                            <a href="#" id='<?php echo $row['d_id']; ?>btncomp' class="btn btn-success m-btn m-btn--icon m-btn--icon-only" title="Installment Complete" style="margin-bottom:3px;" data-toggle="modal" data-target="#modalComplete<?php echo  $row['d_id']; ?>"><i class="la la-check-square"></i></a>
                                            <?php } ?>
                                            <a href="#" id='<?php echo $row['d_id']; ?>btndele' class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" title="Delete" style="margin-bottom:3px;" data-toggle="modal" data-target="#modalDelete<?php echo  $row['d_id']; ?>"><i class="la la-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php 
                                    $cont->getModalCreate($row['d_id']);
                                    $cont->getModalComplete($row['d_id']);
                                    $cont->getModalDelete($row['d_id']);
                                ?>
                                    <?php
                               $bil++;    } } 
                               ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot"></div>
            </div>


            <div class="m-portlet" id='installmentInfo'>
                <div class='m-portlet__head'>
                    <div class='m-portlet__head-caption'>
                        <div class='m-portlet__head-title'>
                            <span class='m-portlet__head-icon'>
                                <i class='la la-info-circle'></i>
                            </span>
                            <h3 class='m-portlet__head-text'>
                                Installment Details
                            </h3>
                        </div>
                    </div>
                </div>
                <div class='m-portlet__body' id='installmentInfoBody'>
                </div>
                <div class='m-portlet__foot'>
                    <div class='col-lg-12  m--align-right'>
                        <a href='#' class='m-link m--font-bold' onclick='closeInfo()'>Close</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $('#installmentInfo').hide();

    function showMe(dataa) {
        $("#alert1").slideDown();
        document.getElementById('alert1').innerHTML = dataa;
        hideMe();
    }

    function hideMe() {
        setTimeout(function() {
            $("#alert1").slideUp();
        }, 3000);

    }

    function submitCreate(did) {
        var get_did = document.getElementById('did' + did).value;
        var get_nom = document.getElementById('nom' + did).value;
        var dataString = {
            process: 'createInstallment',
            val1: get_did,
            val2: get_nom
        };

        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(dataa) {
                location.reload();
                showMe(dataa);
            }
        });
        return false;
    }

    function checkEach(did) {
        var get_did = document.getElementById('did' + did).value;
        var get_nom = document.getElementById('nom' + did).value;
        var dataString = {
            checking: 'checkEach',
            val1: get_did,
            val2: get_nom
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById('each' + did).innerHTML = data;
            }
        });
        return false;

    }

    function showInfo(did) {
        $('#installmentInfo').show();
        document.getElementById('installmentInfoBody').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'> Please wait..</div>";
        var get_did = did;
        var dataString = {
            checking: 'installmentInfo',
            val1: get_did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById('installmentInfoBody').innerHTML = data;
            }
        });
        return false;
    }

    function closeInfo() {
        $('#installmentInfo').hide();
    }

    function actionpaid(did, datacode) {
        document.getElementById(did + "btn").innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='25'>";
        var month = document.getElementById(did + "month").value;
        var year = document.getElementById(did + "year").value;
        var permonth = document.getElementById(did + "permonth").value;
        var dataString = {
            process: 'actionpaid',
            val1: did,
            val2: datacode,
            val3: month,
            val4: year,
            val5: permonth
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                showInfo(datacode);
                showMe(data);
            }
        });
        return false;
    }

    function actionunpaid(did, datacode) {
        document.getElementById('installmentInfoBody').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'> Please wait..</div>";
        var dataString = {
            process: 'actionunpaid',
            val1: did,
            val2: datacode
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                showInfo(datacode);
                showMe(data);
            }
        });
        return false;
    }

    function addInstallment(main) {
        var dataString = {
            process: 'actionaddrow',
            val1: main
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                showInfo(main);
                showMe(data);

            }
        });
        return false;
    }

    function delInstallment(child, main) {
        var dataString = {
            process: 'actiondelrow',
            val1: main,
            val2: child
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                showInfo(main);
                showMe(data);

            }
        });
        return false;
    }

    function completeInstallment(did) {
        document.getElementById(did + "btncomp").innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='30'>";
        var dataString = {
            process: 'actioncomplete',
            val1: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                location.reload();
                showMe(data);

            }
        });
        return false;
    }

    function deleteInstallment(did) {
        document.getElementById(did + "btndele").innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='30'>";
        var dataString = {
            process: 'actiondelete',
            val1: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_process.php",
            data: dataString,
            cache: false,
            success: function(data) {
                location.reload();
                showMe(data);

            }
        });
        return false;
    }
</script>



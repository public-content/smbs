<?php
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$cont = new $pageModel();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
	$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet ">
                <div class="m-portlet__body  m-portlet__body--no-padding">
                    <div class="row m-row--no-padding m-row--col-separator-xl">
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <!--begin::Total Out-patient-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        Out-Patient
                                    </h4>
                                    <br>
                                    <?php 
                                        $total = number_format(array_sum($cont->totalOutpatient()),2);
                                        $usage = number_format($cont->usage('OUT'),2);
                                        $percentage = $cont->getPercentage($total, $usage);
                                        $number = $cont->totalStaff();
                                    ?>
                                    <span class="m-widget24__desc">
                                        Total for all <b> <?php echo $number; ?> Staff</b>
                                    </span>
                                    <span class="m-widget24__stats m--font-brand">
                                        <?php echo 'RM '.$total; ?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo $percentage; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                        Benefits Used <b>RM <?php  echo $usage; ?></b>
                                    </span>
                                    <span class="m-widget24__number">
                                        <?php echo $percentage; ?>
                                    </span>
                                </div>
                            </div>
                            <!--end::Out-patient-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <!--begin::Total In-Patient(benefits)-->
                            <?php 
//                                $totalin = number_format(array_sum($cont->totalInpatient('benefit')),2);
                               
                                $staffinB = $cont->totalStaffIP('benefit');                                
                                $staffinD = $cont->totalStaffIP('day');
                                $totalinB = number_format($cont->totalInpatient('benefit'),2);
                                $totalinBB = $cont->totalInpatient('benefit');
                                $totalinD = array_sum($cont->totalInpatient('day'));
                                $balinB = number_format(array_sum($cont->balanceIP('benefit')),2);
                                $balinBB = array_sum($cont->balanceIP('benefit'));
                                $balinD = array_sum($cont->balanceIP('day'));
                                $percentageB = $cont->getPercentage($totalinBB, $balinBB);
                                $percentageD = $cont->getPercentage($totalinD, $balinD);
                            ?>
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        In-Patient (Benefit)
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        Total for all <b><?php echo $staffinB; ?> Staff</b>
                                    </span>
                                    <span class="m-widget24__stats m--font-danger">
                                        RM <?php echo $totalinB; ?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $percentageB; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                        Benefits Used <b>RM <?php echo $balinB; ?></b>
                                    </span>
                                    <span class="m-widget24__number">
                                        <?php echo $percentageB; ?>
                                    </span>
                                </div>
                            </div>
                            <!--end::Total In-Patient(benefits)-->
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-4">
                            <!--begin::Total In-Patient(days)-->
                            <div class="m-widget24">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title">
                                        In-Patient (Days)
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        Total for all <b><?php echo $staffinD; ?> Staff</b>
                                    </span>
                                    <span class="m-widget24__stats m--font-danger">
                                        <?php  echo $totalinD; ?> Days
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $percentageD; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                        Days Used <b><?php echo $balinD; ?> Days</b>
                                    </span>
                                    <span class="m-widget24__number">
                                        <?php echo $percentageD; ?>
                                    </span>
                                </div>
                            </div>
                            <!--end::Total In-Patient(days)-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Fast search staff benefits records
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="col-lg-12">
                                <div class="form-group m-form__group row">
                                    <label for="example-search-input" class="col-2 col-form-label">
                                        Search by:
                                    </label>
                                    <div class="col-3">
                                        <select class="form-control m-input m-input--solid" id="searchtype">
                                            <option value="std">
                                                Staff ID
                                            </option>
                                            <option value="stc">
                                                Staff IC
                                            </option>
                                            <option value="stm">
                                                MRN
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <input class="form-control m-input m-input--solid" type="text" value="" id="thevalue">
                                    </div>
                                    <div class="col-1">
                                        <button type="button" class="btn btn-secondary m-btn m-btn--icon" onclick="postdid()">
                                            <span>
                                                <i class="la la-search"></i>
                                                <span>
                                                    Search
                                                </span>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-1" style="text-align:center;">
                                        <button class="btn btn-metal m-btn m-btn--icon" type="reset" onclick="closepage()">
                                            <span>
                                                <i class="la la-retweet"></i>
                                                <span>
                                                    Reset
                                                </span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="displayProfile">
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on("keypress", function(e) {
        if (e.which == 13) {
            postdid();
        }
    });

    function postdid() {
        document.getElementById('displayProfile').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var searchtype = document.getElementById('searchtype').value;
        var thevalue = document.getElementById('thevalue').value;
        var page = "page_profile";

        var dataString = {
            page: page,
            searchtype: searchtype,
            thevalue: thevalue
        };
        //        console.log(dataString);
        $.ajax({
            type: "post",
            url: "asset/view/modal_dashboard.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('displayProfile').innerHTML = html;
                //                       alert(searchtype+thevalue); 
            }
        });
        return false;
    }

    function closepage() {
        var page = "page_blanks";
        var dataString = {
            page: page
        };
        //        console.log(dataString);
        $.ajax({
            type: "post",
            url: "asset/view/modal_dashboard.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('displayProfile').innerHTML = html;
                //                       alert(searchtype+thevalue); 
            }
        });
        return false;
    }

    function delThis(xval, xtype, xvalue) {
        var searchtype = xtype;
        var thevalue = xvalue;
        var page = "page_profile";
        var msg = "popup";
        if (confirm("Are you sure!")) {
            var dataString = {
                msg: msg,
                did: xval,
                page: page,
                searchtype: searchtype,
                thevalue: thevalue
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_dashboard.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    document.getElementById('displayProfile').innerHTML = html;
                }
            });
            return false;
        } else {

        }
    }

    function editUser(did) {
        //        var page = "<?php echo $cont->securestring('encrypt','104') ?>";
        //        var url = "system.php?p="+page;
        //        location.replace(url + '&r=' + did);
        var dataString = {
            function: "edit",
            did: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_listofstaff.php",
            data: dataString,
            cache: false,
            success: function(html) {
                var dir = html;
                location.replace(dir);
            }
        });
        return false;
    }
</script>
<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = 'system.php?p='.$_GET['p'];

if(isset($_POST['btn-submit-form'])){
  // var_dump($_POST);
  $task = $cont->saveRole($_POST);
  if($task == 1){
    $pop = $msg->normalMessage('success','Success!','Role has been updated');
  }else{
    $pop = $msg->normalMessage('danger','Ohh no!',$task);
  }
}elseif(isset($_POST['btn-addRole'])){
  // var_dump($_POST);
  $task = $cont->addRole($_POST);
  if($task == 1){
    $pop1 = $msg->normalMessage('success','Success!','New Role has been add');
  }else{
    $pop1 = $msg->normalMessage('danger','Ohh no!',$task);
  }
}elseif(isset($_POST['btn-delRole'])){
  // var_dump($_POST);
  $task = $cont->delRoles($_POST);
  if($task == 1){
    $pop1 = $msg->normalMessage('success','Success!','Role has been deleted');
  }else{
    $pop1 = $msg->normalMessage('danger','Ohh no!',$task);
  }
}elseif(isset($_POST['btn-editRole'])){
  // var_dump($_POST);
  $task = $cont->editRole($_POST);
  if($task == 1){
    $pop1 = $msg->normalMessage('success','Success!','Role name has been add');
  }else{
    $pop1 = $msg->normalMessage('danger','Ohh no!',$task);
  }
}

$alert = $pop;
$alert1 = $pop1;
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="row">
                <div class="col-lg-3">
                    <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Role
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="col-lg-12"><?php if($alert1 != ''){print $alert1;} ?></div>
                            <?php
              $listrole = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'rolecategory' AND d_status = 'active'");
              if(mysql_num_rows($listrole)){
                while($rowrole = mysql_fetch_assoc($listrole)){
                  ?>
                            <table>
                                <tr>
                                    <td class="col-lg-8"><a href="#" class="m-link m--font" style="color:grey;" onclick="openRole('<?php echo $cont->securestring('encrypt',$rowrole['d_id']) ?>')"> - <?php echo $rowrole['d_value']; ?> </a></td>
                                    <?php if($rowrole['d_value2'] != 'master'){ ?>
                                    <td class="col-lg-2"><a href="#" class="m-link m-link--state m-link--danger" style="float:right;"> <i class="la la-trash" data-toggle="modal" data-target="#delRole<?php echo $rowrole['d_id'] ?>"></i> </a><a href="#" class="m-link m-link--state m-link--info" style="float:right;"> <i class="la la-edit" data-toggle="modal" data-target="#editRole<?php echo $rowrole['d_id'] ?>"></i> </a></td>
                                    <?php } ?>
                                    <div class="modal fade" id="delRole<?php echo $rowrole['d_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm" role="document">
                                            <form class="" action="" method="post">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Delete Role <?php echo $rowrole['d_value'] ?>
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">
                                                                &times;
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Do you wish to delete this ?</p>
                                                        <input type="hidden" name="d_id" value="<?php echo $rowrole['d_id']; ?>">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <button type="submit" name="btn-delRole" class="btn btn-danger">
                                                            Yes
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="editRole<?php echo $rowrole['d_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <form class="" action="" method="post">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">
                                                            Edit Role
                                                        </h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">
                                                                &times;
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group m-form__group">
                                                            <label for="exampleInputEmail1">
                                                                Role Name
                                                            </label>
                                                            <input type="text" class="form-control m-input m-input--square col-lg-6" id="" value="<?php echo $rowrole['d_value']; ?>" name="rolename" required>
                                                            <input type="hidden" name="d_id" value="<?php echo $rowrole['d_id']; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                            Close
                                                        </button>
                                                        <button type="submit" name="btn-editRole" class="btn btn-primary">
                                                            Edit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </tr>
                            </table>
                            <!-- <p></p> -->
                            <?php
                }
              }
              ?>
                            <p style="margin-top:30px;"><a href="#" class="m-link" data-toggle="modal" data-target="#addRole"> <i class="la la-plus"></i> Add Role</a></p>
                        </div>
                    </div>
                </div>

                <?php if($_GET['r'] != ''){ ?>
                <div class="col-lg-9">

                    <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        <?php echo $cont->getValue('tbl_setting','d_value',$cont->securestring('decrypt',$_GET['r'])); ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="col-lg-12"><?php if($alert != ''){print $alert;} ?></div>
                            <form class="" action="" method="post">
                                <div class="col-lg-12" style="text-align:right;height:50px;">
                                    <button type="submit" name="btn-submit-form" class="btn m-btn--pill btn-success btn-sm">
                                        Submit
                                    </button>
                                </div>
                                <input type="hidden" name="setRole" value="<?php echo $cont->securestring('decrypt',$_GET['r']); ?>">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr style="background:lightgrey;">
                                          
                                            <th>
                                                Page Name
                                            </th>
                                            <th>
                                                Action
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!--                                        display al page-->
                                        <?php
                                        $page = mysql_query("SELECT DISTINCT pageGroup FROM tbl_page WHERE status = 'active'");
                                           if(mysql_num_rows($page)){
                                               while($rowpage = mysql_fetch_assoc($page)){
                                                    $group = $rowpage['pageGroup'];
                                                    $chckgrpNum = $cont->groupPage($group);
                                                    $grp = mysql_query("SELECT * FROM tbl_page WHERE pageGroup = '$group' AND status = 'active'");
                                                     if($chckgrpNum == '1'){ 
                                                         if(mysql_num_rows($grp)){
                                                            while($rowgrp = mysql_fetch_assoc($grp)){
                                        ?>
                                        <tr>
                                            <th scope="row" colspan="2" style="background:#ffe;">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <?php echo $rowgrp['pageTitle']; if($rowgrp['pageTitleSmall'] != ''){ echo " - ".$rowgrp['pageTitleSmall']; } ?>
                                            </td>
                                            <td>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="<?php echo 'page'.$rowgrp['did']; ?>" value="<?php echo $rowgrp['did']; ?>" <?php if($cont->checkExist($rowgrp['did'], $cont->securestring('decrypt',$_GET['r'])) == 1){ print 'checked';} ?>>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <?php
                                                            }
                                                         }   
                                                     }else{
                                        ?>
                                        <tr>
                                            <th scope="row" colspan="2" style="background:#d3f0f0;">
                                                <?php echo $group; ?>
                                            </th>
                                        </tr>
                                        <?php 
                                                          if(mysql_num_rows($grp)){
                                                              while($rowgrp = mysql_fetch_assoc($grp)){
                                        ?>
                                        <tr>
                                            <td>
                                                <?php echo $rowgrp['pageTitle']; if($rowgrp['pageTitleSmall'] != ''){ echo " - ".$rowgrp['pageTitleSmall']; } ?>
                                            </td>
                                            <td>
                                                <label class="m-checkbox">
                                                    <input type="checkbox" name="<?php echo 'page'.$rowgrp['did']; ?>" value="<?php echo $rowgrp['did']; ?>" <?php if($cont->checkExist($rowgrp['did'], $cont->securestring('decrypt',$_GET['r'])) == 1){ print 'checked';} ?>>
                                                    <input type="checkbox" name="" value="<?php echo $rowgrp['did']; ?>">
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <?php
                                                              }
                                                          }
                                                     }
                                               }
                                           }
                                        ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>

                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="" action="" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Add Role
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">
                            Role Name
                        </label>
                        <input type="text" class="form-control m-input m-input--square col-lg-6" id="" name="rolename" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" name="btn-addRole" class="btn btn-primary">
                        Add
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function openRole(role) {
        var url = "system.php?p=a2ZkSGw3V2xGMWJRU3JpakVNV29lUT09";
        location.replace(url + '&r=' + role);
    }
</script>
<?php
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$cont = new $pageModel();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','200');
	$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="col-lg-12" id="passwordNotice">
                <?php if($passNoty != ''){ echo $passNoty; } ?>
                <?php if($cont->checkPassword($_SESSION['logid']) == 1){ ?>
                <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        <strong>
                            Warning !
                        </strong>
                        Please change your current password: 1234 to new password. This to prevent others from using your account.
                    </div>
                    <div class="m-alert__actions" style="width: 160px;">
                        <button type="button" class="btn btn-warning btn-sm m-btn m-btn--pill m-btn--wide" data-toggle="modal" data-target="#changePassword">
                            Change
                        </button>
                    </div>
                </div>
                <?php }  ?>
            </div>

            <div class="m-portlet m-portlet--bordered m-portlet--unair">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Total Medical Benefits
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <?php
							$befordate = "2005-03-07";
							$employmentdate = $cont->masterTake('tbl_labor', 'd_employment', $_SESSION['labid']);
							$martial = $cont->masterTake('tbl_profile', 'd_martial', $_SESSION['proid']);
							$dependent = $cont->dependentCount($cont->securestring('decrypt',$_SESSION['proid']));
//                            echo $dependent;
							$relic = $cont->masterTake('tbl_profile', 'd_relic', $_SESSION['proid']);
							
							if(strtotime($employmentdate) <= strtotime($befordate)){
                                    if($dependent == '0'){
                                        include('asset/component/sb-s.php');
                                    }else{
                                        include('asset/component/sb-m.php');
                                    }
							}else{
							      if($dependent == '0'){
                                        include('asset/component/sl-s.php');
                                    }else{
                                        include('asset/component/sl-m.php');
                                    }
							}
						?>
                    </div>
                </div>
            </div>

            <div class="m-portlet m-portlet--bordered m-portlet--unair">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                User Infomation
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="m-section">
                                <span class="m-section__sub">
                                    User Profile.
                                </span>
                                <div class="m-section__content">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Name
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_profile', 'd_name', $_SESSION['proid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Staff IC
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_profile', 'd_ic', $_SESSION['proid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Gender
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_profile', 'd_gender', $_SESSION['proid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Martial Status
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_profile', 'd_martial', $_SESSION['proid']) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Staff ID
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_labor', 'd_staffID', $_SESSION['labid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    MRN
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_profile', 'd_mrn', $_SESSION['proid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Date Joined
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_labor', 'd_employment', $_SESSION['labid']); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Department
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_setting', 'd_value', $cont->securestring('encrypt', $cont->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row" class="col-lg-4">
                                                    Designation
                                                </th>
                                                <td>
                                                    <?php echo $cont->masterTake('tbl_labor', 'd_designation', $_SESSION['labid']) ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="m-portlet m-portlet--full-height m-portlet--unair m-portlet--bordered">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Family Members
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="m_widget4_tab1_content">
                                            <div class="m-widget4 m-widget4--progress">
                                                <?php 
													if($cont->dependent($_SESSION['proid']) != '0'){
														$dep = $cont->securestring('decrypt',$_SESSION['proid']);
														$listdep = mysql_query("SELECT d_name,d_relstatus,d_mrn,d_gender FROM tbl_profile WHERE d_dependent = '$dep'");
														if(mysql_num_rows($listdep)){
															while($rowdep = mysql_fetch_assoc($listdep)){
															?>
                                                <div class="m-widget4__item">
                                                    <div class="m-widget4__img m-widget4__img--pic">
                                                        <img src="<?php echo $cont->getDependentImage($rowdep['d_relstatus'],$rowdep['d_gender']); ?>" alt="">
                                                    </div>
                                                    <div class="m-widget4__info">
                                                        <span class="m-widget4__title">
                                                            <?php echo $rowdep['d_name']; ?>
                                                        </span>
                                                        <br>
                                                        <span class="m-widget4__sub">
                                                            <?php echo $rowdep['d_relstatus']; ?> - [ MRN : <?php echo $rowdep['d_mrn']; ?>]
                                                    </div>
                                                    <div class="m-widget4__progress">

                                                    </div>
                                                    <div class="m-widget4__ext">
                                                        <!-- <a href="#" class="m-btn m-btn--hover-brand m-btn--pill btn btn-sm btn-secondary">
																			View
																		</a> -->
                                                    </div>
                                                </div>
                                                <?php
																}
															}
															
															}else{
														?>
                                                <td>
                                                    <i>There is no records of your family members.<br /><br />
                                                        If your family member name not appear:-<br />
                                                        1. It was not entered in <b>KAIZEN ESS</b><br />
                                                        2. It has not been update to SMBS by HR yet<br />
                                                        3. They are not covered by the hospital</i>
                                                </td>
                                                <?php
														} 
													?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


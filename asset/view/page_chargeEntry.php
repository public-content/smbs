<?php
  $pageModel = $general->pageInfo($page,'pageDir');
  $pageTitle = $general->pageInfo($page,'pageTitle');
  $pageName = $general->pageInfo($page,'pageName');
  $pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
  $main->includePHP('model',$pageModel);
  $cont = new $pageModel();
  $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
?>

<?php
    if(isset($_POST['btn_submit'])){
        var_dump($_POST);
    }
?>

<style>
    .red {
        color: red
    }
    
    .ti{
        font-weight: 500;
        text-decoration: underline;
        margin-bottom: 20px;
    }
    
    .ti-small{
        
    }
    
    .tr-head{
        font-weight: 500;
    }
    
    .tb-benefit-chil{
        margin-left: 20px;
        margin-bottom: 20px;
    }

</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-12 m--hide" style="margin-top:10px;" id="alertme"></div>
        <div class="m-content">
            <div class="row">
                <div class="col-lg-12" id="search-col">
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label for="example-search-input" class="col-2 col-form-label">
                                    Search by:
                                </label>
                                <div class="col-2">
                                    <select class="form-control m-input m-input--air" id="searchtype" onchange="searchUsing()">
                                        <option value="usingID">
                                            Staff ID
                                        </option>
                                        <option value="usingIC">
                                            Staff IC
                                        </option>
                                        <option value="usingMRN">
                                            MRN
                                        </option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <input class="form-control m-input m-input--air" type="text" value="" id="searchvalue" oninput="finder()">
                                    <span class="m-form__help" id="displayUser">

                                    </span>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-secondary m-btn m-btn--icon" onclick="postForm()">
                                        <span>
                                            <i class="la la-search"></i>
                                            <span>
                                                Charges
                                            </span>
                                        </span>
                                    </button>
                                </div>
                                <div class="col-1" style="text-align:center;">
                                    <button class="btn btn-metal m-btn m-btn--icon" type="reset" onclick="closepage()">
                                        <span>
                                            <i class="la la-retweet"></i>
                                            <span>
                                                Reset
                                            </span>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8"><div class="m-portlet m-portlet--tab m--hide" id="form-col" style="min-height: 200px;"></div></div>
                <div class="col-lg-4"><div class="m-portlet m-portlet--tab m--hide"  id="info-col" style="min-height: 200px;"></div></div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    <?php if($_GET['i'] != ''){ 
        $labid = $cont->securestring('decrypt',$_GET['i']);
        $ID = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_staffID');
    ?>
        postForm2(<?php echo $ID; ?>,'usingID');
    <?php } ?>
    
//    whenLoad();
    
    function whenLoad() {
        searchBar();
    }

    function searchBar() {
        var dataString = {
            contents: 'searchBar'
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('search-col').innerHTML = html;
            }
        });
        return false;
    }
    
    function searchUsing(){
        finder();
    }
    
    function finder() {
        document.getElementById('displayUser').innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='20'>";
        var type = document.getElementById('searchtype').value;
        var value = document.getElementById('searchvalue').value;
        var dataString = {
            search: type,
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('displayUser').innerHTML = html;
            }
        });
        return false;
    }
    
    function postForm() {
        document.getElementById('form-col').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        var type = document.getElementById('searchtype').value;
        var value = document.getElementById('searchvalue').value;
        var dataString = {
            form: "form",
            val1: type,
            val2: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('form-col').innerHTML = html;
                $("#form-col").removeClass('m--hide');
                icnumbers();
                patientmrns();
            }
        });
        return false;
    }
    
    
    
    function postForm2(value,type) {
        document.getElementById('form-col').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        var dataString = {
            form: "form",
            val1: type,
            val2: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('form-col').innerHTML = html;
                $("#form-col").removeClass('m--hide');
                icnumbers();
                patientmrns();
            }
        });
        return false;
    }
    
    function treatmentType(value){
        var dataString = {
            treatmentType: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                if(value == 'IN'){
                  $("#treatmentlist").removeClass('m--hide');  
                }
                document.getElementById('treatmentname').innerHTML = html;
            }
        });
        return false;
    }
    
    function treatmentNames(value){
        var dataString = {
            treatmentName: value
        };
//        alert(value);
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('treatmentlist').innerHTML = html;
            }
        });
        return false;
    }

    
    function icnumbers(){
        document.getElementById('infoname').innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='20'>";
        var value = document.getElementById('icnumber').value;
        var dataString = {
            target: "staffid",
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                var obj = $.parseJSON(result); // new object of result
                var staffid = obj.staffid;
                var name = obj.name;
                document.getElementById('infoname').innerHTML = name;
                $("#staffid").val(staffid);
                infoColumn(value);
            }
        });
        return false;
    }
    
    function patientmrns(){
        document.getElementById('infomrn').innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='20'>";
        var value = document.getElementById('patientmrn').value;
        var dataString = {
            target: "patientmrn",
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                document.getElementById('infomrn').innerHTML = result;
            }
        });
        return false;
    }
    
    function doctorcodes(){
        document.getElementById('infodoctor').innerHTML = "<img src='images/system/gif/loading.gif' alt='' width='20'>";
        var value = document.getElementById('doctorcode').value;
        var dataString = {
            target: "doctorcode",
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                document.getElementById('infodoctor').innerHTML = result;
            }
        });
        return false;
    }
    
    function infoColumn(value){
        if(value == null){
            $("#info-col").removeClass('m--hide');
            document.getElementById('info-col').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><span style='color:red;'> No User found </span></div>";
        }else{
            $("#info-col").removeClass('m--hide');
        document.getElementById('info-col').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var dataString = {
            infomations: "userInfo",
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                document.getElementById('info-col').innerHTML = result;
            }
        });
        return false;
        }
    }
    
    function checkExceed(){
        var labid = document.getElementById('staffid').value;
        var type = document.getElementById('treatmenttype').value;
        var value = document.getElementById('charges').value;
        var dataString = {
            checker: "charges",
            val1: labid,
            val2: type,
            val3: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                $("#exceed").val(result);
            }
        });
        return false;
    }
    
    function submitions(){
        var icnumber = document.getElementById('icnumber').value;
        var staffid = document.getElementById('staffid').value;
        var datevisit = document.getElementById('datevisit').value;
        var patientmrn = document.getElementById('patientmrn').value;
        var treatmenttype = document.getElementById('treatmenttype').value;
        var treatmentname = document.getElementById('treatmentname').value;
        var treatmentlist = document.getElementById('treatmentlist').value;
        var episode = document.getElementById('episode').value;
        var noofday = document.getElementById('noofday').value;
        var charges = document.getElementById('charges').value;
        var billno = document.getElementById('billno').value;
        var doctorcode = document.getElementById('doctorcode').value;
        var exceed = document.getElementById('exceed').value;
        var remark = document.getElementById('remark').value;
       
        
        if(icnumber == '' || staffid == '' || datevisit == '' || patientmrn == '' || treatmenttype == '' || treatmentname == '' || charges == '' || doctorcode == '' || billno == ''){
            alert("Please fill the form");
        }
        
        if(treatmenttype == 'IN'){
            
            if(noofday == ''){
                alert("Please fill the form");
            }
        }
        var dataString = {
            submitions: "submit",
            icnumber: icnumber,
            staffid: staffid,
            datevisit: datevisit,
            patientmrn: patientmrn,
            treatmenttype: treatmenttype,
            treatmentname: treatmentname,
            treatmentlist: treatmentlist,
            episode: episode,
            noofday: noofday,
            charges: charges,
            billno: billno,
            doctorcode: doctorcode,
            exceed: exceed,
            remark: remark
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_chargeEntry1.php",
            data: dataString,
            cache: false,
            success: function(result) {
                $("#alertme").removeClass('m--hide');

                $(window).scrollTop(0);
                console.log(result);
                var obj = $.parseJSON(result); // new object of result
                var msg = obj.msg;
                var refresh = obj.refresh;
                    document.getElementById('alertme').innerHTML = msg;
                    if(refresh == 'yes'){
                        setTimeout(function() {
                            location.reload();
                        }, 5000);
                    }

            }
        });
        return false;
    }

</script>

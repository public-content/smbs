<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }
    
    .header-l{
        font-size: 22px;
        font-weight: 500;
        text-align: center;
        border-bottom: 1px black solid;
        margin: 0 20px;
    }
    
    .cus-1{
        font-size: 54px;
        font-weight: 500;
        text-align: center;
    }
    
    .title-l{
        font-size: 16px;
        font-weight: 400;
        padding: 0px 20px 20px 0px;
        text-decoration: underline dotted;
    }
    .tr-1{
        border-bottom: 1px black solid;
    }
    .tr-2{
        border-bottom: 1px #c1c1c1 dotted;
    }
    
    .td-1{
        width: 100%;
    }
    
    .td-2{
        font-size: 40px;
        font-weight: 500;
    }
    
    .td-3{
    }
    
    .td-4{
        text-align: right;
    }
    
    .td-5{
        font-size: 20px;
        font-weight: 500;
        text-align: right;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                    <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                        <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" onclick="btnRefresh()">
                            <i class="la la-refresh"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="col-lg-12"><div id="organization-info"></div></div>
            <div class="col-lg-12"><div id="registered-user"></div></div>
            <div class="col-lg-12"><div id="staff-status"></div></div>
            <div class="col-lg-12"><div id="benefit-usage"></div></div>
            <div class="col-lg-12"><div id="benefit-all"></div></div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    
    btnRefresh();
    
    function btnRefresh(){
        organizationInfoPost();
        registerefUserPost();
        staffStatusPost();
        benefitUsagePost();
    }

    
    function organizationInfoPost(){
        document.getElementById('organization-info').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";

        var dataString = {
            display: "organization-info"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('organization-info').innerHTML = html;
            }
        });
        return false;
    }
    
    function registerefUserPost(){
        document.getElementById('registered-user').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";

        var dataString = {
            display: "registered-user"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('registered-user').innerHTML = html;
            }
        });
        return false;
    }
    
    function staffStatusPost(){
        document.getElementById('staff-status').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";

        var dataString = {
            display: "staff-status"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('staff-status').innerHTML = html;
            }
        });
        return false;
    }
    
    function benefitUsagePost(){
        document.getElementById('benefit-usage').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";

        var dataString = {
            display: "benefit-usage"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('benefit-usage').innerHTML = html;
                document.getElementById('usage-list').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
                setTimeout(function() {
                    benefitUsageList();
                }, 5000);
            }
        });
        return false;
    }
    
    function benefitUsageList(){
        document.getElementById('usage-list').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var startdate = document.getElementById('startdate').value;
        var endate = document.getElementById('endate').value;
        console.log(startdate + " - " + endate);
        var dataString = {
            display: "usage-list",
            var1: startdate,
            var2: endate
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('usage-list').innerHTML = html;
            }
        });
        return false;
        
        
    }
    
    function benefitAllPost(){
        document.getElementById('benefit-all').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";

        var dataString = {
            display: "benefit-all"
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_generaltotal.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('benefit-all').innerHTML = html;
            }
        });
        return false;
    }
    

</script>
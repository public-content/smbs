<?php 
    //head
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();

     function getMrn($col, $mrn){
        $sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_mrn = '$mrn'");
        $row = mysql_fetch_assoc($sql);
        return $row[$col];
    }

    function getStaffID($did){
		$sql = mysql_query("SELECT d_staffID FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_staffID'];
	}
?>


<?php
    //action function
    if(isset($_POST['action'])){
        $action = mysql_real_escape_string(trim($_POST['action']));
        if($action == 'check'){
            $getDid = mysql_real_escape_string(trim($_POST['val1']));
            $sql = mysql_query("UPDATE tbl_treatment SET d_treatCheck = 'YES', d_modified = NOW() WHERE d_id = '$getDid'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Ohh No!
                            </strong>
                            ".mysql_error()."
                        </div>";
            }else{
//                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
//                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
//                            <strong>
//                                Well done
//                            </strong>
//                            Treatment has been update
//                        </div>";
            }
        }elseif($action == 'uncheck'){
            $getDid = mysql_real_escape_string(trim($_POST['val1']));
            $sql = mysql_query("UPDATE tbl_treatment SET d_treatCheck = 'NO', d_modified = NOW() WHERE d_id = '$getDid'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Ohh No!
                            </strong>
                            ".mysql_error()."
                        </div>";
            }else{
//                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
//                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
//                            <strong>
//                                Well done
//                            </strong>
//                            Treatment has been update
//                        </div>";
            }
        }elseif($action == 'printPage'){
            $type = $general->securestring(encrypt,$_POST['val1']);
            $start = $general->securestring('encrypt',mysql_real_escape_string($_POST['val2']));
            $end = $general->securestring('encrypt',mysql_real_escape_string($_POST['val3']));
            $url = $type."&start=".$start."&end=".$end;
            echo "report_bo.php?type=".$url;
        }
    }
?>


<?php 
    //page display
    if(isset($_POST['page'])){
        $page = mysql_real_escape_string(trim($_POST['page']));
        if($page == 'pageHistory'){
            $startDate = date('Y-m-d', strtotime($_POST['val1']));
            $endDate = date('Y-m-d', strtotime($_POST['val2']));
?>
<div class="m-portlet m-portlet--bordered m-portlet--unair">
    <div class="m-portlet__body">
        <div class="col-lg-12" style="" id="notification"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <div class="row">
                <h4 class="m-widget24__title col-lg-8">
                    Out-Patient Benefits
                </h4>
                <span class="col-lg-2">
                    <button type="button" class="btn btn-primary" onclick="postPrint('OUT')" style="float:right">
                        <i class="la la-print"></i> Print
                    </button>
                </span>
            </div>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Dept
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Responsible
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
				        $bil=1;
				        $year='2019'; $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_status = 'active' AND d_vdate BETWEEN '$startDate' AND '$endDate' "); 
				        if(mysql_num_rows($listOut)){
				            while($rowOut=mysql_fetch_assoc($listOut)){
                                if($rowOut['d_treatCheck'] == 'NO'){
                                    $dcolor = '';
                                    $dbutton = "<button type='button' class='btn btn-success' onclick='checkThis1(".$rowOut['d_id'].")'>Check</button>";
                                }else{
                                    $dcolor = 'background:#8700004d';
                                    $dbutton = "<button type='button' class='btn btn-info' onclick='checkThis2(".$rowOut['d_id'].")'>Uncheck</button>"; 
                                }
				    ?>
                    <tr style='<?php echo $dcolor; ?>'>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->allTable($general->allTable($rowOut['d_staffID'],'d_staffID','tbl_labor','d_department'),'d_id','tbl_setting','d_value'); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_who']; ?>
                        </td>
                        <td>
                            <?php echo $dbutton; ?>
                        </td>
                    </tr>
                    <?php $bil++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12" style="height:40px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->
            <div class="row">
                <h4 class="m-widget24__title col-lg-8">
                    In-Patient Benefits
                </h4>
                <span class="col-lg-2">
                    <button type="button" class="btn btn-primary" onclick="postPrint('IN')" style="float:right">
                        <i class="la la-print"></i> Print
                    </button>
                </span>
            </div>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Department
                        </th>
                        <th>
                            Days
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Responsible
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $bill=1;
                        $year='2019'; $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_status = 'active' AND d_vdate BETWEEN '$startDate' AND '$endDate'"); 
                        if(mysql_num_rows($listOut)){
                            while($rowOut=mysql_fetch_assoc($listOut)){
                                if($rowOut['d_treatCheck'] == 'NO'){
                                    $dcolor = '';
                                    $dbutton = "<button type='button' class='btn btn-success' onclick='checkThis1(".$rowOut['d_id'].")'>Check</button>";
                                }else{
                                    $dcolor = 'background:#8700004d';
                                    $dbutton = "<button type='button' class='btn btn-info' onclick='checkThis2(".$rowOut['d_id'].")'>Uncheck</button>"; 
                                }
                    ?>
                    <tr style='<?php echo $dcolor; ?>'>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->allTable($general->allTable($rowOut['d_staffID'],'d_staffID','tbl_labor','d_department'),'d_id','tbl_setting','d_value'); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_day']; ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_who']; ?>
                        </td>
                        <td>
                            <?php echo $dbutton; ?>
                        </td>
                    </tr>
                    <?php $bill++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
        }
    }
?>
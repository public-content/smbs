<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
?>

<?php 
    function showAllStaff($type){
        if($type == 'all'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF'");
        }elseif($type == 'active'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND d_status = 'active'");
        }elseif($type == 'deactive'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND (d_status = 'deactive' OR d_status = 'deleted')");
        }
        
        return mysql_num_rows($sql);
    }

    function findValue($gender, $marital, $type){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = '$type' AND d_gender = '$gender' AND d_martial = '$marital' AND d_status = 'active'");
        return mysql_num_rows($sql);
    }

    function findValueTotal($marital, $type){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = '$type' AND d_martial = '$marital' AND d_status = 'active'");
        return mysql_num_rows($sql);
    }

    function findSpouseChild($marital, $rel){
     
                  if($rel == 'SPOUSE'){
                    $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = ANY(SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND d_martial = '$marital' AND d_status = 'active') AND (d_relstatus = 'WIFE') AND d_status = 'active'");
                    $result = mysql_num_rows($sql);
                  }else{
                    $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = ANY(SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND d_martial = '$marital' AND d_status = 'active') AND (d_relstatus = 'CHILDREN') AND d_status = 'active'");
                    $result = mysql_num_rows($sql); 
                  }
        
        return $result;
                  
    }

    function findCustom($start, $end, $type, $group){
        $startdate = date("Y-m-d", strtotime($start));
        $enddate = date("Y-m-d", strtotime($end));
        if($group == 'staff'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group != '85' AND d_category != '19' ) AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_employment > '2005-03-07' AND (d_category != '19' OR d_category != '22' OR d_category != '23') AND d_group != '85') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_fetch_assoc($sql);
        }elseif($group == 'consultant'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group = '85' AND d_category != '19') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_employment > '2005-03-07' AND (d_category != '19' OR d_category != '22' OR d_category != '23') AND d_group = '85') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_fetch_assoc($sql);
        }elseif($group == 'infinite'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_category = '19') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE (d_category = '19' OR d_category = '22' OR d_category = '23')) AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_fetch_assoc($sql);
        }elseif($group == 'dependents'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_mrn = ANY(SELECT d_mrn FROM tbl_profile WHERE d_type = 'DEPENDENTS') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_mrn = ANY(SELECT d_mrn FROM tbl_profile WHERE d_type = 'DEPENDENTS') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_fetch_assoc($sql);
        }
        
        return $row['total'];
    }

    function findCustomii($start, $end, $type, $group){
        $startdate = date("Y-m-d", strtotime($start));
        $enddate = date("Y-m-d", strtotime($end));
        if($group == 'staff'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group != '85' AND d_category != '19') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_employment > '2005-03-07' AND (d_category != '19' OR d_category != '22' OR d_category != '23') AND d_group != '85') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_num_rows($sql);
        }elseif($group == 'consultant'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group = '85' AND d_category != '19') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_employment > '2005-03-07' AND (d_category != '19' OR d_category != '22' OR d_category != '23') AND d_group = '85') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_num_rows($sql);
        }elseif($group == 'infinite'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_category = '19') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE (d_category = '19' OR d_category = '22' OR d_category = '23')) AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_num_rows($sql);
        }elseif($group == 'dependents'){
            if($type == 'OUT'){
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_mrn = ANY(SELECT d_mrn FROM tbl_profile WHERE d_type = 'DEPENDENTS') AND d_type = 'OUT' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }else{
                $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_mrn = ANY(SELECT d_mrn FROM tbl_profile WHERE d_type = 'DEPENDENTS') AND d_type = 'IN' AND (d_vdate BETWEEN '$startdate' AND '$enddate') AND d_status = 'active'");
            }
            $row = mysql_num_rows($sql);
        }
        
        return $row;
    }

    function showAllDependent($type){
        if($type == 'all'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND (d_relstatus = 'WIFE' OR d_relstatus = 'CHILDREN') AND d_status = 'active'");
        }elseif($type == 'wife'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_relstatus = 'WIFE' AND d_status = 'active'");
        }elseif($type == 'children'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_relstatus = 'CHILDREN' AND d_status = 'active'");
        }
        
        return mysql_num_rows($sql);
    }

    function organizationInfo($type){
        if($type == 'name'){
            $result = "KPJ IPOH SPECIALIST HOSPITAL";
        }elseif($type == 'address'){
            $result = "26, Jalan Raja Dihilir, <br>30350 Ipoh, <br>Perak Darul Ridzuan";
        }elseif($type == 'contact'){
            $result = "05-2408777 <i class='la la-phone'></i> <br>05-2541388 <i class='la la-print'></i> <br><a href='mailto:kpjipoh@kpjipoh.com' title='Send Email'>kpjipoh@kpjipoh.com </a> <i class='la la-envelope'></i>";
        }elseif($type == 'website'){
            $result = "<a href='https://kpjipoh.com/' title='Website'>www.kpjipoh.com</a> <i class='la la-globe'></i>";
        }
        
        return $result;
    }
?>


<?php 
    if(isset($_POST['display'])){
        if($_POST['display'] == ''){
?>
    
<?php
        }elseif($_POST['display'] == 'organization-info'){
?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__body ">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-l">Organization Infomation</div>
                <table style="width:100%">
                    <tr class="tr-2"><td class="td-3">Organization Name</td><td class="td-4"><?php echo organizationInfo('name'); ?></td></tr>
                    <tr class="tr-2"><td class="td-3">Address</td><td class="td-4"><?php echo organizationInfo('address'); ?></td></tr>
                    <tr class="tr-2"><td class="td-3">Contact</td><td class="td-4"><?php echo organizationInfo('contact'); ?></td></tr>
                    <tr class=""><td class="td-3">Website</td><td class="td-4"><?php echo organizationInfo('website'); ?></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
        }elseif($_POST['display'] == 'registered-user'){
?>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__body ">
        <div class="row">
            <div class="col-lg-5">
                <div class="title-l">Staff</div>
                <table>
                    <tr class="tr-1"><td class="td-1">Registered Staff</td><td class="td-2"><?php echo showAllStaff('all'); ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Active</td><td class="td-2"><?php echo showAllStaff('active'); ?></td></tr>
                    <tr class=""><td class="td-1">Deactive</td><td class="td-2"><?php echo showAllStaff('deactive'); ?></td></tr>
                </table>
            </div>
            <div class="col-lg-2" style=""></div>
            <div class="col-lg-5">
                <div class="title-l">Dependents / Relative</div>
                <table>
                    <tr class="tr-1"><td class="td-1">Registered Dependents</td><td class="td-2"><?php echo showAllDependent('all'); ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Spouse</td><td class="td-2"><?php echo showAllDependent('wife'); ?></td></tr>
                    <tr class=""><td class="td-1">Children</td><td class="td-2"><?php echo showAllDependent('children'); ?></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
        }elseif($_POST['display'] == 'staff-status'){
?>
         <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body ">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="title-l">Single</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'SINGLE', 'STAFF'); ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'SINGLE', 'STAFF') ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('SINGLE','STAFF') ?></td></tr>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="title-l">Married</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'MARRIED', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'MARRIED', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Spouse</td>  <td class="td-5"><?php echo findSpouseChild('MARRIED', 'SPOUSE'); ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Children (under 18)</td>  <td class="td-5"><?php echo findSpouseChild('MARRIED', 'CHILDREN'); ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('MARRIED','STAFF') ?></td></tr> 
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="title-l">Widow</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'DIVORCE', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'DIVORCE', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Children (under 18)</td>  <td class="td-5"><?php echo findSpouseChild('DIVORCE', 'CHILDREN'); ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('DIVORCE','STAFF') ?></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<?php
        }elseif($_POST['display'] == 'benefit-usage'){
?>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body ">
                <div class="col-lg-12">
                    <div class="title-l">Benefit Usage</div>
                    <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__body">
                            <?php $startdate = "1/1/".date('Y'); $todaydate=date('m/d/Y'); ?>
                            <form>
                                <div class="form-group m-form__group row">
                                    <label class="col-form-label col-lg-2 col-sm-12">
                                        Date range
                                    </label>
                                    <div class="col-lg-6 col-md-9 col-sm-12">
                                        <div class="input-daterange input-group" id="m_datepicker_5">
                                            <input type="text" class="form-control m-input" name="startdate" id="startdate" value="<?php echo $startdate; ?>">
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-ellipsis-h"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" name="endate" id="endate" value="<?php echo $todaydate; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-sm-12">
                                        <button type="button" class="btn btn-secondary" onclick="benefitUsageList()">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body" id='usage-list'></div>
            </div>
        </div>
<?php
        }elseif($_POST['display'] == 'usage-list'){
        $startdate = $_POST['var1'];
        $enddate = $_POST['var2'];
?>
        <div class="row">
            <div class="col-lg-4">
                <div class="title-l">Staff</div>
                <table style="width:100%">
                    <tr class="tr-2">
                        <td class="td-3">OutPatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'OUT','staff'),2)." / ".findCustomii($startdate,$enddate,'OUT','staff')." staff"; ?></td>
                    </tr>
                    <tr class="">
                        <td class="td-3">Inpatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'IN','staff'),2)." / ".findCustomii($startdate,$enddate,'IN','staff')." staff"; ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4">
                <div class="title-l">Consultant</div>
                <table style="width:100%">
                    <tr class="tr-2">
                        <td class="td-3">OutPatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'OUT','consultant'),2)." / ".findCustomii($startdate,$enddate,'OUT','consultant')." consultant"; ?></td>
                    </tr>
                    <tr class="">
                        <td class="td-3">Inpatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'IN','consultant'),2)." / ".findCustomii($startdate,$enddate,'IN','consultant')." consultant"; ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4">
                <div class="title-l">Infinite</div>
                <table style="width:100%">
                    <tr class="tr-2">
                        <td class="td-3">OutPatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'OUT','infinite'),2)." / ".findCustomii($startdate,$enddate,'OUT','infinite')." user"; ?></td>
                    </tr>
                    <tr class="">
                        <td class="td-3">Inpatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'IN','infinite'),2)." / ".findCustomii($startdate,$enddate,'IN','infinite')." user"; ?></td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-4" style="margin-top:30px;">
                <div class="title-l">Relatives</div>
                <table style="width:100%">
                    <tr class="tr-2">
                        <td class="td-3">OutPatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'OUT','dependents'),2)." / ".findCustomii($startdate,$enddate,'OUT','dependents')." relative"; ?></td>
                    </tr>
                    <tr class="">
                        <td class="td-3">Inpatient</td>
                        <td class="td-5"><?php echo "RM ".number_format(findCustom($startdate,$enddate,'IN','dependents'),2)." / ".findCustomii($startdate,$enddate,'IN','dependents')." relative"; ?></td>
                    </tr>
                </table>
            </div>
        </div>
<?php            
        }elseif($_POST['display'] == 'benefit-all'){
?>
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body ">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="title-l">Single</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'SINGLE', 'STAFF'); ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'SINGLE', 'STAFF') ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('SINGLE','STAFF') ?></td></tr>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="title-l">Married</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'MARRIED', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'MARRIED', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Spouse</td>  <td class="td-5"><?php echo findSpouseChild('MARRIED', 'SPOUSE'); ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Children (under 18)</td>  <td class="td-5"><?php echo findSpouseChild('MARRIED', 'CHILDREN'); ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('MARRIED','STAFF') ?></td></tr> 
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="title-l">Widow</div>
                        <table style="width:100%">
                            <tr class="tr-2"><td class="td-3">Male</td>  <td class="td-5"><?php echo findValue('MALE', 'DIVORCE', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Female</td>  <td class="td-5"><?php echo findValue('FEMALE', 'DIVORCE', 'STAFF') ?></td></tr>
                            <tr class="tr-2"><td class="td-3">Children (under 18)</td>  <td class="td-5"><?php echo findSpouseChild('DIVORCE', 'CHILDREN'); ?></td></tr>
                            <tr class=""><td class="td-3">Total</td>  <td class="td-5"><?php echo findValueTotal('DIVORCE','STAFF') ?></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<?php
        }
    }
?>



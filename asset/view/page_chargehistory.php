<?php
  $pageModel = $general->pageInfo($page,'pageDir');
  $pageTitle = $general->pageInfo($page,'pageTitle');
  $pageName = $general->pageInfo($page,'pageName');
  $pageTitleSmall = $general->pageInfo($page,'pageTitleSmall');
  $main->includePHP('model',$pageModel);
  $cont = new $pageModel();
  $homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
//  $pageDir = 'system.php?p='.$_GET['p'];
  $pageDir = $_SERVER['REQUEST_URI'];
?>
<style>
    .red {
        color: red
    }

    ;
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-portlet m-portlet--bordered m-portlet--unair">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            Staff Medical History
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__body">
                                <form>
                                   <?php $dnow=date('m/d/Y'); $dnow2=date('Y-m-d'); ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-form-label col-lg-2 col-sm-12">
                                            Date range
                                        </label>
                                        <div class="col-lg-6 col-md-9 col-sm-12">
                                            <div class="input-daterange input-group" id="m_datepicker_5">
                                                <input type="text" class="form-control m-input" name="startdate" id="startdate" value="<?php echo $dnow; ?>" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-ellipsis-h"></i>
                                                    </span>
                                                </div>
                                                <input type="text" class="form-control" name="endate" id="endate" value="<?php echo $dnow; ?>" />
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-sm-12">
                                            <button type="button" class="btn btn-secondary" onclick="postDate()">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-12" id="getHistory">
                    <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__body">
                            <div class="col-lg-12" style="" id="notification"></div>
                            <div class="col-lg-12" style="border-bottom:1px lightgrey solid; margin:20px 0px;"></div>
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <!--begin::Total Out-patient-->

                                <h4 class="m-widget24__title">
                                    Out-Patient Benefits
                                </h4>

                                <!--end::Out-patient-->
                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
                                <table class="table m-table m-table--head-separator-metal">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                MRN
                                            </th>
                                            <th>
                                                Episode
                                            </th>
                                            <th>
                                                Dept
                                            </th>
                                            <th>
                                                Amount
                                            </th>
                                            <th>
                                                Bill No
                                            </th>
                                            <th>
                                                Remarks
                                            </th>
                                            <th>
                                                Doctor
                                            </th>
                                            <th>
                                                Date
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="background:#d3d3d34d;">
                                        <?php 
											$bil=1;
											$year=date('Y'); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_status = 'active' AND d_vdate BETWEEN '$dnow2' AND '$dnow2'"); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
												?>
                                        <tr>
                                            <th scope="row">
                                                <?php echo $bil; ?>
                                            </th>
                                            <td>
                                                <?php echo $cont->getMrn('d_name',$rowOut['d_mrn']); ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_mrn']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_episode']; ?>
                                            </td>
                                            <td>
                                                <?php echo $cont->masterTake('tbl_setting', 'd_value', $cont->securestring('encrypt', $cont->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                                            </td>
                                            <td>
                                                RM <?php echo $rowOut['d_charge']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_bill']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_doc']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_created']; ?>
                                            </td>
                                            <td>
                                               <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                                            </td>
                                        </tr>
                                        <?php $bil++; }}else{ ?>
                                        <tr>
                                            <td colspan="12" style="text-align:center;background:#FEFEE;">
                                                No Data
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12" style="height:40px;"></div>
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <!--begin::Total Out-patient-->

                                <h4 class="m-widget24__title">
                                    In-Patient Benefits
                                </h4>

                                <!--end::Out-patient-->
                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
                                <table class="table m-table m-table--head-separator-metal">
                                    <thead>
                                        <tr>
                                            <th>
                                                No
                                            </th>
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                MRN
                                            </th>
                                            <th>
                                                Episode
                                            </th>
                                            <th>
                                                Department
                                            </th>
                                            <th>
                                                Days
                                            </th>
                                            <th>
                                                Amount
                                            </th>
                                            <th>
                                                Bill No
                                            </th>
                                            <th>
                                                Remarks
                                            </th>
                                            <th>
                                                Doctor
                                            </th>
                                            <th>
                                                Date
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody style="background:#d3d3d34d;">
                                        <?php 
											$bill=1;
											$year=date('Y'); $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_status = 'active' AND d_vdate BETWEEN '$dnow2' AND '$dnow2'"); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
								        ?>
                                        <tr>
                                            <th scope="row">
                                                <?php echo $bil; ?>
                                            </th>
                                            <td>
                                                <?php echo $cont->getMrn('d_name',$rowOut['d_mrn']); ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_mrn']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_episode']; ?>
                                            </td>
                                            <td>
                                                <?php echo $cont->masterTake('tbl_setting', 'd_value', $cont->securestring('encrypt', $cont->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                                            </td>
                                            <td>
                                                RM <?php echo $rowOut['d_day']; ?>
                                            </td>
                                            <td>
                                                RM <?php echo $rowOut['d_charge']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_bill']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_doc']; ?>
                                            </td>
                                            <td>
                                                <?php echo $rowOut['d_created']; ?>
                                            </td>
                                            <td>
                                                <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                                            </td>
                                        </tr>
                                        <?php $bill++; }}else{ ?>
                                        <tr>
                                            <td colspan="12" style="text-align:center;background:#FEFEE;">
                                                No Data
                                            </td>
                                        </tr>
                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function delThis(did){
        if (confirm("Are you sure!")) {
            var dataString = { val1 : did, action : "delThis" };
            $.ajax({
                type:"post",
                url:"asset/view/modal_chargehistory.php",
                data:dataString,
                cache:false,
                success: function(html){
                    document.getElementById('notification').innerHTML = html;
                    postDate();
                }
            });
            return false;  
      } else {
          
      }
    }
    
    function postDate(){
        var startdate = document.getElementById('startdate').value;
        var enddate = document.getElementById('endate').value;
        var page = "postDate";
        
        var dataString = {val1:startdate, val2:enddate, page:"pageHistory"};
        $.ajax({
            type:"post",
            url:"asset/view/modal_chargehistory.php",
            data:dataString,
            cache:false,
            success: function(html){
                document.getElementById('getHistory').innerHTML = html;  
//                alert(stardate);
            }
        });
        return false;
    }
</script>
<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
?>

<?php 
    function showAllStaff($type){
        if($type == 'all'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF'");
        }elseif($type == 'active'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_status = 'active' AND d_type = 'STAFF'");
        }elseif($type == 'deactive'){
            $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND (d_status = 'deactive' OR d_status = 'deleted')");
        }
        
        return mysql_num_rows($sql);
    }

    function findValue($gender, $marital, $type){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = '$type' AND d_gender = '$gender' AND d_martial = '$marital' AND d_status = 'active'");
        return mysql_num_rows($sql);
    }

    function findValueTotal($marital, $type){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = '$type' AND d_martial = '$marital' AND d_status = 'active'");
        return mysql_num_rows($sql);
    }

    function findSpouseChild($marital, $rel){
     
                  if($rel == 'SPOUSE'){
                    $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = ANY(SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND d_martial = '$marital' AND d_status = 'active') AND (d_relstatus = 'WIFE' OR d_relstatus = 'HUSBAND') AND d_status = 'active'");
                    $result = mysql_num_rows($sql);
                  }else{
                    $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = ANY(SELECT d_id FROM tbl_profile WHERE d_type = 'STAFF' AND d_martial = '$marital' AND d_status = 'active') AND (d_relstatus = 'CHILDREN') AND d_status = 'active'");
                    $result = mysql_num_rows($sql); 
                  }
        
        return $result;
                  
    }

    function benefitUsage($type){
        $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group != '85' AND d_status = 'active') AND d_type = '$type' AND (d_vdate BETWEEN '2019-01-01' AND '2019-06-30') AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        return $row['total'];
    }

    function usageCounter($type){
        $sql = mysql_query("SELECT DISTINCT d_staffID FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_group != '85' AND d_status = 'active') AND d_type = '$type' AND (d_vdate BETWEEN '2019-01-01' AND '2019-06-30') AND d_status = 'active'");
        $row = mysql_num_rows($sql);
        return $row;
    }
?>


<?php 
    if(isset($_POST['display'])){
        if($_POST['display'] == 'general'){
?>
    <div class="row col-lg-12" style="margin-bottom:40px;">
        <div class="col-4"><div class="header-l" > Total Staff</div><div class="cus-1" style="color:royalblue"><?php echo showAllStaff('all'); ?></div></div> 
        <div class="col-4"><div class="header-l" > Active</div><div class="cus-1" style="color:green"><?php echo showAllStaff('active'); ?></div></div> 
        <div class="col-4"><div class="header-l" > Deactive</div><div class="cus-1" style="color:red"><?php echo showAllStaff('deactive'); ?></div></div> 
    </div>
<?php
        }elseif($_POST['display'] == 'single'){
?>
        <div class="m-portlet m-portlet--mobile">
            <div class="title-l">Single</div>
            <div class="m-portlet__body ">
                <table>
                    <tr class="tr-1"><td class="td-1">Male</td>  <td class="td-2"><?php echo findValue('MALE', 'SINGLE', 'STAFF'); ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Female</td>  <td class="td-2"><?php echo findValue('FEMALE', 'SINGLE', 'STAFF') ?></td></tr>
                    <tr class=""><td class="td-1">Total</td>  <td class="td-2"><?php echo findValueTotal('SINGLE','STAFF') ?></td></tr>
                </table>
            </div>
        </div>
<?php
        }elseif($_POST['display'] == 'married'){
?>
        <div class="m-portlet m-portlet--mobile">
            <div class="title-l">Married</div>
            <div class="m-portlet__body ">
                <table>
                    <tr class="tr-1"><td class="td-1">Male</td>  <td class="td-2"><?php echo findValue('MALE', 'MARRIED', 'STAFF') ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Female</td>  <td class="td-2"><?php echo findValue('FEMALE', 'MARRIED', 'STAFF') ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Spouse</td>  <td class="td-2"><?php echo findSpouseChild('MARRIED', 'SPOUSE'); ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Children (under 18)</td>  <td class="td-2"><?php echo findSpouseChild('MARRIED', 'CHILDREN'); ?></td></tr>
                    <tr class=""><td class="td-1">Total</td>  <td class="td-2"><?php echo findValueTotal('MARRIED','STAFF') ?></td></tr> 
                </table>
            </div>
        </div>
<?php
        }elseif($_POST['display'] == 'widow'){
?>
        <div class="m-portlet m-portlet--mobile">
            <div class="title-l">Widow</div>
            <div class="m-portlet__body ">
                <table>
                    <tr class="tr-1"><td class="td-1">Male</td>  <td class="td-2"><?php echo findValue('MALE', 'DIVORCE', 'STAFF') ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Female</td>  <td class="td-2"><?php echo findValue('FEMALE', 'DIVORCE', 'STAFF') ?></td></tr>
                    <tr class="tr-1"><td class="td-1">Children (under 18)</td>  <td class="td-2"><?php echo findSpouseChild('DIVORCE', 'CHILDREN'); ?></td></tr>
                    <tr class=""><td class="td-1">Total</td>  <td class="td-2"><?php echo findValueTotal('DIVORCE','STAFF') ?></td></tr>
                </table>
            </div>
        </div>
<?php
        }
    }

    if(isset($_POST['custom'])){
        if($_POST['custom'] == 'custom'){
?>
    <div class="m-portlet m-portlet--mobile">
        <div class="title-l">Benefit Usage</div>
        <div class="m-portlet__body ">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--bordered m-portlet--unair">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Staff Medical History
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form>
                            <div class="form-group m-form__group row">
                                <label class="col-form-label col-lg-2 col-sm-12">
                                    Date range
                                </label>
                                <div class="col-lg-6 col-md-9 col-sm-12">
                                    <div class="input-daterange input-group" id="m_datepicker_5">
                                        <input type="text" class="form-control m-input" name="startdate" id="startdate" value="09/14/2019">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-ellipsis-h"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" name="endate" id="endate" value="09/14/2019">
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-12">
                                    <button type="button" class="btn btn-secondary" onclick="postDate()">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <table>
                <tr class="tr-1">
                    <td class="td-1">Inpatient / Staff</td>
                    <td class="td-2"><?php echo "RM ".number_format(benefitUsage('IN'),2)." / ".usageCounter('IN')." staff"; ?></td>
                </tr>
                <tr class="">
                    <td class="td-1">OutPatient / Staff</td>
                    <td class="td-2"><?php echo "RM ".number_format(benefitUsage('OUT'),2)." / ".usageCounter('OUT')." staff"; ?></td>
                </tr>
            </table>
        </div>
    </div>      
<?php
        }
    }
?>



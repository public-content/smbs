<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
?>

<?php 
    if(isset($_POST['function'])){
        $func = $_POST['function'];
        $did = $_POST['did'];
        $laboid = $general->allTable($did,'d_id','tbl_profile','d_labor');
        $logid = $general->allTable($did,'d_userid','tbl_user','d_id');
        $d_id = $general->securestring('encrypt',$did);
        $page = 'system.php?p='.$general->securestring('encrypt','104');
        $success = "Update complete";
        $error = mysql_error();
					  
        
        if($func == 'edit'){
            echo $page."&r=".$d_id;
        }elseif($func == 'activate'){
            $sql = mysql_query("UPDATE tbl_profile SET d_status = 'active', d_modified = NOW() WHERE d_id = '$did'");
            if(!$sql){
                echo $error;
            }else{
                $sql2 = mysql_query("UPDATE tbl_user SET d_status = 'active', d_modified = NOW() WHERE d_id = '$logid'");
                if(!$sql2){
                    echo $error;
                }else{
                    $sql3 = mysql_query("UPDATE tbl_labor SET d_status = 'active', d_modified = NOW(), d_resignation = '' WHERE d_id = '$laboid'");
                    if(!$sql3){
                        echo $error;
                    }else{
                        echo $success;
                    }
                }
            }
        }elseif($func == 'deactivate'){
            $sql = mysql_query("UPDATE tbl_profile SET d_status = 'deactive', d_modified = NOW() WHERE d_id = '$did'");
            if(!$sql){
                echo $error;
            }else{
                $sql2 = mysql_query("UPDATE tbl_user SET d_status = 'deactive', d_modified = NOW() WHERE d_id = '$logid'");
                if(!$sql2){
                    echo $error;
                }else{
                    $sql3 = mysql_query("UPDATE tbl_labor SET d_status = 'deactive', d_modified = NOW(), d_resignation = NOW() WHERE d_id = '$laboid'");
                    if(!$sql3){
                        echo $error;
                    }else{
                        echo $success;
                    }
                }
            }
        }elseif($func == 'delete'){
            $sql = mysql_query("UPDATE tbl_profile SET d_status = 'delete', d_modified = NOW() WHERE d_id = '$did'");
            if(!$sql){
                echo $error;
            }else{
                $sql2 = mysql_query("UPDATE tbl_user SET d_status = 'delete', d_modified = NOW() WHERE d_id = '$logid'");
                if(!$sql2){
                    echo $error;
                }else{
                    $sql3 = mysql_query("UPDATE tbl_labor SET d_status = 'delete', d_modified = NOW() WHERE d_id = '$laboid'");
                    if(!$sql3){
                        echo $error;
                    }else{
                        echo $success;
                    }
                }
            }
        }
    }
?>
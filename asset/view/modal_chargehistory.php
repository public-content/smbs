<?php 
    //head
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();

     function getMrn($col, $mrn){
        $sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_mrn = '$mrn'");
        $row = mysql_fetch_assoc($sql);
        return $row[$col];
    }

    function getStaffID($did){
		$sql = mysql_query("SELECT d_staffID FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_staffID'];
	}
?>


<?php
    //action function
    if(isset($_POST['action'])){
        $action = mysql_real_escape_string(trim($_POST['action']));
        if($action == 'delThis'){
            $getDid = mysql_real_escape_string(trim($_POST['val1']));
            $sql = mysql_query("UPDATE tbl_treatment SET d_status = 'deleted', d_modified = NOW() WHERE d_id = '$getDid'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Ohh No!
                            </strong>
                            ".mysql_error()."
                        </div>";
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Well done
                            </strong>
                            Treatment has been deleted
                        </div>";
            }
        }
    }
?>


<?php 
    //page display
    if(isset($_POST['page'])){
        $page = mysql_real_escape_string(trim($_POST['page']));
        if($page == 'pageHistory'){
            $startDate = date('Y-m-d', strtotime($_POST['val1']));
            $endDate = date('Y-m-d', strtotime($_POST['val2']));
?>
<div class="m-portlet m-portlet--bordered m-portlet--unair">
    <div class="m-portlet__body">
        <div class="col-lg-12" style="" id="notification"></div>
        <div class="col-lg-12" style="border-bottom:1px lightgrey solid; margin:20px 0px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                Out-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Dept
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bil=1;
											$year='2019'; $listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'OUT' AND d_status = 'active' AND d_vdate BETWEEN '$startDate' AND '$endDate' "); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
												?>
                    <tr>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                        <td>
                            <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                        </td>
                    </tr>
                    <?php $bil++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12" style="height:40px;"></div>
        <div class="col-md-12 col-lg-12 col-xl-12">
            <!--begin::Total Out-patient-->

            <h4 class="m-widget24__title">
                In-Patient Benefits
            </h4>

            <!--end::Out-patient-->
        </div>
        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
            <table class="table m-table m-table--head-separator-metal">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            MRN
                        </th>
                        <th>
                            Episode
                        </th>
                        <th>
                            Department
                        </th>
                        <th>
                            Days
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            Bill No
                        </th>
                        <th>
                            Remarks
                        </th>
                        <th>
                            Doctor
                        </th>
                        <th>
                            Date
                        </th>
                    </tr>
                </thead>
                <tbody style="background:#d3d3d34d;">
                    <?php 
											$bill=1;
											$listOut = mysql_query("SELECT * FROM tbl_treatment WHERE d_type = 'IN' AND d_status = 'active' AND d_vdate BETWEEN '$startDate' AND '$endDate'"); 
											if(mysql_num_rows($listOut)){
												while($rowOut=mysql_fetch_assoc($listOut)){
								        ?>
                    <tr>
                        <th scope="row">
                            <?php echo $bil; ?>
                        </th>
                        <td>
                            <?php echo getMrn('d_name',$rowOut['d_mrn']); ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_mrn']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_episode']; ?>
                        </td>
                        <td>
                            <?php echo $general->masterTake('tbl_setting', 'd_value', $general->securestring('encrypt', $general->masterTake('tbl_labor', 'd_department', $_SESSION['labid']))); ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_day']; ?>
                        </td>
                        <td>
                            RM <?php echo $rowOut['d_charge']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_bill']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_treatment']; if($rowOut['d_treatmentlist'] != ''){ echo ' - '.$rowOut['d_treatmentlist']; }  ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_doc']; ?>
                        </td>
                        <td>
                            <?php echo $rowOut['d_created']; ?>
                        </td>
                        <td>
                            <a title="Delete"><i class="la la-trash" style="color:red;cursor:pointer" onclick="delThis(<?php echo $rowOut['d_id']; ?>)"></i></a>
                        </td>
                    </tr>
                    <?php $bill++; }}else{ ?>
                    <tr>
                        <td colspan="12" style="text-align:center;background:#FEFEE;">
                            No Data
                        </td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
        }
    }
?>
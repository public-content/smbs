<?php
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$cont = new $pageModel();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','200');
	$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }

    .cusdevider {
        border-bottom: 0.5px solid #ebedf2;
        margin: 28.6px 0px 0px 0px;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="row">
                <div id="alert-1" class="col-lg-12"></div>
                <div class="col-lg-12">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Fast search staff benefits records
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="col-lg-12">
                                <div class="form-group m-form__group row">
                                    <label for="example-search-input" class="col-2 col-form-label">
                                        Search by:
                                    </label>
                                    <div class="col-3">
                                        <select class="form-control m-input m-input--solid" id="searchtype">
                                            <option value="std">
                                                Staff ID
                                            </option>
                                            <option value="stc">
                                                Staff IC
                                            </option>
                                            <option value="stm">
                                                MRN
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-3">
                                        <input class="form-control m-input m-input--solid" type="text" value="" id="thevalue">
                                    </div>
                                    <div class="col-1">
                                        <button type="button" class="btn btn-secondary m-btn m-btn--icon" onclick="postdid()">
                                            <span>
                                                <i class="la la-search"></i>
                                                <span>
                                                    Search
                                                </span>
                                            </span>
                                        </button>
                                    </div>
                                    <div class="col-1" style="text-align:center;">
                                        <button class="btn btn-metal m-btn m-btn--icon" type="reset" onclick="postzero()">
                                            <span>
                                                <i class="la la-retweet"></i>
                                                <span>
                                                    Reset
                                                </span>
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="displayProfile">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function postdid() {
        document.getElementById('displayProfile').innerHTML = "<div style='width: 300px;margin: auto;padding-top: 80px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var searchtype = document.getElementById('searchtype').value;
        var thevalue = document.getElementById('thevalue').value;
        var page = "page_profile";

        var dataString = {
            page: page,
            searchtype: searchtype,
            thevalue: thevalue
        };
        //        console.log(dataString);
        $.ajax({
            type: "post",
            url: "asset/view/modal_dashboard.php",
            data: dataString,
            cache: false,
            success: function(html) {
                document.getElementById('displayProfile').innerHTML = html;
                //                       alert(searchtype+thevalue); 
            }
        });
        return false;
    }
    
    function postzero(){
        document.getElementById('displayProfile').innerHTML = "";
    }
    
    function editUser(did) {
        //        var page = "<?php echo $cont->securestring('encrypt','104') ?>";
        //        var url = "system.php?p="+page;
        //        location.replace(url + '&r=' + did);
        var dataString = {
            function: "edit",
            did: did
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_listofstaff.php",
            data: dataString,
            cache: false,
            success: function(html) {
                var dir = html;
                location.replace(dir);
            }
        });
        return false;
    }
    
    function delThis(xval, xtype, xvalue) {
        var searchtype = xtype;
        var thevalue = xvalue;
        var page = "page_profile";
        var msg = "popup";
        if (confirm("Are you sure!")) {
            var dataString = {
                msg: msg,
                did: xval,
                page: page,
                searchtype: searchtype,
                thevalue: thevalue
            };
            $.ajax({
                type: "post",
                url: "asset/view/modal_dashboard.php",
                data: dataString,
                cache: false,
                success: function(html) {
                    document.getElementById('displayProfile').innerHTML = html;
                }
            });
            return false;
        } else {

        }
    }
</script>
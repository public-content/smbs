<?php
$pageModel = $general->pageInfo($page,'pageDir');
$pageTitle = $general->pageInfo($page,'pageTitle');
$pageName = $general->pageInfo($page,'pageName');
$main->includePHP('model',$pageModel);
$cont = new $pageModel();
$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
$pageDir = 'system.php?p='.$_GET['p'];
?>
<?php
    if($_GET['r'] != ''){
        $refer = $cont->securestring('decrypt',$_GET['r']);
        $labid = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_labor');
        $logid = $cont->allTable($refer, 'd_userid', 'tbl_user', 'd_id');
        $d_name = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_name');
        $title = "<span style='color:#50a9ff;'><i class='fa fa-user-circle'></i> ".$d_name."</span>";
        
        $sName  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_name');
        $sIC  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_ic');
        $sGender  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_gender');
        $sMS  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_martial');
        $sDA  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_available');
        $sMrn  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_mrn');
        $sPN  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_pnum');
        $sEmail  = $cont->allTable($refer, 'd_id', 'tbl_profile', 'd_email');
        
        $sID  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_staffID');
        $sDepartment  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_department');
        $arrDepart = explode(" , ",$sDepartment);
        $sDesign  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_designation');
        $sED  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_employment');
        $sCat  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_category');
        $sGrp  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_group');
        $sWS  = $cont->allTable($labid, 'd_id', 'tbl_labor', 'd_wStatus');
        
        $sUname  = $cont->allTable($logid, 'd_id', 'tbl_user', 'd_username');
        $sUrole  = $cont->allTable($logid, 'd_id', 'tbl_user', 'd_role');
    }else{
        $refer = "";$labid = "";$logid = "";$d_name = "";$title = "";$sName = "";$sIC = "";$sGender = "";$sMS = "";$sDA = "";$sMrn = "";$sPN = "";$sEmail = "";$sID = "";$sDepartment = "";$sDesign = "";$sED = "";$sCat = "";$sGrp = "";$sWS = "";$sUname = "";$sUrole = ""; $arrDepart = "";
    }
?>
<?php 
    if(isset($_POST['btn-register'])){
        $task = $cont->addUser($_POST);
        if($task == 1){
            $msger = "<div class='alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'></button><strong>Well done!</strong>New User Successfully Registered.</div>";
        }else{
            $msger = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'></button><strong>Ohh No</strong>".$task."</div>";
        }
    }elseif(isset($_POST['btn-editUser'])){
        $task = $cont->editUser($_POST);
        if($task == 1){
            $msger = "<div class='alert alert-success alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'></button><strong>Well done!</strong>Successfully Update</div>";
        }else{
            $msger = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'></button><strong>Ohh No</strong>".$task."</div>";
        }
    }
    
$alert = $msger;
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?php
                                    echo $pageTitle." ".$title;
                                ?>
                            </h3>
                        </div>
                    </div>
                </div>
                <?php if($alert != ''){ ?>
                    <div class="col-lg-12" style="margin-top:10px;"><?php echo $alert; ?></div>
                <?php }?>
                <form class="m-form m-form--fit m-form--label-align-right" method="post">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="sName" class="col-lg-2 col-sm-2 col-form-label">
                                Name: <span class="red"> *</span>
                            </label>
                            <div class="col-lg-4 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sName; ?>" id="sName" name="sName" required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sIC" class="col-lg-2 col-sm-2 col-form-label">
                                I/C Number: <span class="red"> *</span>
                            </label>
                            <div class="col-lg-3 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sIC; ?>" id="sIC" name="sIC" onkeyup="pushUsername()" required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sID" class="col-lg-2 col-sm-2 col-form-label">
                                Staff ID:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sID; ?>" id="sID" name="sID">
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sDepartment" class="col-lg-2 col-sm-2 col-form-label">
                                Department: 
                            </label>
                            <div class="col-lg-4 col-sm-10">
                                <select class="form-control m-input m-input--solid" id="sDepartment" name="sDepartment[]" multiple>
                                    <?php $list = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'department' AND d_status = 'active'"); if(mysql_num_rows($list)){ while($row = mysql_fetch_assoc($list)){ ?>
                                    <option value="<?php echo $row['d_id']; ?>" <?php if($refer != ''){ if(in_array($row['d_id'], $arrDepart)){ print 'selected';} } ?> >
                                        <?php echo $row['d_value']; ?>
                                    </option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <span class="m-form__help">
                                Hold CTRL while choosing more than one department
                            </span>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sDesign" class="col-lg-2 col-sm-2 col-form-label">
                                Designation :<span class="red"> *</span>
                            </label>
                            <div class="col-lg-3 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sDesign; ?>" id="sDesign" name="sDesign" required>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sED" class="col-lg-2 col-sm-2 col-form-label">
                                Employment Date:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="date" value="<?php echo $sED; ?>" id="sED" name="sED">
                            </div>
                        </div>
                        <?php if($refer != ''){ ?>
                        <div class="form-group m-form__group row">
                            <label for="sRD" class="col-lg-2 col-sm-2 col-form-label">
                                Resign Date:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="date" value="" id="sRD" name="sRD">
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group m-form__group row">
                            <label for="sGender" class="col-lg-2 col-sm-2 col-form-label">
                                Gender:<span class="red"> *</span>
                            </label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="sGender" value="MALE" required <?php if($sGender == "MALE"){ print 'checked';} ?>>
                                    Male
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="sGender" value="FEMALE" required <?php if($sGender == "FEMALE"){ print 'checked';} ?>>
                                    Female
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sMS" class="col-lg-2 col-sm-2 col-form-label">
                                Marital Status:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <select class="form-control m-input m-input--solid" id="sMS" name="sMS" >
                                    <option value="">
                                        select
                                    </option>
                                    <option value="SINGLE" <?php if($sMS == "SINGLE"){ print 'selected';} ?>>
                                        Single
                                    </option>
                                    <option value="MARRIED" <?php if($sMS == "MARRIED"){ print 'selected';} ?>>
                                        Married
                                    </option>
                                    <option value="DIVORCE" <?php if($sMS == "DIVORCE"){ print 'selected';} ?>>
                                        Divorce
                                    </option>
                                    <option value="WIDOW" <?php if($sMS == "WIDOW"){ print 'selected';} ?>>
                                        Widow/Widower
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sDA" class="col-lg-2 col-sm-2 col-form-label">
                                Dependents Available:<span class="red"> *</span>
                            </label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="sDA" value="NO"  <?php if($sDA == "NO"){ print 'checked';} ?>>
                                    No
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="sDA" value="YES"  <?php if($sDA == "YES"){ print 'checked';} ?>>
                                    Yes
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sCat" class="col-lg-2 col-sm-2 col-form-label">
                                Category:<span class="red"> *</span>
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <select class="form-control m-input m-input--solid" id="sCat" name="sCat" required>
                                    <?php $list = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'categorylevel' AND d_status = 'active'"); if(mysql_num_rows($list)){ while($row = mysql_fetch_assoc($list)){ ?>
                                    <option value="<?php echo $row['d_id']; ?>" <?php if($sCat == $row['d_id']){ print 'selected';} ?>>
                                        <?php echo $row['d_value']; ?>
                                    </option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sGrp" class="col-lg-2 col-sm-2 col-form-label">
                                Group:<span class="red"> *</span>
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <select class="form-control m-input m-input--solid" id="sGrp" name="sGrp" required>
                                    <?php $list = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'staffgroup' AND d_status = 'active'"); if(mysql_num_rows($list)){ while($row = mysql_fetch_assoc($list)){ ?>
                                    <option value="<?php echo $row['d_id']; ?>" <?php if($sGrp == $row['d_id']){ print 'selected';} ?>>
                                        <?php echo $row['d_value']; ?>
                                    </option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sWS" class="col-lg-2 col-sm-2 col-form-label">
                                Status:<span class="red"> *</span>
                            </label>
                            <div class="m-radio-inline">
                                <label class="m-radio">
                                    <input type="radio" name="sWS" value="CONFIRMED" required <?php if($sWS == "CONFIRMED"){ print 'checked';} ?>>
                                    Confirmed
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="sWS" value="NOTCONFIRMED" required <?php if($sWS == "NOTCONFIRMED"){ print 'checked';} ?>>
                                    Not Confirmed
                                    <span></span>
                                </label>
                                <label class="m-radio">
                                    <input type="radio" name="sWS" value="CONTRACT" required <?php if($sWS == "CONTRACT"){ print 'checked';} ?>>
                                    Contract
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sMrn" class="col-lg-2 col-sm-2 col-form-label">
                                MRN:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sMrn; ?>" id="sMrn" name="sMrn" >
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sPN" class="col-lg-2 col-sm-2 col-form-label">
                                Phone Number:
                            </label>
                            <div class="col-lg-2 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sPN; ?>" id="sPN" name="sPN" >
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="sEmail" class="col-lg-2 col-sm-2 col-form-label">
                                Email:
                            </label>
                            <div class="col-lg-3 col-sm-10">
                                <input class="form-control m-input m-input--solid" type="email" value="<?php echo $sEmail; ?>" id="sEmail" name="sEmail" >
                            </div>
                        </div>
                        <div class="m-form__seperator m-form__seperator--dashed"></div>
                        <div class="m-form__section m-form__section--last">
                            <div class="m-form__heading">
                                <h3 class="m-form__heading-title">
                                    Login Info:
                                </h3>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="sUname" class="col-lg-2 col-sm-2 col-form-label">
                                    Username :<span class="red"> *</span>
                                </label>
                                <div class="col-lg-3 col-sm-10">
                                    <input class="form-control m-input m-input--solid" type="text" value="<?php echo $sUname; ?>" id="sUname" name="sUname" required>
                                </div>
                                <span class="m-form__help">
                                    BY DEFAULT SAME WITH I/C NUMBER
                                </span>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="sUpass" class="col-lg-2 col-sm-2 col-form-label">
                                    Password :<span class="red"> *</span>
                                </label>
                                <div class="col-lg-3 col-sm-10">
                                    <input class="form-control m-input m-input--solid" type="password" value="" id="sUpass" name="sUpass"  <?php if($refer != ''){ echo ""; }else{ echo "required"; } ?>>
                                </div>
                                <span class="m-form__help">
                                    <?php if($refer != ''){ echo ""; }else{ echo "BY DEFAULT 1234"; } ?>
                                </span>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="sUrole" class="col-lg-2 col-sm-2 col-form-label">
                                    Role:<span class="red"> *</span>
                                </label>
                                <div class="col-lg-2 col-sm-10">
                                    <select class="form-control m-input m-input--solid" id="sUrole" name="sUrole" required>
                                        <option value="394">
                                            Staff
                                        </option>
                                        <option value="397">
                                            Doctor
                                        </option>
                                        <?php
                                        $secrole = $cont->securestring('decrypt',$_SESSION['roles']);
                                        if($secrole == '391'){
                                        $list = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'rolecategory' AND d_status = 'active' AND d_value2 = ''"); if(mysql_num_rows($list)){ while($row = mysql_fetch_assoc($list)){  ?>
                                        <option value="<?php echo $row['d_id']; ?>" <?php if($sUrole == $row['d_id']){ print 'selected';} ?>>
                                            <?php echo $row['d_value']; ?>
                                        </option>
                                        <?php } } ?>
                                        <option value="391" <?php if($sUrole == "391"){ print 'selected';} ?>>
                                            Administrator
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <?php
                            if($refer != ""){
                             echo "<input type='hidden' value='$refer' name='proid'>
                             <input type='hidden' value='$labid' name='labid'>
                             <input type='hidden' value='$logid' name='logid'>
                            <button type='submit' class='btn btn-info' name='btn-editUser'>
                                Edit
                            </button>";
                            }else{
                                echo "<button type='submit' class='btn btn-primary' name='btn-register'>
                                Register
                            </button>";
                            }
                            ?>
                            <button type="reset" class="btn btn-secondary">
                                Cancel
                            </button>
                        </div>
                    </div>
                </form>
                <?php 
//                    $aaa = array(33); //dari form
//                    $bbb = implode(' , ',$aaa); // masuk ke database
//                    $bbbb = explode(",",$bbb); // convert bali array nk count utk if else
//                    $count = count($bbbb); // count array
//
//                    if(count != 1){ // kalau lebih dari satu
//                        $ccc =  "d_department = ".str_replace(",","OR d_department = "," $bbb "); // keluarkan dan replace
//                        $ddd =  "SELECT * FROM tbl_labor WHERE ".$ccc; // buat query
//                        echo $ddd."<br>";
//                        $sql = mysql_query($ddd);
//                        if(mysql_num_rows($sql)){
//                            while($row = mysql_fetch_assoc($sql)){
//                                echo $row['d_id']."<br>";
//                            }
//                        }   
//                    }else{ // kalau satu
//                        $ddd =  "SELECT * FROM tbl_labor WHERE d_department = ".$bbb; // buat query
//                        echo $ddd."<br>";
//                        $sql = mysql_query($ddd);
//                        if(mysql_num_rows($sql)){
//                            while($row = mysql_fetch_assoc($sql)){
//                                echo $row['d_id']."<br>";
//                            }
//                        }
//                    }
//                    
//                
//                print_r($bbbb);echo "<br>";
//                    echo "bbb = " . $bbb. "<br>";
//                    echo "count = " . $count. "<br>";
//                    
                ?>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function pushUsername(){
        var val = document.getElementById("sIC").value;
        document.getElementById("sUname").value = val; 
    }
</script>
<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
?>

<?php 
    if(isset($_POST['act'])){
            $did = mysql_real_escape_string(trim($_POST['did']));
            $d_value = mysql_real_escape_string(trim($_POST['d_value']));
            $d_value2 = mysql_real_escape_string(trim($_POST['d_value2']));
            $d_value3 = mysql_real_escape_string(trim($_POST['d_value3']));
            
            $sql = mysql_query("UPDATE tbl_setting SET d_value = '$d_value', d_value2 = '$d_value2', d_value3 = '$d_value3', d_modified = NOW() WHERE d_id = '$did'");
            if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Ohh No!</strong>
						  ".mysql_error()."
					  </div>";
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Well done!</strong>
						  Update complete.
					  </div>";
            }
            
    }elseif(isset($_POST['add'])){
        $type = $_POST['add'];
        $d_value = mysql_real_escape_string(trim($_POST['d_value']));
        $d_value2 = mysql_real_escape_string(trim($_POST['d_value2']));
        $d_value3 = mysql_real_escape_string(trim($_POST['d_value3']));
        
        $sql = mysql_query("INSERT INTO tbl_setting(d_type,d_value,d_value2,d_value3,d_created,d_modified,d_status)VALUES('$type','$d_value','$d_value2','$d_value3',NOW(),NOW(),'active')");
        
        if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Ohh No!</strong>
				        ".mysql_error()."
					  </div>";
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Well done!</strong>
						  Successfully added.
					  </div>";
            }
    }elseif(isset($_POST['delete'])){
        $did = $_POST['value'];
        $sql = mysql_query("UPDATE tbl_setting SET  d_status = 'deleted', d_modified = NOW() WHERE d_id = '$did'");
        
        if(!$sql){
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Ohh No!</strong>
						  ".mysql_error()."
					  </div>";
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
						      <strong>Well done!</strong>
						  Successfully delete.
					  </div>";
            }
    }
?>



<?php
    if(isset($_POST['setType'])){
        $type = $_POST['setType'];
        
        if($type == 'setcategory'){
?>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Category Name
            </th>
            <th>
                Out-Patient
            </th>
            <th>
                In-Patient
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'categorylevel' AND d_status = 'active'");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <?php echo $row['d_value2']; ?>
            </td>
            <td>
                <?php echo $row['d_value3']; ?>
            </td>
            <td>
                <a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit" data-toggle="modal" data-target="#editcategory<?php echo $row['d_id']; ?>"><i class="la la-edit"></i></a>
                <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','setcategory')"><i class="la la-trash"></i></a>
            </td>
            <div class="modal fade" id="editcategory<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Category Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Category Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="d_value2-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Outpatient Value (RM):
                                    </label>
                                    <input type="text" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value2']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="d_value3-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Inpatient Value (RM):
                                    </label>
                                    <input type="text" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value3']; ?>">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','setcategory')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php
        }elseif($type == 'setdepartment'){
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Deartment Name
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'department' AND d_status = 'active'");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit" data-toggle="modal" data-target="#editdepartment<?php echo $row['d_id']; ?>"><i class="la la-edit"></i></a>
                <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','setdepartment')"><i class="la la-trash"></i></a>
            </td>
            <div class="modal fade" id="editdepartment<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Department Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Department Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                </div>
                                    <input type="hidden" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="">
                                    <input type="hidden" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','setdepartment')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php
        }elseif($type == 'setgroup'){
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Group Name
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'staffgroup' AND d_status = 'active'");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit" data-toggle="modal" data-target="#editgroup<?php echo $row['d_id']; ?>"><i class="la la-edit"></i></a>
                <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','setgroup')"><i class="la la-trash"></i></a>
            </td>
            <div class="modal fade" id="editgroup<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Group Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Department Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                </div>
                                    <input type="hidden" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="">
                                    <input type="hidden" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','setgroup')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php          
        }elseif($type == 'setdoctor'){
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Doctor Name
            </th>
            <th>
                Short Name
            </th>
            <th>
                Doctor Code
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'doctor' AND d_status = 'active'");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <?php echo $row['d_value2']; ?>
            </td>
            <td>
                <?php echo $row['d_value3']; ?>
            </td>
            <td>
                <a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit"><i class="la la-edit" data-toggle="modal" data-target="#editdoctor<?php echo $row['d_id']; ?>"></i></a>
                <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','setdoctor')"><i class="la la-trash"></i></a>
            </td>
            <div class="modal fade" id="editdoctor<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Doctor Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Doctor Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="d_value2-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Short Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value2']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="d_value3-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Doctor Code:
                                    </label>
                                    <input type="text" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value3']; ?>">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','setdoctor')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php           
        }elseif($type == 'settreattype'){
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Treatment Type
            </th>
            <th>
                Category
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'treatment' AND d_status = 'active'");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <?php echo $row['d_value2']; ?>
            </td>
            <td>
                <button href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit" data-toggle="modal" data-target="#edittype<?php echo $row['d_id']; ?>"><i class="la la-edit"></i></button>
                <button href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','settreattype')"><i class="la la-trash"></i></button>
            </td>
            <div class="modal fade" id="edittype<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Treatment Type Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        New Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                    <input type="hidden" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="">
                                    <input type="hidden" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="">
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','settreattype')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php
        }elseif($type == 'setlist'){
?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Treatment Name
            </th>
            <th>
                Treatment Category
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $bil = 1;
            $sql = mysql_query("SELECT * FROM tbl_setting WHERE d_type = 'treatmentlist' AND d_status = 'active' ORDER BY d_value2 DESC");
            if(mysql_num_rows($sql)){
                while( $row = mysql_fetch_assoc($sql)){
        ?>
        <tr>
            <th scope="row">
                <?php echo $bil; ?>
            </th>
            <td>
                <?php echo $row['d_value']; ?>
            </td>
            <td>
                <?php echo $row['d_value2']; ?>
            </td>
            <td>
                <a href="#" class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="edit"><i class="la la-edit" data-toggle="modal" data-target="#editlist<?php echo $row['d_id']; ?>"></i></a>
                <a href="#" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" title="delete" onclick="delThis('<?php echo $row['d_id'] ?>','setlist')"><i class="la la-trash"></i></a>
            </td>
            <div class="modal fade" id="editlist<?php echo $row['d_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Treatment List Setting
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    &times;
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="d_value-<?php echo $row['d_id'];?>" class="form-control-label">
                                        Treatment List Name:
                                    </label>
                                    <input type="text" class="form-control" id="d_value-<?php echo $row['d_id'];?>" value="<?php echo $row['d_value']; ?>">
                                </div>
                                    <input type="hidden" class="form-control" id="d_value2-<?php echo $row['d_id'];?>" value="">
                                    <input type="hidden" class="form-control" id="d_value3-<?php echo $row['d_id'];?>" value="">
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="actProcess('<?php echo $row['d_id']; ?>','setlist')">
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </tr>
        <?php
                    $bil++;
                }
            }
        ?>
    </tbody>
</table>
<?php
        }
    }
?>
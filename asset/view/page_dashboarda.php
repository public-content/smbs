<?php
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$cont = new $pageModel();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','101');
	$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
	.td-a{
    width: 20px;
    padding-left: 0rem;
    padding-right:0rem;
	}
	
	.red{
    color: red;
	}
</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-subheader ">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
						<li class="m-nav__separator">
							-
						</li>
						<li class="m-nav__item">
							<a href="<?php echo $pageDir ?>" class="m-nav__link">
								<span class="m-nav__link-text">
									<?php echo $pageName ?>
								</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="m-content">
			<div class="m-portlet ">
				<div class="m-portlet__body  m-portlet__body--no-padding">
					<div class="row m-row--no-padding m-row--col-separator-xl">
						<div class="col-md-12 col-lg-6 col-xl-4">
							<!--begin::Total Out-patient-->
							<div class="m-widget24">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										Out-Patient
									</h4>
									<br>
									<?php 
                                        $total = number_format(array_sum($cont->totalOutpatient()),2);
                                        $usage = number_format($cont->usage('OUT'),2);
                                        $percentage = $cont->getPercentage($total, $usage);
                                        $number = $cont->totalStaff();
                                    ?>
									<span class="m-widget24__desc">
										Total for all <b> <?php echo $number; ?> Staff</b>
									</span>
									<span class="m-widget24__stats m--font-brand">
										<?php echo 'RM '.$total; ?>
									</span>
									<div class="m--space-10"></div>
									<div class="progress m-progress--sm">
										<div class="progress-bar m--bg-brand" role="progressbar" style="width: <?php echo $percentage; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									<span class="m-widget24__change">
										Benefits Used <b>RM <?php  echo $usage; ?></b>
									</span>
									<span class="m-widget24__number">
										<?php echo $percentage; ?>
									</span>
								</div>
							</div>
							<!--end::Out-patient-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-4">
							<!--begin::Total In-Patient(benefits)-->
							<?php 
//                                $totalin = number_format(array_sum($cont->totalInpatient('benefit')),2);
                               
                                $staffinB = $cont->totalStaffIP('benefit');                                
                                $staffinD = $cont->totalStaffIP('day');
                                $totalinB = number_format($cont->totalInpatient('benefit'),2);
                                $totalinBB = $cont->totalInpatient('benefit');
                                $totalinD = array_sum($cont->totalInpatient('day'));
                                $balinB = number_format(array_sum($cont->balanceIP('benefit')),2);
                                $balinBB = array_sum($cont->balanceIP('benefit'));
                                $balinD = array_sum($cont->balanceIP('day'));
                                $percentageB = $cont->getPercentage($totalinBB, $balinBB);
                                $percentageD = $cont->getPercentage($totalinD, $balinD);
                                
                            ?>
							<div class="m-widget24">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										In-Patient (Benefit)
									</h4>
									<br>
									<span class="m-widget24__desc">
										Total for all <b><?php echo $staffinB; ?> Staff</b>
									</span>
									<span class="m-widget24__stats m--font-danger">
										RM <?php echo $totalinB; ?>
									</span>
									<div class="m--space-10"></div>
									<div class="progress m-progress--sm">
										<div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $percentageB; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									<span class="m-widget24__change">
										Benefits Used <b>RM <?php echo $balinB; ?></b>
									</span>
									<span class="m-widget24__number">
										<?php echo $percentageB; ?>
									</span>
								</div>
							</div>
							<!--end::Total In-Patient(benefits)-->
						</div>
						<div class="col-md-12 col-lg-6 col-xl-4">
							<!--begin::Total In-Patient(days)-->
							<div class="m-widget24">
								<div class="m-widget24__item">
									<h4 class="m-widget24__title">
										In-Patient (Days)
									</h4>
									<br>
									<span class="m-widget24__desc">
										Total for all <b><?php echo $staffinD; ?> Staff</b>
									</span>
									<span class="m-widget24__stats m--font-danger">
										<?php  echo $totalinD; ?> Days
									</span>
									<div class="m--space-10"></div>
									<div class="progress m-progress--sm">
										<div class="progress-bar m--bg-danger" role="progressbar" style="width: <?php echo $percentageD; ?>;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									<span class="m-widget24__change">
										Days Used <b><?php echo $balinD; ?> Days</b>
									</span>
									<span class="m-widget24__number">
										<?php echo $percentageD; ?>
									</span>
								</div>
							</div>
							<!--end::Total In-Patient(days)-->
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="m-portlet m-portlet--mobile">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text">
										Fast search staff benefits records
									</h3>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="col-lg-12">
								<form class="" action="" method="post">
									<div class="form-group m-form__group row">
										<label for="example-search-input" class="col-2 col-form-label">
											Search by:
										</label>
										<div class="col-3">
											<!-- <input class="form-control m-input" type="search" value="How do I shoot web" id="example-search-input"> -->
											<select class="form-control m-input m-input--solid" id="">
												<option value="">
													Staff ID
												</option>
												<option value="">
													Staff IC
												</option>
												<option value="">
													MRN
												</option>
											</select>
										</div>
										<div class="col-3">
											<input class="form-control m-input m-input--solid" type="search" value="" id="">
										</div>
										<div class="col-1">
											<a href="#" class="btn btn-secondary m-btn m-btn--icon">
												<span>
													<i class="la la-search"></i>
													<span>
														Search
													</span>
												</span>
											</a>
										</div>
										<div class="col-1" style="text-align:center;">
											<a href="#" class="btn btn-metal m-btn m-btn--icon">
												<span>
													<i class="la la-retweet"></i>
													<span>
														Reset
													</span>
												</span>
											</a>
										</div>
									</div>
								</form>
							</div>
							 <div class="row">
							    <div class="col-lg-6" style="margin-top:40px">
							        <table class="table table-hover">
							            <tbody>
							                <tr>
							                    <th scope="row">
							                        Name
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        Viknakaran A/l Gnanasegar <a href="#" title="Edit User Profile" data-toggle="modal" data-target="#editUser"><i class="la la-edit" style="color:royalblue;"></i></a>
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Staff ID
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        2928
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Staff IC
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        820315-14-5399
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Gender
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        Male
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        MRN
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        293986
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Date Joined
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        02-10-2017
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Department
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        Information Technology
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Designation
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        It Services
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Category
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        A5
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        Family Members
							                    </th>
							                    <td class="td-a">
							                        :
							                    </td>
							                    <td>
							                        There is no records of your family members.<br>
							                        If your family member name not appear:-<br>
							                        1. It was not entered in KAIZEN ESS<br>
							                        2. It has not been update to SMBS by HR yet<br>
							                        3. They are not covered by the hospital
							                    </td>
							                </tr>
							                <tr>
							                    <th scope="row">
							                        
							                    </th>
							                    <td class="td-a">
							                        
							                    </td>
							                    <td style="text-align:right;">
							                        <a href="#" class="btn btn-primary m-btn m-btn--icon" data-toggle="modal" data-target="#addCharges">
							                            <span>
							                                <i class="la la-plus-square-o"></i>
							                                <span>
							                                    Add Charge to Staff
							                                </span>
							                            </span>
							                        </a>
							                    </td>
							                </tr>
							            </tbody>
							        </table>
							    </div>
							    <div class="col-lg-6" style="margin-top:40px">
							        <div class="col-md-12 col-lg-12 col-xl-12">
							            begin::Total Out-patient
							            <div class="m-widget24" style="background:#c4e1f3b3;">
							                <div class="m-widget24__item">
							                    <h4 class="m-widget24__title">
							                        Out-Patient
							                    </h4>
							                    <br>
							                    <span class="m-widget24__desc">
							                        Yearly Benefits
							                    </span>
							                    <span class="m-widget24__stats m--font-primary">
							                        RM 400.00
							                    </span>
							                    <div class="m--space-10"></div>
							                    <div class="progress m-progress--sm">
							                        <div class="progress-bar m--bg-primary" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							                    </div>
							                    <span class="m-widget24__change">
							                        Current Usage <b>RM 0.00</b><br>
							                        Balance <b>RM 400.00</b>
							                    </span>
							                    <span class="m-widget24__number">
							                        0%
							                    </span>
							                </div>
							            </div>
							            end::Out-patient
							        </div>
							        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
							            <table class="table m-table m-table--head-separator-metal">
							                <thead>
							                    <tr>
							                        <th>
							                            No
							                        </th>
							                        <th>
							                            Date
							                        </th>
							                        <th>
							                            Name
							                        </th>
							                        <th>
							                            MRN
							                        </th>
							                        <th>
							                            Amount(RM)
							                        </th>
							                        <th>
							                            Remarks
							                        </th>
							                        <th>
							                            
							                        </th>
							                    </tr>
							                </thead>
							                <tbody style="background:#d3d3d34d;">
							                    <tr>
							                        <th scope="row">
							                            1
							                        </th>
							                        <td>
							                            27/12/1996
							                        </td>
							                        <td>
							                            Viknakaran A/l Gnanasegar
							                        </td>
							                        <td>
							                            293986
							                        </td>
							                        <td>
							                            200.00
							                        </td>
							                        <td>
							                            Teruk sakit ni
							                        </td>
							                        <td>
							                            <a href="#" title="Delete"><i class="la la-trash" style="color:red;"></i></a>
							                        </td>
							                    </tr>
							                </tbody>
							            </table>
							        </div>
							        <div class="col-md-12 col-lg-12 col-xl-12">
							            begin::Total In-Patient(benefits)
							            <div class="m-widget24" style="background:#f3efc4b3;">
							                <div class="m-widget24__item">
							                    <h4 class="m-widget24__title">
							                        In-Patient (Benefit)
							                    </h4>
							                    <br>
							                    <span class="m-widget24__desc">
							                        Yearly Benefits
							                    </span>
							                    <span class="m-widget24__stats m--font-info">
							                        RM 10,000.00
							                    </span>
							                    <div class="m--space-10"></div>
							                    <div class="progress m-progress--sm">
							                        <div class="progress-bar m--bg-info" role="progressbar" style="width: 0%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
							                    </div>
							                    <span class="m-widget24__change">
							                        Current Usage <b>RM 0.00 </b><br>
							                        Balance <b>RM 10,000.00</b>
							                    </span>
							                    <span class="m-widget24__number">
							                        0%
							                    </span>
							                </div>
							            </div>
							            end::Total In-Patient(benefits)
							        </div>
							        <div class="col-md-12 col-lg-12 col-xl-12" style="margin-top:20px;margin-bottom:40px;">
							            <table class="table m-table m-table--head-separator-metal">
							                <thead>
							                    <tr>
							                        <th>
							                            No
							                        </th>
							                        <th>
							                            Date
							                        </th>
							                        <th>
							                            Name
							                        </th>
							                        <th>
							                            MRN
							                        </th>
							                        <th>
							                            Amount(RM)
							                        </th>
							                        <th>
							                            Remarks
							                        </th>
							                        <th>
							                            
							                        </th>
							                    </tr>
							                </thead>
							                <tbody style="background:#d3d3d34d;">
							                    <tr>
							                        <th scope="row" colspan="7" style="text-align:center;">
							                            No treatment records found
							                        </th>
							                    </tr>
							                </tbody>
							            </table>
							        </div>
							    </div>
							    
							    <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							        <div class="modal-dialog modal-lg" role="document">
							            <div class="modal-content">
							                <div class="modal-header">
							                    <h5 class="modal-title" id="exampleModalLabel">
							                        Update Records
							                    </h5>
							                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                        <span aria-hidden="true">
							                            &times;
							                        </span>
							                    </button>
							                </div>
							                <div class="modal-body">
							                    <form>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Name <span class="red">*</span>
							                            </label>
							                            <div class="col-8">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Department <span class="red">*</span>
							                            </label>
							                            <div class="col-6">
							                                <select class="form-control m-input m-input--solid" id="exampleSelect1">
							                                    <option>
							                                        Choose...
							                                    </option>
							                                </select>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Designation
							                            </label>
							                            <div class="col-6">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Employment Date <span class="red">*</span>
							                            </label>
							                            <div class="col-4">
							                                <input class="form-control m-input m-input--solid" type="date" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Resign Date
							                            </label>
							                            <div class="col-4">
							                                <input class="form-control m-input m-input--solid" type="date" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Staff ID <span class="red">*</span>
							                            </label>
							                            <div class="col-3">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                IC Number <span class="red">*</span>
							                            </label>
							                            <div class="col-5">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Login Password
							                            </label>
							                            <div class="col-6">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                MRN
							                            </label>
							                            <div class="col-3">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Gender
							                            </label>
							                            <div class="col-9">
							                                <div class="m-radio-inline">
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Male
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Female
							                                        <span></span>
							                                    </label>
							                                </div>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Status
							                            </label>
							                            <div class="col-4">
							                                <select class="form-control m-input m-input--solid" id="exampleSelect1">
							                                    <option>
							                                        Choose...
							                                    </option>
							                                </select>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Email
							                            </label>
							                            <div class="col-6">
							                                <input class="form-control m-input m-input--solid" type="email" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Phone No
							                            </label>
							                            <div class="col-5">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Category <span class="red">*</span>
							                            </label>
							                            <div class="col-2">
							                                <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                            </div>
							                            <div class="col-6"  style="margin-top:8px;">
							                                <span class="m-form__help">
							                                    <small>(If Zero Benefits: X1. If doctor: Y1. If unlimited: Z1.)</small>
							                                </span>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Group
							                            </label>
							                            <div class="col-9">
							                                <div class="m-radio-inline">
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Staff
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        HOS
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Doctor
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Medical Officer
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Management
							                                        <span></span>
							                                    </label>
							                                </div>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Status
							                            </label>
							                            <div class="col-9">
							                                <div class="m-radio-inline">
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Confirm
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Not Confirm
							                                        <span></span>
							                                    </label>
							                                    <label class="m-radio">
							                                        <input type="radio" name="example_1" value="1">
							                                        Contract
							                                        <span></span>
							                                    </label>
							                                </div>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Leave the section
							                            </label>
							                            <div class="col-3">
							                                <select class="form-control m-input m-input--solid" id="exampleSelect1">
							                                    <option>
							                                        Choose...
							                                    </option>
							                                </select>
							                            </div>
							                        </div>
							                        <div class="form-group m-form__group row">
							                            <label for="example-text-input" class="col-3 col-form-label">
							                                Relative
							                            </label>
							                            <div class="col-2"  style="margin-top:8px;">
							                                <a href="#" class="m-link m-link--state m-link--info">
							                                    Add Relative
							                                </a>
							                            </div>
							                            <div class="col-6"  style="margin-top:8px;padding-left:0;">
							                                <span class="m-form__help">
							                                    <small>There is no records of your family members.</small>
							                                </span>
							                            </div>
							                        </div>
							                    </form>
							                </div>
							                <div class="modal-footer">
							                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
							                        Close
							                    </button>
							                    <button type="button" class="btn btn-primary">
							                        Update Record
							                    </button>
							                </div>
							            </div>
							        </div>
							    </div>
							    addCharges
							    <div class="modal fade" id="addCharges" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							        <div class="modal-dialog modal-lg" role="document">
							            <div class="modal-content">
							                <div class="modal-header">
							                    <h5 class="modal-title" id="exampleModalLabel">
							                        Add Charge to this Staff
							                    </h5>
							                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                        <span aria-hidden="true">
							                            &times;
							                        </span>
							                    </button>
							                </div>
							                <div class="modal-body">
							                    <form class="" action="" method="post">
							                        <div class="col-lg-12" style="border-bottom: 1px solid #e9ecef;">
							                            <div class="form-group m-form__group row">
							                                <label for="example-text-input" class="col-2 col-form-label">
							                                    Staff ID
							                                </label>
							                                <div class="col-3">
							                                    <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                </div>
							                            </div>
							                            <div class="form-group m-form__group row">
							                                <label for="example-text-input" class="col-2 col-form-label">
							                                    Staff IC
							                                </label>
							                                <div class="col-5">
							                                    <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                </div>
							                            </div>
							                        </div>
							                        <div class="row col-lg-12" style="margin-top:20px;">
							                            <div class="col-lg-8" style="">
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Date Visit
							                                    </label>
							                                    <div class="col-5">
							                                        <input class="form-control m-input m-input--solid" type="date" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Treatment type
							                                    </label>
							                                    <div class="col-4">
							                                        <select class="form-control m-input m-input--solid" id="exampleSelect1">
							                                            <option>
							                                                Choose...
							                                            </option>
							                                            <option>
							                                                Inpatient
							                                            </option>
							                                            <option>
							                                                Outpatient
							                                            </option>
							                                        </select>
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        MRN
							                                    </label>
							                                    <div class="col-4">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Episode
							                                    </label>
							                                    <div class="col-4">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Treatment
							                                    </label>
							                                    <div class="col-5">
							                                        <select class="form-control m-input m-input--solid" id="exampleSelect1">
							                                            <option>
							                                                Choose...
							                                            </option>
							                                            <option>
							                                                Normal Case
							                                            </option>
							                                            <option>
							                                                Critical Case
							                                            </option>
							                                            <option>
							                                                Gynaecology Case
							                                            </option>
							                                            <option>
							                                                Medication Charges
							                                            </option>
							                                        </select>
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        No. of Days
							                                    </label>
							                                    <div class="col-3">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Charges(RM)
							                                    </label>
							                                    <div class="col-4">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Bill No.
							                                    </label>
							                                    <div class="col-5">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Doctor Code
							                                    </label>
							                                    <div class="col-4">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Exceed Entitlement (RM)
							                                    </label>
							                                    <div class="col-5">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                                <div class="form-group m-form__group row">
							                                    <label for="example-text-input" class="col-4 col-form-label">
							                                        Remarks:
							                                    </label>
							                                    <div class="col-8">
							                                        <input class="form-control m-input m-input--solid" type="text" value="" id="example-text-input">
							                                    </div>
							                                </div>
							                            </div>
							                            <div class="col-lg-4" style="padding:0;">
							                                <div style="background-color:#CCCCFF;padding:10px;margin:0;">
							                                    <span style="font-size:13px;">
							                                        <b>MRN:</b> 84798<br/>
							                                        <b>Date Joined:</b> 10-10-2016<br/>
							                                        <b>Department:</b> Public Relation & Customer Service<br/>
							                                        <b>Designation:</b> Clinic Receptionist<br/>
							                                        <b>Category:</b> C3<br/>
							                                    </span>
							                                    <h6 style="margin-top:20px;text-decoration:underline;">Outpatient Benefits</h4>
							                                    <span style="font-size:13px;">
							                                        Benefit: <b>RM400.00</b><br/>
							                                        Current Usage: <b>RM0.00</b><br/>
							                                        Balance: <b>RM400.00</b><br/>
							                                    </span>
							                                    <h6 style="margin-top:20px;text-decoration:underline;">Inpatient Benefits for Staff</h4>
							                                    <span style="font-size:13px;">
							                                        Benefit: <b>RM 5,000.00</b><br/>
							                                        Current Usage: <b>RM 0.00</b><br/>
							                                        Balance: <b>5,000.00</b><br/>
							                                    </span>
							                                </div>
							                            </div>
							                        </div>
							                    </form>
							                </div>
							                <div class="modal-footer">
							                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
							                        Close
							                    </button>
							                    <button type="button" class="btn btn-primary">
							                        Send message
							                    </button>
							                </div>
							            </div>
							        </div>
							    </div>
							    
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

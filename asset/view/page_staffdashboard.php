<?php
	$pageModel = $general->pageInfo($page,'pageDir');
	$pageTitle = $general->pageInfo($page,'pageTitle');
	$pageName = $general->pageInfo($page,'pageName');
	$main->includePHP('model',$pageModel);
	$cont = new $pageModel();
	$homeDir = 'system.php?p='.$cont->securestring('encrypt','200');
	$pageDir = 'system.php?p='.$_GET['p'];
?>
<style media="screen">
    .td-a {
        width: 20px;
        padding-left: 0rem;
        padding-right: 0rem;
    }

    .red {
        color: red;
    }

    .cusdevider {
        border-bottom: 0.5px solid #ebedf2;
        margin: 28.6px 0px 0px 0px;
    }

</style>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator"><?php echo $pageTitle; ?></h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="<?php echo $homeDir; ?>" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="<?php echo $pageDir ?>" class="m-nav__link">
                                <span class="m-nav__link-text">
                                    <?php echo $pageName ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="m-content">
            <div class="row">
                <div id="alert-1" class="col-lg-12"></div>
                <div id="alert-pass" class="col-lg-12"></div>
                <div id="body-info" class="col-lg-12">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon">
                                        <i class="flaticon-info"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        User Infomation
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="row" id="benefit-info">
                                <!--display benefit-->
                                <div class="col-lg-6">
                                    <div class="m-widget24" style="background:#dde1e4b3;color:white;">
                                        <div class="m-widget24__item">
                                            <h4 class="m-widget24__title" style="background:#f0f1f2;width:10rem">&nbsp;</h4>
                                            <br>
                                            <div class="progress m-progress--sm" style="background:#f0f1f2;"></div><span class="m-widget24__change" style="color:#4b4545;"></span><span class="m-widget24__number"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="m-widget24" style="background:#dde1e4b3;color:white;">
                                        <div class="m-widget24__item">
                                            <h4 class="m-widget24__title" style="background:#f0f1f2;width:10rem">&nbsp;</h4>
                                            <br>
                                            <div class="progress m-progress--sm" style="background:#f0f1f2;"></div><span class="m-widget24__change" style="color:#4b4545;"></span><span class="m-widget24__number"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cusdevider" style="margin-top:28.6px;"></div>
                            <div class="row" id="history-info">
                                <div class="col-lg-12" style="margin-top:28.6px;margin-bottom:20px;">
                                    <span class="m-section__sub" style="text-decoration: underline;">
                                        Out-patient History.
                                    </span>
                                    <table class="table m-table m-table--head-separator-metal"><thead style="background: #dde1e4b3;"><tr><th style="background: #f0f1f2;color: #f0f1f2;">No</th><th style="background: #f0f1f2;color: #f0f1f2;">Date</th><th style="background: #f0f1f2;color: #f0f1f2;">Name</th><th style="background: #f0f1f2;color: #f0f1f2;">MRN</th><th style="background: #f0f1f2;color: #f0f1f2;">Amount(RM)</th><th style="background: #f0f1f2;color: #f0f1f2;">Remarks</th><th style="background: #f0f1f2;color: #f0f1f2;"></th></tr></thead><tbody style="background:#d3d3d34d;"><tr><th scope="row"></th><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><th scope="row" colspan="7" style="text-align:center;"></th></tr></tbody></table>
                                </div>
                                <div class="col-lg-12" style="margin-top:20px;">
                                    <span class="m-section__sub" style="text-decoration: underline;">
                                        In-patient History.
                                    </span>
                                    <table class="table m-table m-table--head-separator-metal"><thead><tr><th style="background: #f0f1f2;color: #f0f1f2;">No</th><th style="background: #f0f1f2;color: #f0f1f2;">Date</th><th style="background: #f0f1f2;color: #f0f1f2;">Name</th><th style="background: #f0f1f2;color: #f0f1f2;">MRN</th><th style="background: #f0f1f2;color: #f0f1f2;">Amount(RM)</th><th style="background: #f0f1f2;color: #f0f1f2;">Remarks</th><th style="background: #f0f1f2;color: #f0f1f2;"></th></tr></thead><tbody style="background:#d3d3d34d;"><tr><th scope="row"></th><td></td><td></td><td></td><td></td><td></td><td></td></tr><tr><th scope="row" colspan="8" style="text-align:center;"></th></tr></tbody></table>
                                </div>
                            </div>
                            <div class="cusdevider"></div>
                            <div class="row" id="personal-info">
                                <div class="col-lg-6" style="margin-top:28.6px;">
                                    <div class="m-section">
                                        <div class="m-section__sub" style="text-decoration: underline;">User Profile.</div>
                                        <div class="m-section__content">
                                            <table class="table">
                                                <tbody style="background:#dde1e4b3;">
                                                    <tr><th scope="row" class="col-lg-4" style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</th><td style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</td></tr><tr><th scope="row" class="col-lg-4" style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</th><td style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</td></tr><tr><th scope="row" class="col-lg-4" style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</th><td style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</td></tr><tr><th scope="row" class="col-lg-4" style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</th><td style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</td></tr><tr><th scope="row" class="col-lg-4" style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</th><td style="background:#dde1e4b3;margin:8px;color:#f0f1f2l;">&nbsp;</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="margin-top:28.6px;">
                                    <div class="m-section__sub" style="text-decoration: underline;margin-bottom:10px;margin-top:5px;">Dependents.</div>
                                    <div class="tab-pane active" id="m_widget4_tab1_content">
                                        <div class="m-widget4 m-widget4--progress"><div class="m-widget4__item" style="background:#dde1e4b3;padding:14px 20px;"><div class="m-widget4__img m-widget4__img--pic" style="background:#f0f1f2;color:#f0f1f2;border-radius:50%;">for picture</div><div class="m-widget4__info"><span class="m-widget4__title" style="background:#f0f1f2;color:#f0f1f2;">John Doe n Jane doe</span><br><span class="m-widget4__sub" style="background:#f0f1f2;color:#f0f1f2;">fountain de toure . co</span></div><div class="m-widget4__progress"></div><div class="m-widget4__ext"></div></div></div>
                                    </div>
                                    <div class="tab-pane active" id="m_widget4_tab1_content">
                                        <div class="m-widget4 m-widget4--progress"><div class="m-widget4__item" style="background:#dde1e4b3;padding:14px 20px;"><div class="m-widget4__img m-widget4__img--pic" style="background:#f0f1f2;color:#f0f1f2;border-radius:50%;">for picture</div><div class="m-widget4__info"><span class="m-widget4__title" style="background:#f0f1f2;color:#f0f1f2;">John Doe n Jane doe</span><br><span class="m-widget4__sub" style="background:#f0f1f2;color:#f0f1f2;">fountain de toure . co</span></div><div class="m-widget4__progress"></div><div class="m-widget4__ext"></div></div></div>
                                    </div>
                                    <div class="tab-pane active" id="m_widget4_tab1_content">
                                        <div class="m-widget4 m-widget4--progress"><div class="m-widget4__item" style="background:#dde1e4b3;padding:14px 20px;"><div class="m-widget4__img m-widget4__img--pic" style="background:#f0f1f2;color:#f0f1f2;border-radius:50%;">for picture</div><div class="m-widget4__info"><span class="m-widget4__title" style="background:#f0f1f2;color:#f0f1f2;">John Doe n Jane doe</span><br><span class="m-widget4__sub" style="background:#f0f1f2;color:#f0f1f2;">fountain de toure . co</span></div><div class="m-widget4__progress"></div><div class="m-widget4__ext"></div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--
                <div class="col-lg-3">
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption"><div class="m-portlet__head-title"><span class="m-portlet__head-icon"><i class="flaticon-list"></i></span><h3 class="m-portlet__head-text">User in department</h3></div></div>
                        </div>
                        <div class="m-portlet__body" id="aside-info">
                            <div class="m-widget4">
                                <div class="m-widget4__item" style="background:#dde1e4b3;padding:14px 20px;">
                                    <div class="m-widget4__img m-widget4__img--pic" style="background:#f0f1f2;color:#f0f1f2;border-radius:50%;">for picture</div>
                                    <div class="m-widget4__info"><span class="m-widget4__title" style="background:#f0f1f2;color:#f0f1f2;">John Doe n Jane doe</span><br><span class="m-widget4__sub" style="background:#f0f1f2;color:#f0f1f2;">fountain de toure . co</span></div>
                                    <div class="m-widget4__progress"></div>
                                    <div class="m-widget4__ext"></div>
                                </div>
                            </div><div class="m-widget4">
                                <div class="m-widget4__item" style="background:#dde1e4b3;padding:14px 20px;">
                                    <div class="m-widget4__img m-widget4__img--pic" style="background:#f0f1f2;color:#f0f1f2;border-radius:50%;">for picture</div>
                                    <div class="m-widget4__info"><span class="m-widget4__title" style="background:#f0f1f2;color:#f0f1f2;">John Doe n Jane doe</span><br><span class="m-widget4__sub" style="background:#f0f1f2;color:#f0f1f2;">fountain de toure . co</span></div>
                                    <div class="m-widget4__progress"></div>
                                    <div class="m-widget4__ext"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
-->
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    whenPageLoad();

    function whenPageLoad() {
        var session_logid = "<?php echo $_SESSION['logid']; ?>";
        passwordAlert();
        benefitInfo(session_logid);
        historyInfo(session_logid);
        personalInfo(session_logid);
//        departmentInfo(session_logid);
    }

    function changeInfo(value) {
        document.getElementById('history-info').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        document.getElementById('benefit-info').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        document.getElementById('personal-info').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        document.getElementById('aside-info').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>"; 
        var session_logid = value;
        passwordAlert();
        benefitInfo(session_logid);
        historyInfo(session_logid);
        personalInfo(session_logid);
//        departmentInfo(session_logid);
    }

    function passwordAlert() {
        var logid = "<?php echo $_SESSION['logid']; ?>";
        var dataString = {
            display: 'passwordAlert',
            val1: logid
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_display.php",
            data: dataString,
            cache: false,
            success: function(data) {
                if (data != '') {
                    document.getElementById("alert-pass").innerHTML = data;
                    $("#alert-pass").hide();

                    setTimeout(function() {
                        $("#alert-pass").slideDown();
                    }, 500);
                }
            }
        });
        return false;
    }

    function benefitInfo(value) {
//        document.getElementById('benefit-info').innerHTML = "<div style='width: 300px;margin: auto;padding:50px 0px;text-align: center;'><img src='images/system/gif/loading.gif' alt='' width='40'>Please wait..</div>";
        var logid = value;
        var dataString = {
            display: 'benefitInfo',
            val1: logid
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_display.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById("benefit-info").innerHTML = data;
                $("#benefit-info").hide();
                setTimeout(function() {
                    $("#benefit-info").fadeToggle(1500);
                }, 500);
            }
        });
        return false;
    }
    
    function historyInfo(value){
        var dataString = {
            display: 'historyInfo',
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_display.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById("history-info").innerHTML = data;
                $("#history-info").hide();
                setTimeout(function() {
                    $("#history-info").fadeToggle(1500);
                }, 500);
            }
        });
        return false;
    }
    
    function personalInfo(value){
        var dataString = {
            display: 'personalInfo',
            val1: value
        };
        $.ajax({
            type: "post",
            url: "asset/view/modal_display.php",
            data: dataString,
            cache: false,
            success: function(data) {
                document.getElementById("personal-info").innerHTML = data;
                $("#personal-info").hide();
                setTimeout(function() {
                    $("#personal-info").fadeToggle(1500);
                }, 500);
            }
        });
        return false;
    }
    
//    function departmentInfo(value){
//        var dataString = {
//            display: 'departmentInfo',
//            val1: value
//        };
//        $.ajax({
//            type: "post",
//            url: "asset/view/modal_display.php",
//            data: dataString,
//            cache: false,
//            success: function(data) {
//                document.getElementById("aside-info").innerHTML = data;
//                $("#aside-info").hide();
//                setTimeout(function() {
//                    $("#aside-info").fadeToggle(1500);
//                }, 500);
//            }
//        });
//        return false;
//    }

</script>

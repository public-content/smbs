<?php 
    include('../controller/general.php');
    include('../controller/connection.php');
    $general = new general();
    $who = $general->securestring('decrypt',$_SESSION['logid']);
    $errorMessage = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'></button><strong>Something Wrong!</strong>".mysql_error()."</div>";
?>

<!--function-->
<?php
    function getDevide($installment,$nom){
        $result = number_format(($installment/$nom),2);
        return $result;
    }

    function getNumber($dc,$status){
        $sql = mysql_query("SELECT d_id FROM tbl_installment WHERE d_datacode = '$dc' AND d_status = '$status'");
        $result = mysql_num_rows($sql);
        if($result == 0){
            $finish = mysql_query("UPDATE tbl_installment SET d_status = 'finish', d_modified = NOW(), d_who = '$who' WHERE d_id = '$dc'");
        }else{
            $finish = mysql_query("UPDATE tbl_installment SET d_status = 'progress', d_modified = NOW(), d_who = '$who' WHERE d_id = '$dc'");
        }
        return $result;
    }
?>







<!--checking-->
<?php
    if(isset($_POST['checking'])){
        if($_POST['checking'] == 'checkEach'){
            $did = mysql_real_escape_string($_POST['val1']);
            $nom = mysql_real_escape_string($_POST['val2']);
            
            if($nom != ''){
                $installment = $general->allTable($did, 'd_id', 'tbl_installment', 'd_value2');
                $after = getDevide($installment,$nom);
                $result = '<b>RM '.$after.' </b> for each month';
            }
            echo $result;
        }elseif($_POST['checking'] == 'installmentInfo'){
            $did = mysql_real_escape_string($_POST['val1']);
            $sql = mysql_query("SELECT * FROM tbl_installment WHERE d_datacode = $did");
            $row = mysql_fetch_assoc($sql);
            $user = $general->allTable($did,'d_id','tbl_installment','d_value1');
            $labid = $general->allTable($user,'d_staffID','tbl_labor','d_id');
            $departmentID = $general->allTable($user,'d_staffID','tbl_labor','d_department');
            $department = $general->allTable($departmentID,'d_id','tbl_setting','d_value');
            $name = $general->allTable($labid,'d_labor','tbl_profile','d_name');
            $ic = $general->allTable($labid,'d_labor','tbl_profile','d_ic');
            $total = $general->allTable($did,'d_id','tbl_installment','d_value2');
            $status = $general->allTable($did,'d_id','tbl_installment','d_status');
            
            echo "
                    <div class='col-lg-12'>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td class='col-1'>:</td>
                                    <td> ".$name." </td>
                                </tr>
                                <tr>
                                    <td>Department</td>
                                    <td class='col-1'>:</td>
                                    <td> ".$department." </td>
                                </tr>
                                <tr>
                                    <td>IC</td>
                                    <td class='col-1'>:</td>
                                    <td> ".$ic." </td>
                                </tr>
                                <tr>
                                    <td>Staff ID</td>
                                    <td class='col-1'>:</td>
                                    <td> ".$user." </td>
                                </tr>
                                <tr style='height:20px'>
                                    <td colspan='3'></td>
                                </tr>
                                <tr>
                                    <td>Exceed Usage Balance</td>
                                    <td class='col-1'>:</td>
                                    <td> RM ".$total." </td>
                                </tr>
                                <tr>
                                    <td>Installment Status</td>
                                    <td class='col-1'>:</td>
                                    <td> ".$status." </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class='col-lg-12' style='border-bottom:1px lightgrey solid; margin:20px 0px;'></div>
               ";
            
            echo "  <div class='kt-section'>
                        <div class='kt-section__content'>
                            <table class='table'>
                                <thead class='thead-dark'>
                                    <tr>
                                        <th>#</th>
                                        <th>Month</th>
                                        <th>Year</th>
                                        <th>Amount(RM)</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                ";
                $num = 1;
                $year = date("Y");
                $line = mysql_query("SELECT * FROM tbl_installment WHERE d_datacode = '$did'");
                if(mysql_num_rows($line)){
                    while($row = mysql_fetch_assoc($line)){
                        $perMonth = $row['d_value2'];
                        if($row['d_status'] == 'unpaid'){
                            $btn = "<button type='button' id='".$row['d_id']."btn' class='btn btn-info' onclick='actionpaid(".$row['d_id'].",".$row['d_datacode'].")'>Paid</button><a style='background:none;' class='m-link btn' onclick='delInstallment(".$row['d_id'].",".$did.")'><i class='la la-remove'></i></a>";
                            $bgcolor = "class=''";
                            $imonth = "<select class='form-control' id='".$row['d_id']."month'>
							<option value=''>select</option>
							<option value='January'>January</option>
							<option value='February'>February</option>
							<option value='March'>March</option>
							<option value='April'>April</option>
							<option value='May'>May</option>
							<option value='June'>June</option>
							<option value='July'>July</option>
							<option value='August'>August</option>
							<option value='September'>September</option>
							<option value='Oktober'>Oktober</option>
							<option value='November'>November</option>
							<option value='December'>December</option>
						  </select>";
                            $iyear = "<input type='text' class='form-control' id='".$row['d_id']."year' value='$year'>";
                            $ipermonth = "<input type='text' class='form-control' id='".$row['d_id']."permonth' value='$perMonth'>";
                        }elseif($row['d_status'] == 'paid'){
                            $btn = "<button type='button' id='".$row['d_id']."btn' class='btn btn-danger' onclick='actionunpaid(".$row['d_id'].",".$row['d_datacode'].")'>Unpaid</button>";
                            $bgcolor = "class='table-info'";
                            $imonth = $row['d_value1'];
                            $iyear = $row['d_value4'];
                            $ipermonth = $row['d_value2'];
                        }
                        echo "<tr ".$bgcolor.">";
                        echo "<th scope='row'>".$num."</th>";
                        echo "<td>".$imonth."</td>";
                        echo "<td>".$iyear."</td>";
                        echo "<td>".$ipermonth."</td>";
                        echo "<td>".$btn."</td>";
                        echo "</tr>";
                $num++;
                    }
                }
            echo "<tr><td colspan='4'></td><td colspan='1'><a href='#' class='m-link m--font-bold' onclick='addInstallment(".$did.")'>Add Row</a></td></tr>";
            echo "</tbody></table>";
            

        }elseif($_POST['checking'] == 'getList'){
            $num = 1;
            $list = mysql_query("SELECT * FROM tbl_installment WHERE d_status = 'complete' AND d_type = 'MAIN'");
            if(mysql_num_rows($list)){
                while($row = mysql_fetch_assoc($list)){
                    $labid = $general->allTable($row['d_value1'],'d_staffID','tbl_labor','d_id');
                    $name = $general->allTable($labid,'d_labor','tbl_profile','d_name');
                    $departmentID = $general->allTable($labid,'d_id','tbl_labor','d_department');
                    $department = $general->allTable($departmentID,'d_id','tbl_setting','d_value');
                    echo "<tr>
                              <th>".$num."</th>
                              <th>".$name."</th>
                              <th>".$row['d_value1']."</th>
                              <th>".$department."</th>
                              <th>".$row['d_value5']."</th>
                              <th>".$row['d_created']."</th>
                              <th>".$row['d_modified']."</th>
                              <th><a title='Delete'><i class='la la-trash' style='color:red;cursor:pointer' onclick='deleteInstallment('".$row['d_id']."')'></i></a></th>
                          </tr>";
                } $num++;
            }
        }
    }
?>








<!--process-->
<?php
    if(isset($_POST['process'])){
        
        if($_POST['process'] == 'createInstallment'){
            $did = mysql_real_escape_string($_POST['val1']);
            $nom = mysql_real_escape_string($_POST['val2']);
            $installment = $general->allTable($did, 'd_id', 'tbl_installment', 'd_value2');
            $refnumber = $general->allTable($did, 'd_id', 'tbl_installment', 'd_value3');
//            $who = $general->securestring('decrypt',$_SESSION['logid']);
            $after = getDevide($installment,$nom);
            
            echo $did;
            
//            loop depends on $nom
            for($i=0; $i<$nom; $i++){
                $sql = mysql_query("INSERT INTO tbl_installment(d_type, d_datacode, d_value1, d_value2, d_value3, d_value4, d_created, d_modified, d_who, d_status)VALUES('CHILD', '$did', '', '$after', '$refnumber', '', NOW(), NOW(), '$who', 'unpaid')");
                
                if(!$sql){
                    echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Something Wrong!
                            </strong>
                         ".mysql_error()."
                        </div>";
                }else{
                    $result = 1;   
                    
                }
            }
            
            if($result == 1){
                $sql2 = mysql_query("UPDATE tbl_installment SET d_status = 'progress' WHERE d_id = '$did'");
                    if(!$sql2){
                        
                        echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Something Wrong!
                            </strong>
                         ".mysql_error()."
                        </div>";  
                        
                    }else{
                        
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            New Installment has been created!
                        </div>";
                    }
            }
            
        }elseif($_POST['process'] == 'actionpaid'){
            $did = $_POST['val1'];
            $datacode = $_POST['val2'];
            $month = mysql_real_escape_string(trim($_POST['val3']));
            $year = mysql_real_escape_string(trim($_POST['val4']));
            $permonth = mysql_real_escape_string(trim($_POST['val5']));
            
            
            //UPDATE child d_status and other column
            $update = mysql_query("UPDATE tbl_installment SET d_value1 = '$month', d_value2 = '$permonth', d_value4 = '$year', d_modified = NOW(), d_who = '$who', d_status = 'paid' WHERE d_id = '$did'");
            if(!$update){
                echo $errorMessage;
            }else{
                //UPDATE Main d_value2(installment balance)
                $actualBalance = $general->allTable($datacode,'d_id','tbl_installment','d_value2'); //get the main balance
                $newBalance = $actualBalance-$permonth; //balance is minus with permonth.
                $balance = mysql_query("UPDATE tbl_installment SET d_value2 = '$newBalance', d_modified = NOW(), d_who = '$who' WHERE d_id = '$datacode'");
                if(!$balance){
                    echo $errorMessage;
                }else{
                    //UPDATE equal payment
                    $getUnpaid = getNumber($datacode,'unpaid'); //get number unpaid
                    $newUnpaid = $newBalance/$getUnpaid; //balance devided to each child unpaid
                    $unpaid = mysql_query("UPDATE tbl_installment SET d_value2 = '$newUnpaid' WHERE d_datacode = '$datacode' AND d_status = 'unpaid'");
                    if(!$unpaid){
                        echo $errorMessage;
                    }else{
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
                    }
                }
            }
            
        }elseif($_POST['process'] == 'actionunpaid'){
            $did = $_POST['val1'];
            $datacode = $_POST['val2'];
            $permonth = $general->allTable($did,'d_id','tbl_installment','d_value2');
            echo mysql_error();
            
            //UPDATE child d_status and other column
            $update = mysql_query("UPDATE tbl_installment SET d_value1 = '', d_value2 = '', d_value4 = '', d_modified = NOW(), d_who = '$who', d_status = 'unpaid' WHERE d_id = '$did'");
            if(!$update){
                echo $errorMessage;
            }else{
                $actualBalance = $general->allTable($datacode,'d_id','tbl_installment','d_value2'); //get the main balance
                $newBalance = $actualBalance+$permonth; //balance is minus with permonth. //balance is minus with permonth.
                $balance = mysql_query("UPDATE tbl_installment SET d_value2 = '$newBalance', d_modified = NOW(), d_who = '$who' WHERE d_id = '$datacode'");
                if(!$balance){
                    echo $errorMessage;
                }else{
                    //UPDATE equal payment
                    $getUnpaid = getNumber($datacode,'unpaid'); //get number unpaid
                    $newUnpaid = $newBalance/$getUnpaid; //balance devided to each child unpaid
                    $unpaid = mysql_query("UPDATE tbl_installment SET d_value2 = '$newUnpaid' WHERE d_datacode = '$datacode' AND d_status = 'unpaid'");
                    if(!$unpaid){
                        echo $errorMessage;
                    }else{
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
                    }
                }
            }
        }elseif($_POST['process'] == 'actionaddrow'){
            $maindid = $_POST['val1'];
            $getUnpaid = getNumber($maindid,'unpaid') + 1;
            $actualBalance = $general->allTable($maindid,'d_id','tbl_installment','d_value2');
            $equalBal = $actualBalance/$getUnpaid;
            $add = mysql_query("INSERT INTO tbl_installment(d_type, d_datacode, d_value1, d_value2, d_value3, d_value4, d_created, d_modified, d_who, d_status)VALUES('CHILD', '$maindid', '', '$equalBal', '', '', NOW(), NOW(), '$d_who', 'unpaid')");
            if(!$add){
               echo mysql_error(); 
            }else{
                $unpaid = mysql_query("UPDATE tbl_installment SET d_value2 = '$equalBal' WHERE d_datacode = '$maindid' AND d_status = 'unpaid'");
                if(!$unpaid){
                        echo $errorMessage;
                    }else{
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
                    }
            }
        }elseif($_POST['process'] == 'actiondelrow'){
            $maindid = $_POST['val1'];
            $childdid = $_POST['val2'];
            $delete = mysql_query("DELETE FROM tbl_installment WHERE d_id = '$childdid'");
            if(!$delete){
                echo $errorMessage;
            }else{
                $actualBalance = $general->allTable($maindid,'d_id','tbl_installment','d_value2');
                $getUnpaid = getNumber($maindid,'unpaid');
                $equalBal = $actualBalance/$getUnpaid;
                $unpaid = mysql_query("UPDATE tbl_installment SET d_value2 = '$equalBal' WHERE d_datacode = '$maindid' AND d_status = 'unpaid'");
                if(!$unpaid){
                        echo $errorMessage;
                    }else{
                        echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
                    }
            }
        }elseif($_POST['process'] == 'actioncomplete'){
            $maindid = $_POST['val1'];
            $main = mysql_query("UPDATE tbl_installment SET d_status = 'complete' WHERE d_id = '$maindid'");
            if(!$main){
                echo $errorMessage;
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
            }
        }elseif($_POST['process'] == 'actiondelete'){
            $maindid = $_POST['val1'];
            $main = mysql_query("UPDATE tbl_installment SET d_status = 'delete' WHERE d_id = '$maindid'");
            if(!$main){
                echo $errorMessage;
            }else{
                echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button>
                            <strong>
                                Success!
                            </strong>
                            Update complete!
                        </div>";
            }
        }
        
    }
?>
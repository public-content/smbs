<?php
  class message{

    public function normalMessage($color,$title,$message){
      $msg = "<div class='alert alert-".$color." alert-dismissible fade show' role='alert'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'></button> <strong> ".$title."! </strong>".$message." </div>";
      return $msg;
    }

  }
?>

<?php
  class pageSession extends general{
    function __construct(){
		
    }

    public function userLogin($username,$password){
      $username = mysql_real_escape_string(trim($username));
      $password = mysql_real_escape_string(trim($password));

      // 1. check existant
      $checkLogin = $this->checkExist($username);
      if($checkLogin == 1){
        // 2. check password
        $checkPassword = $this->checkPass($username, $password);
        if($checkPassword == 1){
          // 3. check active
          $checkActive = $this->checkActive($username, $password);
          if($checkActive == 1){
            // 4. check session
            $setSession = $this->setSession($username);
            if($setSession == 1){
              // 5.check role
              $checkRole = $this->checkRole($username);
              if($checkRole == "0"){
                return 'Location:index.php?action='.$this->securestring('encrypt','invalid-session');
              }else{
                return $checkRole;
              }
            }else{
              return 'Location:index.php?action='.$this->securestring('encrypt','invalid-session');
            }
          }else{
            return 'Location:index.php?action='.$this->securestring('encrypt','acc-deactive');
          }
        }else{
          return 'Location:index.php?action='.$this->securestring('encrypt','wrong-idpass');
        }
      }else{
        return 'Location:index.php?action='.$this->securestring('encrypt','No-Exist');
      }
    }

    public function checkExist($username){
      $sql = mysql_query("SELECT d_id FROM tbl_user WHERE d_username = '$username'");
      $row = mysql_num_rows($sql);
      return $row;
    }

    public function checkPass($user, $pass){
      $sql = mysql_query("SELECT d_id FROM tbl_user WHERE d_username = '$user' AND d_password = '$pass'");
      $row = mysql_num_rows($sql);
      return $row;
    }

    public function checkActive($user, $pass){
      $sql = mysql_query("SELECT d_id FROM tbl_user WHERE d_username = '$user' AND d_password = '$pass' AND d_status = 'active'");
      $row = mysql_num_rows($sql);
      return $row;
    }

    public function setSession($username){
      $sql = mysql_query("SELECT d_id,d_userid,d_role FROM tbl_user WHERE d_username = '$username' AND d_status = 'active'");
      $row = mysql_fetch_assoc($sql);
      $_SESSION['logid'] = $this->securestring('encrypt',$row['d_id']);
      $_SESSION['proid'] = $this->securestring('encrypt',$row['d_userid']);
      $_SESSION['labid'] = $this->securestring('encrypt',$this->takeProfile('d_labor',$row['d_userid']));
      $_SESSION['roles'] = $this->securestring('encrypt',$row['d_role']);
      $str = 'NOW';
      $salt = strtotime($str)+$row['did'];
      $_SESSION['loginsession'] = $this->securestring('encrypt',$salt);
      $setsess = $this->updateSession($row['d_id'], $salt);

      return $setsess;
    }

    public function takeProfile($col, $id){
      $sql = mysql_query("SELECT $col FROM tbl_profile WHERE d_id = '$id'");
      $row = mysql_fetch_assoc($sql);
      return $row[$col];
    }

    public function updateSession($id, $data){
      $sql = mysql_query("UPDATE tbl_user SET d_loginsess = '$data' WHERE d_id = '$id'");
      if(!$sql){
        return mysql_error();
      }else{
        return 1;
      }
    }

    public function checkRole($user){
      $sql = mysql_query("SELECT d_role FROM tbl_user WHERE d_username = '$user' AND d_status = 'active'");
      $row = mysql_fetch_assoc($sql);
      $role = $row['d_role'];

      $firstPage = $this->checkPrivillege($role);

      if($firstPage != ''){
        $result = 'Location:system.php?p='.$this->securestring('encrypt',$firstPage);
      }else{
        $result = "0";
      }
      return $result;
    }

    public function checkPrivillege($data){
      $sql = mysql_query("SELECT d_value FROM tbl_privileges WHERE d_role = '$data' AND d_type = 'page' ORDER BY d_id ASC LIMIT 1 ");
      $row = mysql_fetch_assoc($sql);
      return $row['d_value'];
    }

  }
?>

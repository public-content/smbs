<?php
//general function store in this class
class general
{
  public function __construct(){
	
  }

  public function headerTo($did){
    return 'system.php?p='.$this->securestring('encrypt',$did);
  }

  public function pageInfo($did,$col){
    $sql = mysql_query("SELECT $col FROM tbl_page WHERE did = '$did'");
    $row = mysql_fetch_assoc($sql);
    return $row[$col];
  }

  public function securestring($action,$string){
    $output = false;

    $encrypt_method = "AES-256-CBC";
    $secret_key = 'IPOH SPECIALIST HOSPITAL';
    $secret_iv = 'KPJ IPOH';

    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    if( $action == 'encrypt' ) {
      $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
      $output = base64_encode($output);
    }
    else if( $action == 'decrypt' ){
      $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }

    return $output;
  }

  public function getUserIP(){
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
      $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
      $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
      $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
      $ip = $forward;
    }
    else
    {
      $ip = $remote;
    }

    return $ip;
  }

  public function checkPrivilleges($page, $role){
    $roles = $this->securestring('decrypt',$role);
    // $ses = "391";
    $sql = mysql_query("SELECT d_id FROM tbl_privileges WHERE d_value = '$page' AND d_role = '$roles'");
    $row = mysql_num_rows($sql);
    return $row;
  }
    
  public function checkPrivilleges1($grp, $role){
    $roles = $this->securestring('decrypt',$role);
    $sql1 = mysql_query("SELECT did FROM tbl_page WHERE pageGroup = '$grp'");
    if(mysql_num_rows($sql1)){
        while($row1 = mysql_fetch_assoc($sql1)){
           $pagedid = $row1['did'];
           $sql = mysql_query("SELECT d_id FROM tbl_privileges WHERE d_role = '$roles' AND d_value = '$pagedid'"); 
           $row = mysql_num_rows($sql);
           $total[] = $row;
        }
    }
    
      $result = array_sum($total);
      return $result;
  }
  
  public function masterTake($tbl, $col, $did){
	$did = $this->securestring('decrypt',$did);
	$sql = mysql_query("SELECT $col FROM $tbl WHERE d_id = '$did'");
	$row = mysql_fetch_assoc($sql);
	return $row[$col];
  }
    
  public function changePassword($data){
      $old = sha1(mysql_real_escape_string(trim($data['input-old'])));
      $new = sha1(mysql_real_escape_string(trim($data['input-new'])));
      $renew = sha1(mysql_real_escape_string(trim($data['input-renew'])));
      $logidPass = $this->masterTake('tbl_user', 'd_password', $_SESSION['logid']);
      $logid = $this->securestring('decrypt',$_SESSION['logid']);
      //check old
      if($old == $logidPass){
          if($new == $renew){
              $sql = mysql_query("UPDATE tbl_user SET d_password = '$new' WHERE d_id = '$logid'");
              if(!$sql){
                  $result = mysql_error(); 
              }else{
                  $result = 1; 
              }
          }else{
             $result = "New password not match";
          }
      }else{
           $result = 'Old password not match';
      }
      
      return $result;
  }
    
  public function allTable($val, $colP, $Ftbl, $colN){
      $sql = mysql_query("SELECT $colN FROM $Ftbl WHERE $colP = '$val'");
      $row = mysql_fetch_assoc($sql);
      if($row[$colN] != ''){
          return $row[$colN];
      }else{
        return '';
      }
  }
    
  public function allTableActive($val, $colP, $Ftbl, $colN){
      $sql = mysql_query("SELECT $colN FROM $Ftbl WHERE $colP = '$val' AND d_status = 'active'");
      $row = mysql_fetch_assoc($sql);
      if($row[$colN] != ''){
          return $row[$colN];
      }else{
        return '';
      }
  }
    
  public function allTable2($val, $colP, $Ftbl, $colN){
      $sql = mysql_query("SELECT $colN FROM $Ftbl WHERE $colP = '$val'");
      $row = mysql_fetch_assoc($sql);
      if($row[$colN] != ''){
          return $row[$colN];
      }else{
        return '0';
      }
  }
    
        
  public function groupPage($group){
      $sql = mysql_query("SELECT did FROM tbl_page WHERE pageGroup = '$group' AND status = 'active'");
      $row = mysql_num_rows($sql);
      return $row;
  }
    
  public function getLatest($table, $column){
      $sql = mysql_query("SELECT $column FROM $table ORDER BY $column DESC LIMIT 1");
      $row = mysql_fetch_assoc($sql);
      return $row['d_datacode'] + 1;
  }

}
?>

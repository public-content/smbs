<?php
  class dashboard extends general{

    public function __construct(){

    }
//------------------------------------------------------------------------------------------------------------------- OUTPATIENT --//     
    public function totalOutpatient(){
        $sql = mysql_query("SELECT d_id,d_labor FROM tbl_profile WHERE d_type = 'STAFF' AND d_status = 'active'");
        if(mysql_num_rows($sql)){
            while($row = mysql_fetch_assoc($sql)){
                $category = $this->getCategory($row['d_labor']);
                $oupatient = $this->getOupatient($category);
                $dependent = $this->valueAfterDependent($oupatient, $row['d_id']);
                $total[] = $dependent;
            }
        }
        return $total;
    }
      
    public function getCategory($did){
         $sql = mysql_query("SELECT d_category FROM tbl_labor WHERE d_id = '$did'");
        $row = mysql_fetch_assoc($sql);
        return $row['d_category'];
    }
    
    public function getOupatient($cat){
        $sql = mysql_query("SELECT d_value2 FROM tbl_setting WHERE d_id = '$cat'");
        $row = mysql_fetch_assoc($sql);
        return $row['d_value2'];
    }
      
    public function valueAfterDependent($val, $id){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$id'");
        $row = mysql_num_rows($sql);
        if($row != '0' && $val == '400.00'){
            $result = $val*2;
        }else{
            $result = $val;
        }
        return $result;
    }
      
    public function usage($type){
        $yr = date('Y');
        if($type == 'OUT'){
//            $sql = mysql_query("SELECT SUM(tbl_treatment.d_charge) AS chargeMe FROM tbl_treatment LEFT JOIN tbl_labor ON tbl_treatment.d_staffID = tbl_labor.d_staffID LEFT JOIN tbl_profile ON tbl_labor.d_id = tbl_profile.d_labor WHERE (tbl_profile.d_status = 'active') AND YEAR(tbl_treatment.d_vdate) = '$yr' AND tbl_treatment.d_type = 'OUT' AND (tbl_labor.d_category != '19')");
            $sql = mysql_query("SELECT sum(d_charge) as chargeMe FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_status = 'active') AND d_type = 'OUT' AND YEAR(d_vdate) = '$yr' AND d_status = 'active'");
            
        $row = mysql_fetch_assoc($sql);
        $result = $row['chargeMe'];
        }
        return $result;
    }
      
    public function getPercentage($total,$bal){
        $result = round(($bal/$total)*100);
        return $result."%";
    }
      
    public function totalStaff(){
        $first = mysql_query("SELECT d_id FROM tbl_labor WHERE d_status = 'active'");
        $row = mysql_num_rows($first);
        return $row;
    }
//------------------------------------------------------------------------------------------------------------------- OUTPATIENT --//
//------------------------------------------------------------------------------------------------------------------- INPATIENT --//
    public function totalInpatient($type){
        $befordate = "2005-03-07";
        if($type == 'benefit'){
            $sql = mysql_query("SELECT SUM(tbl_setting.d_value3) as TOTAL FROM tbl_setting LEFT JOIN tbl_labor ON tbl_setting.d_id = tbl_labor.d_category LEFT JOIN tbl_profile ON tbl_labor.d_id = tbl_profile.d_labor WHERE tbl_profile.d_status = 'active' AND tbl_labor.d_employment >= '2005-03-07'");
            $row = mysql_fetch_assoc($sql);
            $result = $row['TOTAL'];
            return $result;
        }elseif($type == 'day'){
            $sql = mysql_query("SELECT tbl_profile.d_id FROM tbl_profile LEFT JOIN tbl_labor ON tbl_profile.d_labor = tbl_labor.d_id WHERE tbl_profile.d_status = 'active' AND tbl_labor.d_employment < '$befordate'");
            if(mysql_num_rows($sql)){
                while($row = mysql_fetch_assoc($sql)){
                    $did = $row['d_id'];
                    $day = $this->countDays('14',$did);
                    $result[] = $day;
                    
                }
            }
            return $result;
        }
        
        
    }
      
    public function countDays($val, $did){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$did'");
        $row = mysql_num_rows($sql);
        if($row != '0'){
            $result = $val*2;
        }else{
            $result = $val;
        }
        return $result;
    }
      
    public function getEmployment($did){
        $sql = mysql_query("SELECT d_employment FROM tbl_labor WHERE d_id = '$did'");
        $row = mysql_fetch_assoc($sql);
        return $row['d_employment'];
    }
    
    public function getInpatient($cat){
        $sql = mysql_query("SELECT d_value3 FROM tbl_setting WHERE d_id = '$cat'");
        $row = mysql_fetch_assoc($sql);
        return $row['d_value3'];
    }
      
    public function valueAfterIP($val, $id){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$id'");
        $row = mysql_num_rows($sql);
        if($row != '0' && $val != 'infinite'){
//            $result = $val*2;           
            $result = $val;
        }else{
            $result = $val;
        }
        return $result;
    }
      
    public function totalStaffIP($type){
       if($type == 'benefit'){
           $sql = mysql_query("SELECT d_id FROM tbl_labor WHERE d_status = 'active' AND d_employment > '2005-03-07'");
           $row = mysql_num_rows($sql);
       }else{
           $sql = mysql_query("SELECT d_id FROM tbl_labor WHERE d_status = 'active' AND d_employment < '2005-03-07'");
           $row = mysql_num_rows($sql);
       }
        return $row;
    }
      
    public function balanceIP($type){
        if($type == 'benefit'){
//           $sql = mysql_query("SELECT SUM(tbl_treatment.d_charge) as total FROM tbl_labor LEFT JOIN tbl_treatment ON tbl_labor.d_staffID = tbl_treatment.d_staffID WHERE tbl_labor.d_status = 'active' AND tbl_labor.d_employment > '2005-03-07' AND tbl_treatment.d_type = 'IN' AND YEAR(tbl_treatment.d_created) = '2019' AND (tbl_labor.d_category != '19' OR tbl_labor.d_category != '22' AND tbl_labor.d_category != '23') ");
             $sql = mysql_query("SELECT sum(d_charge) as total FROM tbl_treatment WHERE d_staffID = ANY(SELECT d_staffID FROM tbl_labor WHERE d_employment > '2005-03-07' AND (d_category != '19' OR d_category != '22' OR d_category != '23') AND d_status = 'active') AND d_type = 'IN' AND YEAR(d_vdate) = '2019' AND d_status = 'active'");
           if(mysql_num_rows($sql)){
               while($row=mysql_fetch_assoc($sql)){
                   $result[] = $row['total'];
               }
           }
           return $result;
        }else{
            $sql = mysql_query("SELECT SUM(tbl_treatment.d_day) as total FROM tbl_labor LEFT JOIN tbl_treatment ON tbl_labor.d_staffID = tbl_treatment.d_staffID WHERE tbl_labor.d_status = 'active' AND tbl_labor.d_employment > '2005-03-07' AND tbl_treatment.d_type = 'IN' AND YEAR(tbl_treatment.d_created) = '2019' AND (tbl_labor.d_category != '19' OR tbl_labor.d_category != '22' AND tbl_labor.d_category != '23') ");
           if(mysql_num_rows($sql)){
               while($row=mysql_fetch_assoc($sql)){
                   $result[] = $row['total'];
               }
           }
           return $result;
        }
        return $total;
    }
      
    public function getTreatment($did){
        $yr  = date('Y');
        $sql = mysql_query("SELECT sum(d_charge)as total FROM tbl_treatment WHERE d_staffID = '$did' AND year(d_created) = '$yr'");
//        if(mysql_num_rows($sql)){
//            while($row=mysql_fetch_assoc($sql)){
//                $total[] = $row['d_charge'];
//            }
//        }
        $row = mysql_fetch_assoc($sql);
        return $rowp['total'];
    }
      
    
//------------------------------------------------------------------------------------------------------------------- INPATIENT --//      
      

      

      
  }
?>

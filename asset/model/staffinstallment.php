<?php 
    class staffinstallment extends general{
        
        public function getModalCreate($did){
            $staffID = $this->allTable($did, 'd_id', 'tbl_installment', 'd_value1');
            $labid = $this->allTable($staffID, 'd_staffID', 'tbl_labor', 'd_id');
            $staffIC = $this->allTable($labid, 'd_labor', 'tbl_profile', 'd_ic');
            $staffName = $this->allTable($labid, 'd_labor', 'tbl_profile', 'd_name');
        ?>
        <div class="modal fade" id="create<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Installment Report Generator
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="divue">
                                &times;
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="m-form m-form--fit m-form--label-align-right">
                            <div class="form-group m-form__group row">
								<label for="did<?php echo $did; ?>" class="col-2 col-form-label">
                                    Staff IC:
                                </label>
								<div class="col-lg-10">
									<input type="text" class="form-condivol" id="" value="<?php echo $staffIC; ?>" readonly>
									<input type="hidden" class="form-condivol" id="did<?php echo $did; ?>" value="<?php echo $did; ?>" readonly>
									<span class="m-form__help">
										<?php echo $staffName; ?>
									</span>
								</div>
                            </div>
                            <div class="form-group m-form__group row">
								<label for="example-text-input" class="col-2 col-form-label">
                                    Staff ID:
                                </label>
								<div class="col-lg-10">
									<input type="text" class="form-condivol" id="" value="<?php echo $staffID; ?>" readonly>
								</div>
                            </div>
                            <div class="form-group">
                                <label for="nom<?php echo $did; ?>" class="form-condivol-label">
                                    No of Month:
                                </label>
                                <input type="text" class="form-condivol" id="nom<?php echo $did; ?>" value="" onkeyup="checkEach('<?php echo $did; ?>')" required>
                                <span class="m-form__help" id="each<?php echo $did; ?>">
								    
								</span>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary" onclick="submitCreate(<?php echo $did; ?>)" data-dismiss="modal">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        
        public function getModalComplete($did){
        ?>
        <div class="modal fade" id="modalComplete<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Complete Installment
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="divue">
                                &times;
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                      <?php  
                            $sql = mysql_query("SELECT * FROM tbl_installment WHERE d_datacode = $did");
                            $row = mysql_fetch_assoc($sql);
                            $user = $this->allTable($did,'d_id','tbl_installment','d_value1');
                            $labid = $this->allTable($user,'d_staffID','tbl_labor','d_id');
                            $departmentID = $this->allTable($user,'d_staffID','tbl_labor','d_department');
                            $department = $this->allTable($departmentID,'d_id','tbl_setting','d_value');
                            $name = $this->allTable($labid,'d_labor','tbl_profile','d_name');
                            $ic = $this->allTable($labid,'d_labor','tbl_profile','d_ic');
                            $total = $this->allTable($did,'d_id','tbl_installment','d_value2');
                            $status = $this->allTable($did,'d_id','tbl_installment','d_status'); 
                        ?>
                       <div class='col-lg-12'>
                            <div>
                                <div>
                                    <div>
                                        <div class="na-col-1">Name :</div>
                                        <div class="na-col-2"> <?php echo $name; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Department :</div>
                                        <div class="na-col-2"> <?php echo $department; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">IC :</div>
                                        <div class="na-col-2"> <?php echo $ic; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Staff ID :</div>
                                        <div class="na-col-2"> <?php echo $user; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Exceed Usage Balance :</div>
                                        <div class="na-col-2"> RM <?php echo $total; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Installment Status :</div>
                                        <div class="na-col-2"> <?php echo $status; ?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <p>
                            Are you sure to complete this installment ?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="completeInstallment(<?php echo $did; ?>)">
                            Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        
        public function getModalDelete($did){
        ?>
        <div class="modal fade" id="modalDelete<?php echo $did; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="divue">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Deleting Installment
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="divue">
                                &times;
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                         <?php  
                            $sql = mysql_query("SELECT * FROM tbl_installment WHERE d_datacode = $did");
                            $row = mysql_fetch_assoc($sql);
                            $user = $this->allTable($did,'d_id','tbl_installment','d_value1');
                            $labid = $this->allTable($user,'d_staffID','tbl_labor','d_id');
                            $departmentID = $this->allTable($user,'d_staffID','tbl_labor','d_department');
                            $department = $this->allTable($departmentID,'d_id','tbl_setting','d_value');
                            $name = $this->allTable($labid,'d_labor','tbl_profile','d_name');
                            $ic = $this->allTable($labid,'d_labor','tbl_profile','d_ic');
                            $total = $this->allTable($did,'d_id','tbl_installment','d_value2');
                            $status = $this->allTable($did,'d_id','tbl_installment','d_status'); 
                        ?>
                       <div class='col-lg-12'>
                            <div>
                                <div>
                                    <div>
                                        <div class="na-col-1">Name :</div>
                                        <div class="na-col-2"> <?php echo $name; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Department :</div>
                                        <div class="na-col-2"> <?php echo $department; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">IC :</div>
                                        <div class="na-col-2"> <?php echo $ic; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Staff ID :</div>
                                        <div class="na-col-2"> <?php echo $user; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Exceed Usage Balance :</div>
                                        <div class="na-col-2"> RM <?php echo $total; ?> </div>
                                    </div>
                                    <div>
                                        <div class="na-col-1">Installment Status :</div>
                                        <div class="na-col-2"> <?php echo $status; ?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <p>
                            Are you sure to delete this ?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deleteInstallment(<?php echo $did; ?>)">
                            Yes
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        
    }
?>
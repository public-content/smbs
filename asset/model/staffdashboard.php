<?php
  class staffdashboard extends general{

    public function __construct(){

    }
	
	public function checkPassword($id){
		$did = $this->securestring('decrypt',$id);
		$sql = mysql_query("SELECT d_password FROM tbl_user WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		$pass = $row['d_password'];
		$general = sha1('1234');
		
		if($pass == $general){
			return 1;
		}else{
			return 0;
		}
		
	}
	
	public function getYearlyAmount($type, $did){
		$did = $this->securestring('decrypt',$did);
		$sql = mysql_query("SELECT d_category FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		$category = $row['d_category'];
		
		if($type == 'out'){
			$value = $this->getAmount('d_value2',$category);
			$result = $this->checkDependent($value,$did);
		}elseif($type == 'in'){
			$result = $this->getAmount('d_value3',$category);
		}
		return $result;
	}
	
	public function getCurrentUsage($type, $did){
		$did = $this->securestring('decrypt',$did);
		$staffID = $this->getStaffID($did);
		$year = date('Y');
		$sql = mysql_query("SELECT sum(d_charge) as 'total' FROM tbl_treatment WHERE d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
		$row = mysql_fetch_assoc($sql);
		$result = $row['total'];
		if($result != ''){
			return $result;
		}else{
			return '0.00';
		}
	}
	
	public function getCurrentUsageDays($type, $did){
		$did = $this->securestring('decrypt',$did);
		$staffID = $this->getStaffID($did);
		$year = date('Y');
		$sql = mysql_query("SELECT sum(d_day) as 'total' FROM tbl_treatment WHERE d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
		$row = mysql_fetch_assoc($sql);
		$result = $row['total'];
		if($result != ''){
			return $result." days";
		}else{
			return '0 day';
		}
	}
	
	public function getCurrentUsageDaysDependent($type, $did){
		$did = $this->securestring('decrypt',$did);
		$staffID = $this->getStaffID($did);
		$year = date('Y');
		$sql = mysql_query("SELECT sum(d_day) as 'total' FROM tbl_treatment WHERE d_type = 'DEPENDENTS' AND d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
		$row = mysql_fetch_assoc($sql);
		$result = $row['total'];
		if($result != ''){
			return $result." days";
		}else{
			return '0 day';
		}
	}
      
      public function getCurrentUsageDependent($type, $did){
		$did = $this->securestring('decrypt',$did);
		$staffID = $this->getStaffID($did);
		$year = date('Y');
		$sql = mysql_query("SELECT sum(d_charge) as 'total' FROM tbl_treatment WHERE d_type = 'DEPENDENTS' AND d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
		$row = mysql_fetch_assoc($sql);
		$result = $row['total'];
		if($result != ''){
			return $result;
		}else{
			return '0.00';
		}
	}
       
	
	public function getBalance($type, $did){
		$yearly = $this->getYearlyAmount($type, $did);
		$usage = $this->getCurrentUsage($type, $did);
		$result = $yearly-$usage;
		return $result;
	}
       
	
	public function getBalanceDependent($type, $did){
		$yearly = $this->getYearlyAmount($type, $did);
		$usage = $this->getCurrentUsageDependent($type, $did);
		$result = $yearly-$usage;
		return $result;
	}
	
	public function getBalanceDays($type, $did){
		$yearly = 14;
		$usage = $this->getCurrentUsageDays($type, $did);
		$result = $yearly-$usage;
		return $result;
	}
	
	public function getBalanceDaysDependent($type, $did){
		$yearly = 14;
		$usage = $this->getCurrentUsageDaysDependent($type, $did);
		$result = $yearly-$usage;
		return $result;
	}
	
	public function getPercentage($type, $did){
		$yearly = $this->getYearlyAmount($type, $did);
		$usage = $this->getCurrentUsage($type, $did);
		$result = round(($usage/$yearly)*100);
		return $result."%";
	}
	
	public function getPercentageDependent($type, $did){
		$yearly = $this->getYearlyAmount($type, $did);
		$usage = $this->getCurrentUsageDependent($type, $did);
		$result = round(($usage/$yearly)*100);
		return $result."%";
	}
	
	public function getPercentageDays($type, $did){
		$yearly = 14;
		$usage = $this->getCurrentUsageDays($type, $did);
		$result = round(($usage/$yearly)*100);
		return $result."%";
	}
	
	public function getPercentageDaysDependent($type, $did){
		$yearly = 14;
		$usage = $this->getCurrentUsageDaysDependent($type, $did);
		$result = round(($usage/$yearly)*100);
		return $result."%";
	}
	
	public function getAmount($col, $did){
		$sql = mysql_query("SELECT $col FROM tbl_setting WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row[$col];
	}
	
	public function getStaffID($did){
		$sql = mysql_query("SELECT d_staffID FROM tbl_labor WHERE d_id = '$did'");
		$row = mysql_fetch_assoc($sql);
		return $row['d_staffID'];
	}
	
	public function checkDependent($value, $did){
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = '$did'");
		$row = mysql_num_rows($sql);
		if($row != 0 && $value == '400.00'){
			$result = $value*2;
		}else{
			$result = $value;
		}
		return $result;
	}
	
	public function dependent($did){
		$did = $this->securestring('decrypt',$did);
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = '$did'");
		$row = mysql_num_rows($sql);
		return $row;
	}
	
	public function getDependentImage($who, $gen){
		if($who == 'WIFE'){
			$result = 'images/profile/wife.png';
		}else{    
			if($gen == 'MALE'){
				$result = 'images/profile/childmale.png';
			}else{
				$result = 'images/profile/childfemale.png';
			}
		}
		
		return $result;
	}
	
	public function dependentCount($did){
		$sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$did'");
		$row = mysql_num_rows($sql);
		return $row;
	}

  }
?>

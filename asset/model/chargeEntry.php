<?php
class chargeEntry extends general{

    public function __construct(){

    }

    //--------------------------------------------------------------------------------------
    //      Bahagian blue background
    //--------------------------------------------------------------------------------------
    public function dependentCount($did){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_dependent = '$did'");
        $row = mysql_num_rows($sql);
        return $row;
    }

    public function getYearlyAmount($type, $did){
        $sql = mysql_query("SELECT d_category FROM tbl_labor WHERE d_id = '$did'");
        $row = mysql_fetch_assoc($sql);
        $category = $row['d_category'];
        $did2 = $this->allTable($did, 'd_labor', 'tbl_profile', 'd_id');
        if($type == 'out'){
            $value = $this->getAmount('d_value2',$category);
            $result = $this->checkDependent($value,$did2);
            
        }elseif($type == 'in'){
            $result = $this->getAmount('d_value3',$category);
        }
        return $result;
    }


    public function getAmount($col, $did){
        $sql = mysql_query("SELECT $col FROM tbl_setting WHERE d_id = '$did'");
        $row = mysql_fetch_assoc($sql);
        return $row[$col];
    }

    public function checkDependent($value, $did){
        $sql = mysql_query("SELECT d_id FROM tbl_profile WHERE d_type = 'DEPENDENTS' AND d_dependent = '$did'");
        $row = mysql_num_rows($sql);
        if($row != 0 && $value == '400.00'){
            $result = $value*2;
        }else{
            $result = $value;
        }
        return $result;
    }

    function usage1($type, $did){
        $staffID = $this->allTable($did, 'd_id', 'tbl_labor', 'd_staffID');
        $year = date('Y');
        $sql = mysql_query("SELECT sum(d_charge) as 'total' FROM tbl_treatment WHERE d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        $result = $row['total'];
        if($result != ''){
            return $result;
        }else{
            return '0.00';
        }
    }

    function usage2($type, $did){
        $staffID = $this->allTable($did, 'd_id', 'tbl_labor', 'd_staffID');
        $year = date('Y');
        $sql = mysql_query("SELECT sum(d_day) as 'total' FROM tbl_treatment WHERE d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        $result = $row['total'];
        if($result != ''){
            return $result;
        }else{
            return 0;
        }
    }

    function usage3a($type, $did){
        $staffID = $this->allTable($did, 'd_id', 'tbl_labor', 'd_staffID');		
        $mrn = $this->allTable($did, 'd_labor', 'tbl_profile', 'd_mrn');
        $year = date('Y');
        $sql = mysql_query("SELECT sum(d_day) as 'total' FROM tbl_treatment WHERE d_mrn = '$mrn' AND d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        $result = $row['total'];
        if($result != ''){
            return $result;
        }else{
            return '0';
        }
    }
 
    function usage3b($type, $did){
        $staffID = $this->allTable($did, 'd_id', 'tbl_labor', 'd_staffID');		
        $mrn = $this->allTable($did, 'd_labor', 'tbl_profile', 'd_mrn');
        $year = date('Y');
        $sql = mysql_query("SELECT sum(d_day) as 'total' FROM tbl_treatment WHERE d_staffID = '$staffID' AND d_type = '$type' AND year(d_vdate) = '$year' AND d_status = 'active'");
        $row = mysql_fetch_assoc($sql);
        $result = $row['total'];
        if($result != ''){
            return $result;
        }else{
            return '0';
        }
    }

    function balance1($yearly, $usage){
        $result = $yearly-$usage;
        return $result;
    }

    //--------------------------------------------------------------------------------------
    //      Bahagian blue background
    //--------------------------------------------------------------------------------------
    public function addCharges($data){
        $sIC = mysql_real_escape_string(trim($data['sIC']));
        $sID = mysql_real_escape_string(trim($data['sID']));
        $sDate = $data['sDate'];
        $treatmentType = mysql_real_escape_string(trim($data['treatmentType']));
        $treatment = mysql_real_escape_string(trim($data['treatment']));
        $treatmentlist = mysql_real_escape_string(trim($data['treatmentlist']));
        $sconfimDate = $data['sconfimDate'];
        $sMRN = mysql_real_escape_string(trim($data['sMRN']));
        $sEpisode = mysql_real_escape_string(trim($data['sEpisode']));
        $sNOD = mysql_real_escape_string(trim($data['sNOD']));
        $sCh = mysql_real_escape_string(trim($data['sCh']));
        $sBill = mysql_real_escape_string(trim($data['sBill']));
        $sDcode = mysql_real_escape_string(trim($data['sDcode']));
        $sEEr = mysql_real_escape_string(trim($data['sEEr']));
        $sEEd = mysql_real_escape_string(trim($data['sEEd']));
        $sRemark = mysql_real_escape_string(trim($data['sRemark']));
        $who = $this->securestring('decrypt',$_SESSION['logid']);
                                         
        if($treatmentlist == 'Covered' || $treatmentlist == 'Kiv' || $treatmentlist == 'Not Covered'){
            $treatmentstat = $treatmentlist;
            $treatmentlist = '';
        }else{
            $treatmentstat = '';
            $treatmentlist = $treatmentlist;
        }                               
        
        $insert = mysql_query("INSERT INTO tbl_treatment(d_staffID, d_mrn, d_episode, d_charge, d_day, d_bill, d_doc, d_type, d_treatment, d_treatmentstatus, d_treatmentlist, d_treatmentconfirm, d_exceedday, d_exceedcharge, d_exceedRemarks, d_who, d_created, d_modified, d_status, d_vdate, d_treatCheck)
        VALUES('$sID', '$sMRN', '$sEpisode', '$sCh', '$sNOD', '$sBill', '$sDcode', '$treatmentType', '$treatment', '$treatmentstat', '$treatmentlist', '$sconfimDate', '$sEEd', '$sEEr', '$sRemark', '$who', NOW(), NOW(), 'active', '$sDate', 'NO')");
        
        if(!$insert){
            return mysql_error();
        }else{
            return 1;
        }
    }
    
    public function addCharges2($data){
        $task = $this->addCharges($data);
        
        if($task == 1){
            $datacode = $this->getLatest('tbl_installment', 'd_datacode');
            $user = mysql_real_escape_string(trim($data['sID']));
            $sEEr = mysql_real_escape_string(trim($data['sEEr']));
            $sRemark = mysql_real_escape_string(trim($data['sRemark']));
            $who = $this->securestring('decrypt',$_SESSION['logid']);
            $code = strtotime("now");
            
            $sql = mysql_query("INSERT INTO tbl_installment(d_type, d_datacode, d_value1, d_value2, d_value3, d_value4, d_value5, d_created, d_modified, d_status, d_who)VALUES('MAIN', '$datacode', '$user', '$sEEr', '$code', '$sRemark', '$sEEr', NOW(), NOW(), 'new', '$who')");
            
            if(!$sql){
                return mysql_error();
            }else{
                return 1;
            }
        }else{
            return mysql_error();
        }
    }

}
?>
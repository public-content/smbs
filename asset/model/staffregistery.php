<?php 
    class staffregistery extends general{
        
        public function addUser($data){
            $sName = mysql_real_escape_string(trim($data['sName']));
            $sIC = mysql_real_escape_string(trim($data['sIC']));
            $sID = mysql_real_escape_string(trim($data['sID']));
            $sDepartment = $data['sDepartment'];
            $sDesign = mysql_real_escape_string(trim($data['sDesign']));
            $sED = mysql_real_escape_string(trim($data['sED']));
            $sGender = mysql_real_escape_string(trim($data['sGender']));
            $sMS = mysql_real_escape_string(trim($data['sMS']));
            $sDA = mysql_real_escape_string(trim($data['sDA']));
            $sCat = mysql_real_escape_string(trim($data['sCat']));
            $sGrp = mysql_real_escape_string(trim($data['sGrp']));
            $sWS = mysql_real_escape_string(trim($data['sWS']));
            $sMrn = mysql_real_escape_string(trim($data['sMrn']));
            $sPN = mysql_real_escape_string(trim($data['sPN']));
            $sEmail = mysql_real_escape_string(trim($data['sEmail']));
            $sUname = mysql_real_escape_string(trim($data['sUname']));
            $sUpass = mysql_real_escape_string(trim($data['sUpass']));
            $sUrole = mysql_real_escape_string(trim($data['sUrole']));
            
            $task1 = $this->insertLabor($sID,$sDepartment,$sDesign,$sED,$sCat,$sGrp,$sWS); //insert labor
            if($task1 != 0){
                $task2 = $this->insertProfie($task1,'STAFF',$sName,$sIC,$sGender,$sMS,$sDA,$sMrn,$sPN,$sEmail); //insert profile
                if($task2 != 0){
                      $task3 = $this->insertUser($task2,$sUname,$sUpass,$sUrole); //insert user
                      if($task3 != 0){
                          $result = 1;      
                      }else{
                          $result = mysql_error();
                      }
                }else{
                    $result = mysql_error();
                }
            }else{
                $result = mysql_error();
            }
            return $result;
        }
        
        public function insertLabor($sID,$sDepartment,$sDesign,$sED,$sCat,$sGrp,$sWS){
            $sDepartments = implode(' , ',$sDepartment);
            $sql = mysql_query("INSERT INTO tbl_labor(d_staffID, d_department, d_employment, d_group, d_designation, d_category, d_created, d_modified, d_wstatus, d_status)
                                VALUES('$sID','$sDepartments','$sED','$sGrp','$sDesign','$sCat',NOW(),NOW(),'$sWS','active')");
            if(!$sql){
                return 0;
            }else{
                $did = mysql_query("SELECT d_id FROM tbl_labor WHERE d_staffID = '$sID' AND d_department = '$sDepartments' AND d_employment = '$sED' ORDER BY d_id DESC LIMIT 1");
                $drow = mysql_fetch_assoc($did);
                return $drow['d_id'];
            }
        }
        
        public function insertProfie($task1,$d_type,$sName,$sIC,$sGender,$sMS,$sDA,$sMrn,$sPN,$sEmail){
            $sql = mysql_query("INSERT INTO tbl_profile(d_type, d_labor, d_available, d_name, d_gender, d_ic, d_pnum, d_email, d_martial,d_mrn, d_created, d_modified, d_status)VALUES('$d_type','$task1','$sDA','$sName','$sGender','$sIC','$sPN','$sEmail','$sMS','$sMrn',NOW(),NOW(),'active')");
            if(!$sql){
                return 0;
            }else{
                $did = mysql_query("SELECT d_id FROM tbl_profile WHERE d_labor = '$task1' AND d_ic = '$sIC' AND d_mrn = '$sMrn' ORDER BY d_id DESC LIMIT 1");
                $drow = mysql_fetch_assoc($did);
                return $drow['d_id'];
            }
        }
        
        public function insertUser($task2,$sUname,$sUpass,$sUrole){
            $sUpasss = sha1($sUpass);
            $sql = mysql_query("INSERT INTO tbl_user(d_role, d_userid, d_username, d_password, d_created, d_modified, d_status)
                                VALUES('$sUrole','$task2','$sUname','$sUpasss',NOW(),NOW(),'active')");
            if(!$sql){
                return 0;
            }else{
                return 1;
            }
        }
        
        public function editUser($data){
            $sName = mysql_real_escape_string(trim($data['sName']));
            $sIC = mysql_real_escape_string(trim($data['sIC']));
            $sID = mysql_real_escape_string(trim($data['sID']));
            $sDepartment = $data['sDepartment'];
            $sDesign = mysql_real_escape_string(trim($data['sDesign']));
            $sED = mysql_real_escape_string(trim($data['sED']));
            $sRD = mysql_real_escape_string(trim($data['sRD']));
            $sGender = mysql_real_escape_string(trim($data['sGender']));
            $sMS = mysql_real_escape_string(trim($data['sMS']));
            $sDA = mysql_real_escape_string(trim($data['sDA']));
            $sCat = mysql_real_escape_string(trim($data['sCat']));
            $sGrp = mysql_real_escape_string(trim($data['sGrp']));
            $sWS = mysql_real_escape_string(trim($data['sWS']));
            $sMrn = mysql_real_escape_string(trim($data['sMrn']));
            $sPN = mysql_real_escape_string(trim($data['sPN']));
            $sEmail = mysql_real_escape_string(trim($data['sEmail']));
            $sUname = mysql_real_escape_string(trim($data['sUname']));
            $sUpass = mysql_real_escape_string(trim($data['sUpass']));
            $sUrole = mysql_real_escape_string(trim($data['sUrole']));
            $proid = mysql_real_escape_string(trim($data['proid']));
            $labid = mysql_real_escape_string(trim($data['labid']));
            $logid = mysql_real_escape_string(trim($data['logid']));
            
            
            //department
            $sDepartments = implode(' , ',$sDepartment);
            
            //password
            if($sUpass != ''){
                $password = sha1($sUpass);
            }else{
                $password = $this->allTable($logid,'d_id','tbl_user','d_password');
            }
            
            
            $sql1 = mysql_query("UPDATE tbl_labor SET d_staffID='$sID', d_department='$sDepartments', d_employment='$sED', d_resignation='$sRD', d_group='$sGrp', d_designation='$sDesign', d_category='$sCat', d_modified=NOW(), d_wstatus='$sWS' WHERE d_id = '$labid'");
            
            
            if(!$sql1){
                return mysql_error();
            }else{
                $sql2 = mysql_query("UPDATE tbl_profile SET d_available='$sDA', d_name='$sName', d_gender='$sGender', d_ic='$sIC', d_pnum='$sPN', d_email='$sEmail', d_martial='$sMS', d_mrn='$sMrn', d_modified=NOW() WHERE d_id = '$proid'");
                if(!$sql2){
                    return mysql_error();
                }else{
                    $sql3 = mysql_query("UPDATE tbl_user SET  d_role='$sUrole', d_username='$sUname', d_password='$password', d_modified=NOW() WHERE d_id = '$logid'");
                    if(!$sql3){
                        return mysql_error();
                    }else{
                        return 1;
                    }   
                }
            }
        }
        
    }
?>